package com.pointcliki.meshnetwork.graphing;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Viewport;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.meshnetwork.simulator.SimulatorScene;
import com.pointcliki.ui.UIEntity;

public class Grapher extends UIEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5511726232753186426L;
	
	private String fLabel;
	protected int fMf;
	protected int fMt;
	protected ArrayList<Line> fLines;
	protected Vector2f fDragFrom;
	protected Vector2f fDragOrigin;
	
	public Grapher(String label, int mt) {
		fLabel = label;
		fMf = 0;
		fMt = mt;
		fLines = new ArrayList<Line>();
	}
	
	public void addLine(Color c) {
		fLines.add(new Line(c));
	}
	
	public void add(int index, int value) {
		fLines.get(index).add(value);
	}
	
	@Override
	public void render(Graphics graphics, Viewport view, long currentTime) {
		super.render(graphics, view, currentTime);
		
		graphics.setColor(Color.darkGray);
		graphics.fillRect(-30, -30, fSpan.getWidth() + 44, fSpan.getHeight() + 20);
		
		// Label
		SimulatorScene.font().drawString(-25, -25, fLabel, Color.white);
		
		graphics.setColor(Color.white);
		graphics.fillRect(-30, -10, fSpan.getWidth() + 44, fSpan.getHeight() + 24);
		
		graphics.setLineWidth(1);
		graphics.setColor(Color.black);
		graphics.drawRect(0, 0, fSpan.getWidth(), fSpan.getHeight());
						
		int w = SimulatorScene.font().getWidth(fMf + "");
		
		// Axis
		SimulatorScene.font().drawString(-w - 8, 0, fMf + "", Color.black);
		graphics.drawLine(-6, 6, 0, 6);
		
		// Draw lines
		for (Line l: fLines) l.render(graphics);
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		super.handleUIMouseEvent(type, local, event);
		if (type.equals("mouse.down")) {
			fDragFrom = new Vector2f(event.x(), event.y());
			fDragOrigin = position();
			
		} else if (type.equals("mouse.drag")) {
			Vector2f v = new Vector2f(event.x(), event.y()).sub(fDragFrom);
			position(v.add(fDragOrigin));
		}
	}
	
	public class Line {
		
		private Color fColor;
		private int[] fList;
		private int fI;
		
		public Line(Color c) {
			fColor = c;
			fList = new int[fMt];
			fI = 0;
		}
		
		public void add(int n) {
			fList[fI] = n;
			fMf = Math.max(fMf, n);
			fI++;
		}
		
		public void render(Graphics graphics) {
			float prevx = 1;
			float prevy = fSpan.getHeight();
			float dx = (fSpan.getWidth() - 2) / fMt;
			float y = (fSpan.getHeight() - 7) / fMf;
			
			for (int i = 0; i < fI; i++) {
				int n = fList[i];
				float h = fSpan.getHeight() - 1 - n * y;
				// Draw line
				graphics.setColor(fColor);
				graphics.drawLine(prevx, prevy, prevx + dx, h);
				prevx += dx;
				prevy = h;
			}
		}
	}
}
