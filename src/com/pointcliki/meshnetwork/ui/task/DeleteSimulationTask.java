package com.pointcliki.meshnetwork.ui.task;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class DeleteSimulationTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 888770941313800729L;

	public DeleteSimulationTask() {
		super("Delete Simulation");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int ok = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this simulation?", "Delete Simulation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (ok == 1) return;
		
		TreePath path = SimulationsPanel.getInstance().tree().getSelectionModel().getSelectionPath();
		Object c = path.getPathComponent(1);
		if (c instanceof IconLabelNode) {
			IconLabelNode n = (IconLabelNode) c;
			if (n.item() instanceof SimulationItem) {
				((DefaultTreeModel) SimulationsPanel.getInstance().tree().getModel()).removeNodeFromParent(n);
			}
		}
	}

}
