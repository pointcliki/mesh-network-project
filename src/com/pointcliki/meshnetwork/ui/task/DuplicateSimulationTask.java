package com.pointcliki.meshnetwork.ui.task;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.tree.TreePath;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class DuplicateSimulationTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -559119476001448480L;

	public DuplicateSimulationTask() {
		super("Duplicate Simulation...");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		TreePath path = SimulationsPanel.getInstance().tree().getSelectionModel().getSelectionPath();
		Object c = path.getPathComponent(1);
		if (c instanceof IconLabelNode) {
			IconLabelNode n = (IconLabelNode) c;
			if (n.item() instanceof SimulationItem) {
				SimulationItem sim = (SimulationItem) n.item();
				String name = JOptionPane.showInputDialog("Simulation Name:", sim.name());
				try {
					JSONObject json = sim.exportToJSON(); 
					json.put("name", name);
					SimulationsPanel.getInstance().importSimulation(json);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}

