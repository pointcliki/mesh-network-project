package com.pointcliki.meshnetwork.ui.task;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;

public class CreateSimulationTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -4605047491764679117L;

	public CreateSimulationTask() {
		super("Create New Simulation...");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String name = JOptionPane.showInputDialog("Simulation Name:");
		try {
			JSONObject json = new JSONObject("{\"name\":\"Simulation 1\",\"options\":{},\"stack\":{\"name\":\"Protocol Stack\",\"options\":{},\"items\":[]},\"scripts\":{\"name\":\"Scripts\",\"options\":{},\"items\":[]},\"annotations\":{\"name\":\"Annotations\",\"options\":{},\"items\":[]},\"metrics\":{\"name\":\"Metrics\",\"options\":{},\"items\":[]},\"events\":{\"name\":\"Events\",\"options\":{},\"items\":[]}}");
			json.put("name", name);
			SimulationsPanel.getInstance().importSimulation(json);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
}
