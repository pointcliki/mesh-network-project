package com.pointcliki.meshnetwork.ui.task;

import java.awt.event.ActionEvent;

import javax.swing.tree.TreePath;

import org.json.JSONException;

import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.panel.ResultsPanel;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class RunSimulationTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 2199860007847544509L;

	public RunSimulationTask() {
		super("Run Simulation");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			TreePath path = SimulationsPanel.getInstance().tree().getSelectionModel().getSelectionPath();
			Object c = path.getPathComponent(1);
			if (c instanceof IconLabelNode) {
				ResultsPanel.getInstance().runSimulation(((SimulationItem) ((IconLabelNode) c).item()).exportToJSON());
				ResultsPanel.getInstance().focus();
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

}
