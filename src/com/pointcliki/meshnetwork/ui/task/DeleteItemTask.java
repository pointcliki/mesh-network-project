package com.pointcliki.meshnetwork.ui.task;

import java.awt.event.ActionEvent;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.SimulationSubItem;
import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.panel.TaskPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class DeleteItemTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 888770941313800729L;

	public DeleteItemTask() {
		super("Remove from Simulation");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		TreePath path = SimulationsPanel.getInstance().tree().getSelectionModel().getSelectionPath();
		Object c = path.getLastPathComponent();
		if (c instanceof IconLabelNode) {
			IconLabelNode n = (IconLabelNode) c;
			IconLabelNode parent = (IconLabelNode) n.getParent();
			SimulationSubItem grp = (SimulationSubItem) parent.item();
			grp.remove((DefaultTreeModel) SimulationsPanel.getInstance().tree().getModel(), n, n.item());
		}
		TaskPanel.getInstance().setItem((SimulationItem) ((IconLabelNode) path.getPathComponent(1)).item(), null);
	}

}
