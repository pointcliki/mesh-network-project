package com.pointcliki.meshnetwork.ui.task;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;

public class ImportSimulationTask extends Task {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -693165071823952831L;

	public ImportSimulationTask() {
		super("Import a Simulation...");
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		try {
			JFileChooser fc = new JFileChooser("Import a Simulation...");
			fc.addChoosableFileFilter(new FileNameExtensionFilter("Simulation (*.sim)", "sim"));
			fc.showOpenDialog(null);
			
			if (fc.getSelectedFile() == null) return;
			
			SimulationsPanel.getInstance().importSimulation(new JSONObject(new JSONTokener(new FileReader(fc.getSelectedFile()))));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
