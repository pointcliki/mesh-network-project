package com.pointcliki.meshnetwork.ui.panel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.l2fprod.common.propertysheet.DefaultProperty;
import com.l2fprod.common.propertysheet.PropertySheetTable;
import com.l2fprod.common.propertysheet.PropertySheetTableModel;
import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.model.SimulationItem;

public class ConfigurationPanel extends AbstractPanel {

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = -8987908774396699284L;
	
	private static ConfigurationPanel sPanel;
	private JLabel fLabel;
	private PropertySheetTable fTable;

	public ConfigurationPanel() {
		super();
		
		sPanel = this;
		
		JPanel panel = panel();
		
		BoxLayout layout = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
		panel.setLayout(layout);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		fLabel = new JLabel("Choose an item to configure");
		fTable = new PropertySheetTable();
		fTable.setAlignmentX(LEFT_ALIGNMENT);
		fTable.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		fLabel.setAlignmentX(LEFT_ALIGNMENT);
		fLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		panel.add(fLabel);		
		panel.add(Box.createVerticalStrut(5));
		panel.add(fTable);
	}
	
	public static ConfigurationPanel getInstance() {
		return sPanel;
	}
	
	public void setItem(SimulationItem sim, Item item2) {
		setItem(sim, item2, true);
	}

	public void setItem(SimulationItem sim, Item item2, boolean editable) {
		final Item item;
		
		PropertySheetTableModel model = new PropertySheetTableModel();
		
		if (sim == null && item2 == null) {
			fTable.setModel(model);
			return;
		}
		if (item2 == null) item = sim;
		else item = item2;
		
		fTable.commitEditing();
		model.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent e) {
				String name = ((DefaultProperty) e.getSource()).getName();
				
				for (Option<? extends Object> o: item.options()) {
					if (o.name().equals(name)) {
						o.setValue(e.getNewValue());
					}
				}
				SimulationsPanel.getInstance().save();
			}
			
		});
		fLabel.setText("<html><b>" + item.name() + "</b></html>");
		for (Option<? extends Object> o: item.options()) {
			model.addProperty(o.property());
		}
		fTable.setModel(model);
		fTable.setEnabled(editable);
	}
}
