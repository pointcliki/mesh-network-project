package com.pointcliki.meshnetwork.ui.panel;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.tree.TreePath;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.data.xy.YIntervalSeriesCollection;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.Metric;
import com.pointcliki.meshnetwork.model.ResultsItem;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class GraphPanel extends AbstractPanel {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -7128488129237465004L;

	private static GraphPanel sPanel;

	private YIntervalSeriesCollection fDataset;
	private XYPlot fPlot;
	
	public GraphPanel() {
		sPanel = this;
		
		panel().setLayout(new BorderLayout());
		
		// Add the series to your data set
		fDataset = new YIntervalSeriesCollection();
		
		JFreeChart chart = ChartFactory.createXYLineChart("Network Simulation Results", "Frame", "Count", fDataset, PlotOrientation.VERTICAL, true, true, false);
		fPlot = chart.getXYPlot();
		DeviationRenderer renderer = new DeviationRenderer();
		renderer.setSeriesStroke(0, new BasicStroke(3.0f, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND));
        renderer.setSeriesStroke(0, new BasicStroke(1.0f));
        renderer.setSeriesStroke(1, new BasicStroke(1.0f));
        renderer.setSeriesFillPaint(0, new Color(200, 200, 255));
        renderer.setSeriesFillPaint(1, new Color(255, 200, 200));
		renderer.setBaseShapesVisible(false);
		
		fPlot.setRenderer(renderer);

		panel().add(new ChartPanel(chart), BorderLayout.CENTER);
	}
	
	public static GraphPanel getInstance() {
		return sPanel;
	}

	public void setItems(TreePath[] paths) {
		fDataset.removeAllSeries();
		
		if (paths == null) return;
		
		for (TreePath p: paths) {
			ResultsItem res = (ResultsItem) (((IconLabelNode) p.getPathComponent(1)).item());
			Item item = ((IconLabelNode) p.getLastPathComponent()).item();
			
			if (item instanceof Metric) {
				Metric m = (Metric) item;
				fDataset.addSeries(m.series());
				
			} else if (item instanceof Event) {
				Event e = (Event) item;
				fDataset.addSeries(e.series());
			}
		}
	}
}
