package com.pointcliki.meshnetwork.ui.panel;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class AbstractPanel extends JPanel {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -540426339404405739L;
	
	private JPanel fPanel;

	public AbstractPanel() {
		setLayout(new BorderLayout());
		fPanel = new JPanel();
		add(new JScrollPane(fPanel));
	}
	
	protected JPanel panel() {
		return fPanel;
	}
}
