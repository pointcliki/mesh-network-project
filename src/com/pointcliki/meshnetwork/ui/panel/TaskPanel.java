package com.pointcliki.meshnetwork.ui.panel;

import javax.swing.JScrollPane;

import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTaskPaneGroup;
import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.ResultsItem;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.Task;
import com.pointcliki.meshnetwork.ui.task.CreateSimulationTask;
import com.pointcliki.meshnetwork.ui.task.ImportSimulationTask;

public class TaskPanel extends AbstractPanel {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 2229305826461635642L;
	
	private static TaskPanel sPanel;
	private JTaskPaneGroup fGroup;
	private JTaskPane fTasks;
	
	public TaskPanel() {
		super();
		
		sPanel = this;		
		fTasks = new JTaskPane();		 
		populateTasks();
		((JScrollPane) getComponent(0)).getViewport().add(fTasks);
	}
	
	public void setItem(SimulationItem sim, Item e) {
		((JScrollPane) getComponent(0)).getViewport().remove(0);
		
		fTasks = new JTaskPane();
		
		populateTasks();
		
		fGroup = new JTaskPaneGroup();
		fGroup.setTitle(sim.name());
		for (Task t: sim.tasks()) fGroup.add(t);
		
		fTasks.add(fGroup);
		
		if (e != null) {
			fGroup = new JTaskPaneGroup();
			fGroup.setTitle(e.name());
			for (Task t: e.tasks()) fGroup.add(t);
			
			fTasks.add(fGroup);
		}
		((JScrollPane) getComponent(0)).getViewport().add(fTasks);
	}
	
	public void setItem(ResultsItem res) {
		((JScrollPane) getComponent(0)).getViewport().remove(0);
		
		fTasks = new JTaskPane();
		
		populateTasks();
		
		((JScrollPane) getComponent(0)).getViewport().add(fTasks);		
	}
	
	private void populateTasks() {
		JTaskPaneGroup group = new JTaskPaneGroup();
		group = new JTaskPaneGroup();
		group.setTitle("Create & Import");
		group.add(new CreateSimulationTask());
		group.add(new ImportSimulationTask());
		fTasks.add(group);
	}

	public static TaskPanel getInstance() {
		return sPanel;
	}
}
