package com.pointcliki.meshnetwork.ui.panel;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.pointcliki.meshnetwork.model.Script;
import com.pointcliki.meshnetwork.model.ScriptGroup;
import com.pointcliki.meshnetwork.protocol.ProtocolGroup;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Library;
import com.pointcliki.meshnetwork.ui.utils.SimulationsCellRenderer;
import com.pointcliki.meshnetwork.ui.utils.LibraryNodeTransferHandler;

public class LibraryPanel extends AbstractPanel {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 2421740570485076039L;
	
	private JTree fTree;
	private DefaultMutableTreeNode fRoot = new DefaultMutableTreeNode("root");

	public LibraryPanel() {
		super();
		
		fTree = new JTree(fRoot);
		fTree.setRootVisible(false);
		fTree.setShowsRootHandles(true);
		fTree.setBorder(new EmptyBorder(5, 5, 5, 5));
		fTree.setScrollsOnExpand(true);
		fTree.setCellRenderer(new SimulationsCellRenderer());
		fTree.setTransferHandler(new LibraryNodeTransferHandler());
		fTree.setDragEnabled(true);
		
		panel().setLayout(new BorderLayout());
		panel().add(fTree);
		
		DefaultTreeModel model = (DefaultTreeModel) fTree.getModel();
		
		for (ProtocolGroup grp: Library.protocols()) {
			model.insertNodeInto(new IconLabelNode(model, grp), fRoot, model.getChildCount(fRoot));
		}
		
		fRoot.add(new IconLabelNode(model, new ScriptGroup("Layout Scripts", Library.layoutScripts())));
		fRoot.add(new IconLabelNode(model, new ScriptGroup("Mobility Scripts", Library.mobilityScripts())));
		fRoot.add(new IconLabelNode(model, new ScriptGroup("Flow Scripts", Library.flowScripts())));
		
		JPanel info = new JPanel();
		info.setLayout(new BorderLayout());
		info.setBorder(new EmptyBorder(5, 5, 5, 5));
		JLabel label = new JLabel("<html><b>Library</b><br/>Drag protocols and scripts onto a simulation to add them to the simulation.");
		info.add(label);
		
		add(info, BorderLayout.SOUTH);
		
		fTree.expandPath(new TreePath(fRoot));
	}
}
