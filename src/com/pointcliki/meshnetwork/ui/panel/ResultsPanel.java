package com.pointcliki.meshnetwork.ui.panel;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.pointcliki.meshnetwork.model.ResultsItem;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.ResultsTreeSelectorListener;
import com.pointcliki.meshnetwork.ui.utils.SimulationsCellRenderer;

public class ResultsPanel extends AbstractPanel {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -4839105397032440637L;

	private static ResultsPanel sPanel;
	private JTree fTree;
	private DefaultMutableTreeNode fRoot = new DefaultMutableTreeNode("root");
	private Runnable fFocuser;
	
	public ResultsPanel() {
		super();
		
		sPanel = this;
		
		fTree = new JTree(fRoot);
		fTree.setRootVisible(false);
		fTree.setShowsRootHandles(true);
		fTree.setBorder(new EmptyBorder(5, 5, 5, 5));
		fTree.setScrollsOnExpand(true);
		fTree.setCellRenderer(new SimulationsCellRenderer());
		fTree.setDragEnabled(true);
		fTree.addTreeSelectionListener(new ResultsTreeSelectorListener());
		
		panel().setLayout(new BorderLayout());
		panel().add(fTree);
		
		load();
		
		fTree.expandPath(new TreePath(fRoot));
	}
	
	public void load() {
		/*
		try {
			JSONObject obj = new JSONObject(new JSONTokener(new FileReader(".simulations")));
			JSONArray arr = obj.getJSONArray("results");
			for (int i = 0; i < arr.length(); i++) {
				importResults(arr.getJSONObject(i));
			}
			fTree.expandPath(new TreePath(fRoot));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/
	}
	
	public void importResults(JSONObject obj) throws JSONException {
		int i = fRoot.getChildCount();
		ResultsItem res = new ResultsItem(obj.getString("name"));
		res.importFromJSON(obj);
		DefaultTreeModel model = (DefaultTreeModel) fTree.getModel();
		model.insertNodeInto(new IconLabelNode(model, res), fRoot, i);
		fTree.expandPath(new TreePath(fRoot));
	}
	
	public void runSimulation(JSONObject obj) throws JSONException {
		int i = fRoot.getChildCount();
		SimulationItem sim = new SimulationItem(obj.getString("name"));
		sim.importFromJSON(obj);
		
		DefaultTreeModel model = (DefaultTreeModel) fTree.getModel();
		
		ResultsItem res = new ResultsItem("#" + i + " - " + obj.getString("name"));
		IconLabelNode n = new IconLabelNode(model, res);
		res.setNode(n);
		model.insertNodeInto(n, fRoot, i);
		fTree.expandPath(new TreePath(fRoot));
		res.runSimulation(sim);
	}
	
	public JTree tree() {
		return fTree;
	}
	
	public void focus() {
		if (fFocuser != null) fFocuser.run();
	}
	
	public static ResultsPanel getInstance() {
		return sPanel;
	}
	
	public void setFocusWorker(Runnable runnable) {
		fFocuser = runnable;
	}
}
