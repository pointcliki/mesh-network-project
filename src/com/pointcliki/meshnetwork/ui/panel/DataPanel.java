package com.pointcliki.meshnetwork.ui.panel;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.Metric;
import com.pointcliki.meshnetwork.model.ResultsItem;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class DataPanel extends AbstractPanel {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -7128488129237465004L;

	private static DataPanel sPanel;
	private JList fList;
	private Simulator fSimulator;
	
	public DataPanel() {
		sPanel = this;
		
		JPanel panel = panel();
		
		BorderLayout layout = new BorderLayout();
		panel.setLayout(layout);
		panel.setOpaque(false);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		fList = new JList(new DefaultListModel());
		panel().add(fList, BorderLayout.CENTER);
	}
	
	public static DataPanel getInstance() {
		return sPanel;
	}
	
	public void setItem(ResultsItem res, Item item) {
		if (item instanceof SimulationItem) {
			fSimulator = ((SimulationItem) item).simulator();
			fList.setModel(fSimulator.log());
			
		} else if (item instanceof Metric) {
			fList.setModel(((Metric) item).listModel());
		} else if (item instanceof Event) {
			fList.setModel(((Event) item).listModel());
		}
 	}
}
