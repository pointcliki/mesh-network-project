package com.pointcliki.meshnetwork.ui.panel;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.SimulationsCellRenderer;
import com.pointcliki.meshnetwork.ui.utils.SimulationsNodeTransferHandler;
import com.pointcliki.meshnetwork.ui.utils.SimulationsTreeSelectorListener;

public class SimulationsPanel extends AbstractPanel {

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = -3874408837009308387L;
	
	private static SimulationsPanel sPanel;
	private JTree fTree;
	private DefaultMutableTreeNode fRoot = new DefaultMutableTreeNode("root");
	
	public SimulationsPanel() {
		super();
		
		sPanel = this;
		
		fTree = new JTree(fRoot);
		fTree.setRootVisible(false);
		fTree.setShowsRootHandles(true);
		fTree.setBorder(new EmptyBorder(5, 5, 5, 5));
		fTree.setScrollsOnExpand(true);
		fTree.setCellRenderer(new SimulationsCellRenderer());
		fTree.setTransferHandler(new SimulationsNodeTransferHandler());
		fTree.setDragEnabled(true);
		fTree.addTreeSelectionListener(new SimulationsTreeSelectorListener());
		
		panel().setLayout(new BorderLayout());
		panel().add(fTree);
		
		load();
		
		fTree.expandPath(new TreePath(fRoot));
	}
	
	public JTree tree() {
		return fTree;
	}
	
	public static SimulationsPanel getInstance() {
		return sPanel;
	}
	
	public void load() {
		try {
			JSONObject obj = new JSONObject(new JSONTokener(new FileReader(".simulations")));
			JSONArray arr = obj.getJSONArray("simulations");
			for (int i = 0; i < arr.length(); i++) {
				importSimulation(arr.getJSONObject(i));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void save() {
		try {
			FileWriter w = new FileWriter(".simulations");
			w.write(exportToJSON().toString());
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();
		for (int i = 0; i < fRoot.getChildCount(); i++) {
			arr.put(((IconLabelNode) fRoot.getChildAt(i)).item().exportToJSON());
		} 
		obj.put("simulations", arr);
		return obj;
	}

	public void importSimulation(JSONObject obj) throws JSONException {
		SimulationItem sim = new SimulationItem(obj.getString("name"));
		sim.importFromJSON(obj);
		DefaultTreeModel model = (DefaultTreeModel) fTree.getModel();
		model.insertNodeInto(new IconLabelNode(model, sim), fRoot, model.getChildCount(fRoot));
	}
}
