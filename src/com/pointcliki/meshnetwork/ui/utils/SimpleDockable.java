package com.pointcliki.meshnetwork.ui.utils;

import java.awt.Component;

import javax.swing.Icon;

import bibliothek.gui.dock.common.DefaultSingleCDockable;
import bibliothek.gui.dock.common.action.predefined.CBlank;
import bibliothek.gui.dock.common.intern.CDockable;

public class SimpleDockable extends DefaultSingleCDockable {

	public SimpleDockable(String name, Component content, Icon icon) {
		super(name, icon, name, content); 
		
		//addAction(new CButton("Wow", Icons.EventIcon));
		putAction(CDockable.ACTION_KEY_EXTERNALIZE, CBlank.BLANK);
	}

}
