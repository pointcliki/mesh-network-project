package com.pointcliki.meshnetwork.ui.utils;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.pointcliki.meshnetwork.model.Item;

public class IconLabelNode extends DefaultMutableTreeNode {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 2260330766842226281L;
	
	private Item fItem;
	
	public IconLabelNode(String name, Icon icon) {
		fItem = new Item(name, icon);
	}
	
	public IconLabelNode(DefaultTreeModel model, Item item) {
		fItem = item;
		fItem.populate(model, this);
	}
	
	public IconLabelNode(DefaultTreeModel model, Item item, boolean populate) {
		fItem = item;
		if (populate) fItem.populate(model, this);
	}

	public String name() {
		return fItem.name();
	}
	
	public Icon icon() {
		return fItem.icon();
	}
	public Item item() {
		return fItem;
	}
}
