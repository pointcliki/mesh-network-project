package com.pointcliki.meshnetwork.ui.utils;

import java.util.List;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.TreePath;

public class LibraryNodeTransferHandler extends TransferHandler {
	
    /**
	 * Serial key
	 */
	private static final long serialVersionUID = 5028148428256098362L;
	
	DataFlavor nodesFlavor;
    DataFlavor[] flavors = new DataFlavor[1];

    public LibraryNodeTransferHandler() {
        try {
            String mimeType = DataFlavor.javaJVMLocalObjectMimeType + ";class=\"" +
                com.pointcliki.meshnetwork.ui.utils.IconLabelNode[].class.getName() + "\"";
            nodesFlavor = new DataFlavor(mimeType);
            flavors[0] = nodesFlavor;
        } catch(ClassNotFoundException e) {
            System.out.println("ClassNotFound: " + e.getMessage());
        }
    }

    protected Transferable createTransferable(JComponent c) {
        JTree tree = (JTree)c;
        TreePath[] paths = tree.getSelectionPaths();
        if(paths != null) {
            // Make up a node array of copies for transfer and
            // another for/of the nodes that will be removed in
            // exportDone after a successful drop.
            List<IconLabelNode> copies =
                new ArrayList<IconLabelNode>();
            List<IconLabelNode> toRemove =
                new ArrayList<IconLabelNode>();
            IconLabelNode node =
                (IconLabelNode)paths[0].getLastPathComponent();
            IconLabelNode copy = copy(node);
            copies.add(copy);
            toRemove.add(node);
            for(int i = 1; i < paths.length; i++) {
                IconLabelNode next =
                    (IconLabelNode)paths[i].getLastPathComponent();
                // Do not allow higher level nodes to be added to list.
                if(next.getLevel() < node.getLevel()) {
                    break;
                } else if(next.getLevel() > node.getLevel()) {  // child node
                    copy.add(copy(next));
                    // node already contains child
                } else {                                        // sibling
                    copies.add(copy(next));
                    toRemove.add(next);
                }
            }
            IconLabelNode[] nodes = copies.toArray(new IconLabelNode[copies.size()]);
            return new NodesTransferable(nodes);
        }
        return null;
    }

    /** Defensive copy used in createTransferable. */
    private IconLabelNode copy(IconLabelNode node) {
        return (IconLabelNode) node.clone();
    }

    public int getSourceActions(JComponent c) {
        return LINK;
    }

    public class NodesTransferable implements Transferable {
        IconLabelNode[] nodes;

        public NodesTransferable(IconLabelNode[] nodes) {
            this.nodes = nodes;
         }

        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
            if(!isDataFlavorSupported(flavor))
                throw new UnsupportedFlavorException(flavor);
            return nodes;
        }

        public DataFlavor[] getTransferDataFlavors() {
            return flavors;
        }

        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return nodesFlavor.equals(flavor);
        }
    }
}