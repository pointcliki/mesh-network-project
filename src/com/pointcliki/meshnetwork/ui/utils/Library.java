package com.pointcliki.meshnetwork.ui.utils;

import java.io.File;
import java.util.ArrayList;

import com.pointcliki.meshnetwork.application.Application;
import com.pointcliki.meshnetwork.application.ConsumerApplication;
import com.pointcliki.meshnetwork.application.ProducerConsumerApplication;
import com.pointcliki.meshnetwork.model.Script;
import com.pointcliki.meshnetwork.model.ScriptType;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolGroup;
import com.pointcliki.meshnetwork.protocol.accack.AccACK;
import com.pointcliki.meshnetwork.protocol.aodv.AODV;
import com.pointcliki.meshnetwork.protocol.aomp.AOMP;
import com.pointcliki.meshnetwork.protocol.dv.DistanceVector;
import com.pointcliki.meshnetwork.protocol.dvmrp.DVMRP;
import com.pointcliki.meshnetwork.protocol.mac.MAC;
import com.pointcliki.meshnetwork.protocol.nocy.NoCy;
import com.pointcliki.meshnetwork.protocol.recovery.Recovery;
import com.pointcliki.meshnetwork.protocol.rob.RetryOnBusy;
import com.pointcliki.meshnetwork.protocol.search.Search;
import com.pointcliki.meshnetwork.protocol.srm.SRM;
import com.pointcliki.meshnetwork.protocol.tcp.TCP;
import com.pointcliki.meshnetwork.protocol.ttl.TTL;

public class Library {
	
	private static ArrayList<ProtocolGroup> fProtocols; 

	public static ArrayList<ProtocolGroup> protocols() {
		if (fProtocols != null) return fProtocols;
		fProtocols = new ArrayList<ProtocolGroup>();
		
		// Create the protocol groups
		
		ArrayList<Protocol> list = new ArrayList<Protocol>();
		list.add(new MAC());
		//list.add(new AndroidMAC());
		fProtocols.add(new ProtocolGroup("Link Layer Protocols", list));
		
		list = new ArrayList<Protocol>();
		list.add(new DistanceVector());
		list.add(new AODV());
		list.add(new Search());
		fProtocols.add(new ProtocolGroup("Routing Protocols", list));
		
		list = new ArrayList<Protocol>();
		list.add(new DVMRP());
		list.add(new AOMP());
		fProtocols.add(new ProtocolGroup("Multicast Protocols", list));
		
		list = new ArrayList<Protocol>();
		list.add(new TTL());
		list.add(new NoCy());
		fProtocols.add(new ProtocolGroup("Termination Protocols", list));
		
		list = new ArrayList<Protocol>();
		list.add(new RetryOnBusy());
		list.add(new Recovery());
		fProtocols.add(new ProtocolGroup("Recovery Protocols", list));
		
		list = new ArrayList<Protocol>();
		list.add(new TCP());
		list.add(new SRM());
		fProtocols.add(new ProtocolGroup("Transport Protocols", list));
		
		list = new ArrayList<Protocol>();
		//list.add(new Sluice());
		//list.add(new AODV2());
		//list.add(new AOMP2());
		list.add(new AccACK());
		fProtocols.add(new ProtocolGroup("Loss Reduction Protocols", list));
		
		list = new ArrayList<Protocol>();
		//list.add(new TierBinder());
		fProtocols.add(new ProtocolGroup("Hierarchy Protocols", list));
		
		list = new ArrayList<Protocol>();
		list.add(new Application());
		list.add(new ConsumerApplication());
		list.add(new ProducerConsumerApplication());
		//list.add(new AndroidAppSocket());
		fProtocols.add(new ProtocolGroup("Application Protocols", list));
		return fProtocols;
	}

	public static ArrayList<Script> layoutScripts() {
		ArrayList<Script> arr = new ArrayList<Script>();
		File dir = new File("layouts");
		if (dir.isDirectory()) {
			for (String s: dir.list()) {
				arr.add(new Script(s, ScriptType.Layout));
			}
		}
		return arr;
	}
	public static ArrayList<Script> mobilityScripts() {
		ArrayList<Script> arr = new ArrayList<Script>();
		File dir = new File("mobility");
		if (dir.isDirectory()) {
			for (String s: dir.list()) {
				arr.add(new Script(s, ScriptType.Mobility));
			}
		}
		return arr;
	}
	public static ArrayList<Script> flowScripts() {
		ArrayList<Script> arr = new ArrayList<Script>();
		File dir = new File("flows");
		if (dir.isDirectory()) {
			for (String s: dir.list()) {
				arr.add(new Script(s, ScriptType.Flow));
			}
		}
		return arr;
	}
}
