package com.pointcliki.meshnetwork.ui.utils;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.SimulationSubItem;
import com.pointcliki.meshnetwork.ui.panel.ConfigurationPanel;
import com.pointcliki.meshnetwork.ui.panel.TaskPanel;

public class SimulationsTreeSelectorListener implements TreeSelectionListener {

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		SimulationItem sim = (SimulationItem) (((IconLabelNode) e.getPath().getPathComponent(1)).item());
		Item item = ((IconLabelNode) e.getPath().getLastPathComponent()).item();
		
		if (item instanceof SimulationSubItem || item instanceof SimulationItem) {
			item = null;
		}
		
		ConfigurationPanel.getInstance().setItem(sim, item);
		TaskPanel.getInstance().setItem(sim, item);
	}

}
