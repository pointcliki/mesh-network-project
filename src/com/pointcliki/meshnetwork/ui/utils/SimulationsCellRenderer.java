package com.pointcliki.meshnetwork.ui.utils;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.pointcliki.meshnetwork.model.SimulationItem;

public class SimulationsCellRenderer extends DefaultTreeCellRenderer {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -1836977965439036376L;
	
	private static final Border border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		JLabel l = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		l.setBorder(border);
		
		if (value instanceof SimulationItem) {
			l.setText(((SimulationItem) value).name());
			l.setIcon(Icons.PlayIcon);
		} else if (value instanceof IconLabelNode) {
			IconLabelNode n = (IconLabelNode) value;
			l.setText(n.name());
			l.setIcon(n.icon());
		}
		return l;
	}
}
