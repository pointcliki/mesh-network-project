package com.pointcliki.meshnetwork.ui.utils;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Icons {
	public static final Icon PlayIcon = new ImageIcon("lib/icon_play.png");
	public static final Icon BooksIcon = new ImageIcon("lib/icon_books.png");
	public static final Icon StackIcon = new ImageIcon("lib/icon_stack.png");
	public static final Icon ScriptIcon = new ImageIcon("lib/icon_script.png");
	public static final Icon ConfigurationIcon = new ImageIcon("lib/icon_cog.png");
	public static final Icon ProtocolIcon = new ImageIcon("lib/icon_protocols.png");
	public static final Icon EventIcon = new ImageIcon("lib/icon_event.png");
	public static final Icon MetricIcon = new ImageIcon("lib/icon_chart.png");
	public static final Icon DrawIcon = new ImageIcon("lib/icon_draw.png");
}
