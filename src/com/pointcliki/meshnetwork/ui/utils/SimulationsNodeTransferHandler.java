package com.pointcliki.meshnetwork.ui.utils;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.Metric;
import com.pointcliki.meshnetwork.model.SimulationSubItem;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.protocol.ProtocolGroup;
import com.pointcliki.meshnetwork.model.Script;

public class SimulationsNodeTransferHandler extends LibraryNodeTransferHandler {
	
    /**
	 * Serial key
	 */
	private static final long serialVersionUID = 5028148428256098362L;

    public SimulationsNodeTransferHandler() {
        try {
            String mimeType = DataFlavor.javaJVMLocalObjectMimeType + ";class=\"" +
                com.pointcliki.meshnetwork.ui.utils.IconLabelNode[].class.getName() + "\"";
            nodesFlavor = new DataFlavor(mimeType);
            flavors[0] = nodesFlavor;
        } catch(ClassNotFoundException e) {
            System.out.println("ClassNotFound: " + e.getMessage());
        }
    }

    @Override
    public boolean canImport(TransferHandler.TransferSupport support) {
    	  	
    	support.setShowDropLocation(true);
    	
        if(!support.isDataFlavorSupported(nodesFlavor)) {
            return false;
        }
        
        try {
			IconLabelNode[] nodes = (IconLabelNode[])(support.getTransferable().getTransferData(nodesFlavor));
			
			for (IconLabelNode n: nodes) {
				if (n.item() instanceof ProtocolGroup) return false;
			}
			
		} catch (UnsupportedFlavorException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return true;
    }

    @SuppressWarnings("unchecked")
	@Override
    public boolean importData(TransferHandler.TransferSupport support) {
        if(!canImport(support)) {
            return false;
        }
        // Extract transfer data
        try {
            Transferable t = support.getTransferable();
            IconLabelNode[] nodes = (IconLabelNode[])t.getTransferData(nodesFlavor);
            
            JTree tree = (JTree) support.getComponent();
            DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
            
            // Get drop location info.
            JTree.DropLocation dl = (JTree.DropLocation)support.getDropLocation();            
            TreePath dest = dl.getPath();
            
            if (dest == null) return false;
            
            IconLabelNode parent = (IconLabelNode) dest.getLastPathComponent();
                        
            for (IconLabelNode n: nodes) {
            	
            	IconLabelNode grp = null;
            	int index = parent.getParent().getIndex(parent);
            	
            	if (n.item() instanceof ProtocolData) {
            		
            		grp = (IconLabelNode) ((IconLabelNode) dest.getPathComponent(1)).getChildAt(0);
            	
            	} else if (n.item() instanceof Script) {
            		grp = (IconLabelNode) ((IconLabelNode) dest.getPathComponent(1)).getChildAt(1);
            		
            	} else if (n.item() instanceof Annotation) {
            		grp = (IconLabelNode) ((IconLabelNode) dest.getPathComponent(1)).getChildAt(2);
            		
            	} else if (n.item() instanceof Event) {
            		grp = (IconLabelNode) ((IconLabelNode) dest.getPathComponent(1)).getChildAt(3);
            		
            	} else if (n.item() instanceof Metric) {
            		grp = (IconLabelNode) ((IconLabelNode) dest.getPathComponent(1)).getChildAt(4);
            		
            	}
            	
            	if (grp == null) return false;
            	
            	if (parent.getParent() instanceof IconLabelNode && ((IconLabelNode) parent.getParent()).item() instanceof SimulationSubItem) {
            		index = Math.min(parent.getParent().getIndex(parent), grp.getChildCount());
            	} else {
            		index = grp.getChildCount();
            	}
            	
            	((SimulationSubItem) (grp.item())).add(model, grp, n.item().clone(), index);
            	tree.expandPath(new TreePath(grp.getPath()));
            }
            
            
        } catch(Exception e) {
            e.printStackTrace();
        }
       
        return true;
    }
}