package com.pointcliki.meshnetwork.ui.utils;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.ResultsItem;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.ui.panel.ConfigurationPanel;
import com.pointcliki.meshnetwork.ui.panel.DataPanel;
import com.pointcliki.meshnetwork.ui.panel.GraphPanel;
import com.pointcliki.meshnetwork.ui.panel.DataPanel;
import com.pointcliki.meshnetwork.ui.panel.ResultsPanel;
import com.pointcliki.meshnetwork.ui.panel.TaskPanel;

public class ResultsTreeSelectorListener implements TreeSelectionListener {

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		ResultsItem res = (ResultsItem) (((IconLabelNode) e.getPath().getPathComponent(1)).item());
		Item item = ((IconLabelNode) e.getPath().getLastPathComponent()).item();
		
		if (e.getPath().getPath().length > 2) {
			Item item2 = ((IconLabelNode) e.getPath().getLastPathComponent()).item();
			
			if (item instanceof SimulationItem) {
				SimulationItem sim = (SimulationItem) item;
				ConfigurationPanel.getInstance().setItem(sim, item2, false);
			} else {
				ConfigurationPanel.getInstance().setItem(null, item2, false);
			}
			
			
		} else {
			ConfigurationPanel.getInstance().setItem(null, null, false);
		}

		DataPanel.getInstance().setItem(res, item);
		DataPanel.getInstance().setItem(res, item);
		GraphPanel.getInstance().setItems(ResultsPanel.getInstance().tree().getSelectionPaths());
		TaskPanel.getInstance().setItem(res);
	}

}
