package com.pointcliki.meshnetwork.protocol;

import java.util.HashMap;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.model.EventInstance;
import com.pointcliki.meshnetwork.model.IEventListener;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.simulator.IReceiver;
import com.pointcliki.meshnetwork.simulator.ISender;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

public abstract class Protocol implements ISender, IReceiver {
	
	protected Protocol fUpStream;
	protected Protocol fDownStream;
	private ProtocolData fData;
	private Node fNode;
	private HashMap<String, IEventListener<? extends Protocol>> fEvents = new HashMap<String, IEventListener<? extends Protocol>>();
	
	public Protocol() {}
	
	public Protocol(Node n, ProtocolData data, Protocol downstream) {
		fNode = n;
		fData = data;
		fDownStream = downstream;
		if (downstream != null) downstream.fUpStream = this;
		
		createEventListeners();
		createMessageColorers();
	}
	
	protected void createEventListeners() {};
	
	protected void createMessageColorers() {};
	
	public Protocol downstream() {
		return fDownStream;
	}

	@SuppressWarnings("unchecked")
	public <V> V downstream(Class<V> type) {
		if (type.isInstance(fDownStream)) return (V) fDownStream;
		return fDownStream.downstream(type);
	}
	
	public void startup() {
		fUpStream.startup();
	}
	
	public Node node() {
		return fNode;
	}
	
	public Simulator simulator() {
		return fNode.simulator();
	}
	
	public SimulationItem simulation() {
		return simulator().simulation();
	}
	
	@SuppressWarnings("unchecked")
	public <P extends Protocol> void recieveEvent(EventInstance e) {
		if (fEvents.containsKey(e.name())) {
			((IEventListener<P>) (fEvents.get(e.name()))).handleEvent((P) this, e);
		}
		fire(e);
	}
	
	protected <P extends Protocol> void setEventHandler(String name, IEventListener<P> r) {
		fEvents.put(name, r);
	}
	
	public boolean receive(String source, Message m) {
		return fUpStream.receive(source, m);
	}
	
	public abstract String name();
	
	public void tick(long frame) {
		if (fUpStream != null) fUpStream.tick(frame);
	}
	
	protected abstract ProtocolData createData();
	
	public ProtocolData data() {
		if (fData == null) fData = createData();
		return fData;
	}
	
	public boolean send(String destination, Message m) {
		return fDownStream.send(destination, m);
	}

	public void finish() {}

	public Protocol upstream() {
		return fUpStream;
	}
	
	@SuppressWarnings("unchecked")
	public <V> V upstream(Class<V> type) {
		if (type.isInstance(fUpStream)) return (V) fUpStream;
		return fUpStream.upstream(type);
	}
	
	public void fire(String type, float value) {
		fire(new EventInstance(type, simulator().frame(), value));
	}
	
	protected void fire(EventInstance e) {
		if (fUpStream != null) fUpStream.recieveEvent(e);
		else simulation().contributeToEvent(e);
	}
}
