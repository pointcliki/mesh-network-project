package com.pointcliki.meshnetwork.protocol.dv;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.TaggedChannelMessage;

public class DVPayloadMessage extends TaggedChannelMessage {
	public DVPayloadMessage(String source, String destination, int number, Message m) {
		super(source, destination, number, m);
	}
	
	@Override
	public String toString() {
		return "[DV payload message from " + source() + " to " + destination() + " of size " + size() + "] " + (payload() != null ? " " + payload().toString() : ""); 
	}
}

