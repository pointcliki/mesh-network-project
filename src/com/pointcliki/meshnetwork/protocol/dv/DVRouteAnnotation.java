package com.pointcliki.meshnetwork.protocol.dv;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.utils.Forward;

public class DVRouteAnnotation extends Annotation {

	public final static String NAME = "[DV] Routes";
	
	public DVRouteAnnotation(Protocol p) {
		super(NAME, p);
	}
	
	@Override
	public void render(Simulator sim, Graphics g, long time) {
		g.setLineWidth(0.2f);
		for (Node n: sim.nodes().values()) {
			DistanceVector dv = n.upstream(DistanceVector.class);
			for (Forward f: dv.fTable.values()) { 
				if (f.distance == 0) continue;
				Node m = sim.nodes().get(f.destination);
				if (m != null) {
					g.setColor(new Color(100, 0, 100).scaleCopy(1f / f.distance));
					g.drawLine(n.position().x, n.position().y, m.position().x, m.position().y);
				}
			}
		}
	}
}
