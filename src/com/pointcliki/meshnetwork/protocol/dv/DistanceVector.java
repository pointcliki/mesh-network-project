package com.pointcliki.meshnetwork.protocol.dv;

import java.util.HashMap;
import java.util.Map.Entry;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.protocol.mac.MAC;
import com.pointcliki.meshnetwork.service.IForwardingService;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.service.IMessageColourerService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.utils.Forward;

public class DistanceVector extends Protocol implements IForwardingService {
	
	protected boolean fSettled;
	protected HashMap<String, Forward> fTable;
	protected HashMap<String, Forward> fDelta;
	private int fBeacon;
	private String fLastHop;
	
	private int fTag = 0;
	
	public DistanceVector() {
		super();
	}

	public DistanceVector(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fBeacon = data().beacon();
	}
	
	@Override
	public void startup() {
		super.startup();
		
		// Initialise empty routing table
		fTable = new HashMap<String, Forward>();
		fDelta = new HashMap<String, Forward>();
		// Add local nodes
		Node n = node();
		fTable.put(n.name(), new Forward(n.name(), n.name(), 0));
		for (Node n2: downstream(ILinkLayerService.class).localNodes()) if (n2.upstream(ILinkLayerService.class).state() != MAC.OFFLINE) fTable.put(n2.name(), new Forward(n2.name(), n2.name(), 1));
		// Broadcast information
		fDownStream.send("broadcast", new DVControlMessage(fTable, true));
	}

	@Override
	public boolean receive(String source, Message m) {
		fLastHop = source;
		
		if (m instanceof DVControlMessage) {
			handleControlMessage(source, (DVControlMessage) m);
			return true;
		} else if (m instanceof DVPayloadMessage) {
			handlePayloadMessage(source, (DVPayloadMessage) m);
			return true;
		}
		return fUpStream.receive(source, m);
	}

	private void handlePayloadMessage(String source, DVPayloadMessage m) {
		// Check that the payload is for this node
		if (m.destination().equals("broadcast") || m.destination().equals(node().name())) fUpStream.receive(m.source(), m.payload());
		// Repeat the message
		else repeat(m);
	}

	private void handleControlMessage(String source, DVControlMessage m) {
		
		boolean redo = m.resync && fTable.size() > 0;
		
		for (Entry<String, Forward> e: m.forwards.entrySet()) {
			// If we don't have this node in our forwarding table or there's a shorter route
			if (!fTable.containsKey(e.getKey()) || e.getValue().distance + 1 < fTable.get(e.getKey()).distance) {
				Forward f = new Forward(e.getValue().destination, source, e.getValue().distance + 1);
				fTable.put(f.destination, f);
				fDelta.put(f.destination, f);
				
				simulator().log(Simulator.LOG_ROUTES, "[DV] New route from " + node().name() + " to " + f.destination + " in " + f.distance);
			}
		}
		// Re-sync
		if (redo) fDownStream.send(source, new DVControlMessage(fTable));
	}

	@Override
	public boolean send(String dest, Message m) {
		// Check whether a lower layer can handle the request
		IForwardingService s = downstream(IForwardingService.class);
		if (s != null && s.route(dest)) {
			return fDownStream.send(dest, m);
		}
		
		return repeat(new DVPayloadMessage(node().name(), dest, fTag++, m));
	}
	
	private boolean repeat(DVPayloadMessage m) {
		if (m.destination() == null) return false;
		// Check for broadcast
		if (m.destination().equals("broadcast")) {
			fDownStream.send("broadcast", m);
			return true;
		}
				
		// Route through next hop
		if (fTable.containsKey(m.destination())) {
			String d = fTable.get(m.destination()).nextHop;
			fDownStream.send(d, m);
			return true;
			
		} else {
			//System.err.println("[Distance Vector] Network Busy");
			return false;	
		}
	}
	
	@Override
	public void tick(long frame) {
		super.tick(frame);
		if (fBeacon == 0 || frame % fBeacon == 0) {
			data().iterating();
						
			if (!fDelta.isEmpty()) {
				// Broadcast information
				if (fDownStream.send("broadcast", new DVControlMessage(fDelta))) {
					// Reset changes
					fDelta.clear();
				}
				data().incrementDelta();
			}
		}
		data().contributeLinks(fTable.size());
	}
	
	public DistanceVectorData data() {
		return (DistanceVectorData) super.data();
	}

	@Override
	public String name() {
		return "DistanceVector";
	}

	@Override
	public String nextHop(String destination) {
		if (fTable.containsKey(destination)) return fTable.get(destination).nextHop;
		return null;
	}

	@Override
	public int distance(String destination) {
		if (fTable.containsKey(destination)) return fTable.get(destination).distance;
		return -1;
	}

	@Override
	public boolean route(String destination) {
		return fTable.containsKey(destination);
	}

	@Override
	protected ProtocolData createData() {
		return new DistanceVectorData(this);
	}

	@Override
	protected void createMessageColorers() {
		IMessageColourerService s = downstream(IMessageColourerService.class);
		s.addColorer(DVControlMessage.class, Color.orange);
	}

	@Override
	public String receivedHop() {
		return fLastHop;
	}
}
