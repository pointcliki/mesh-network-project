package com.pointcliki.meshnetwork.protocol.dv;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.EventInstance;
import com.pointcliki.meshnetwork.model.Metric;
import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class DistanceVectorData extends ProtocolData {

	private static final String DV_BEACON = "[DV] Beacon";
	private static final String DV_PAIRS = "[DV] Pairwise Links";
	private static final String DV_DELTA = "[DV] Link Delta";
	private static final String DV_LINKED = "[DV] All Linked";
	private static final String DV_ENDED = "[DV] Algorithm End";
	
	private int fLinks;
	private int fChanged;
	private boolean fIteration;
	private boolean fComplete;
	private boolean fTerminated;
	
	public DistanceVectorData(Protocol p) {
		super(p);
		annotations().add(new DVRouteAnnotation(p));
		metrics().add(new Metric(DV_PAIRS, p));
		metrics().add(new Metric(DV_DELTA, p));
		events().add(new Event("[DV] Algorithm Begin", p));
		events().add(new Event(DV_LINKED, p));
		events().add(new Event(DV_ENDED, p));
		
		options().add(new Option<Integer>(DV_BEACON, "frame duration", 10));
	}
	
	@Override
	public void startup() {
		//Grapher g = simulator().createGraph("dv", "Distance Vector Links");
		//g.addLine(new Color(0, 180, 0));
		//g.addLine(new Color(0, 180, 180));
	}
	
	@Override
	public void finish() {
		
	}

	@Override
	public void beginTick() {
		fLinks = 0;
		fChanged = 0;
		fIteration = false;
	}
	
	@Override
	public void endTick() {
		if (fIteration && fLinks == (int) Math.pow(simulator().nodes().size(), 2) && !fComplete) {
			fComplete = true;
			simulator().log(Simulator.LOG_ROUTES, "[Distance Vector] All nodes linked at frame " + simulator().frame());
			simulation().contributeToEvent(new EventInstance(DV_LINKED, simulator().frame(), fLinks / simulator().nodes().size()));
		}
		if (fIteration && fChanged == 0 && fComplete && !fTerminated) {
			fTerminated = true;
			simulator().log(Simulator.LOG_ROUTES, "[Distance Vector] Algorithm terminated at frame " + simulator().frame());
			simulation().contributeToEvent(new EventInstance(DV_ENDED, simulator().frame(), fLinks / simulator().nodes().size()));
		}
		
		simulation().contributeToMetric(DV_PAIRS, fLinks / simulator().nodes().size());
		simulation().contributeToMetric(DV_DELTA, fChanged);
	}
	
	public void iterating() {
		fIteration = true;
	}

	public void contributeLinks(int size) {
		fLinks += size;
	}
	public void incrementDelta() {
		fChanged++;
	}

	@Override
	public String info() {
		return "<html><b>Distance Vector</b><br/></html>";
	}
	
	public int beacon() {
		return getOption(DV_BEACON, int.class);
	}
}
