package com.pointcliki.meshnetwork.protocol.dv;

import java.util.HashMap;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.utils.Forward;

public class DVControlMessage extends Message {

	public HashMap<String, Forward> forwards;
	public boolean resync;
	
	@SuppressWarnings("unchecked")
	public DVControlMessage(HashMap<String, Forward> forwards) {
		this.forwards = (HashMap<String, Forward>) forwards.clone();
	}
		
	public DVControlMessage(HashMap<String, Forward> forwards, boolean resync) {
		this(forwards);
		this.resync = resync;
	}
	
	@Override
	public String toString() {
		return "[DV Control Message] " + forwards;
	}

	@Override
	public int size() {
		return 1;
	}
}
