package com.pointcliki.meshnetwork.protocol.tcp;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class TCPData extends ProtocolData {
	
	public final static String TRANSPORT_WINDOW = "[Transport] Initial Window Size";
	public final static String TRANSPORT_BUFFER = "[Transport] Queue Size";
	public final static String TRANSPORT_RETRY_LIMIT = "[Transport] Initial Retry Limit";
	public final static String TRANSPORT_RETRY_THRESHOLD = "[Transport] Retry Threshold";
	
	public final static String TRANSPORT_GOOD_ACK_EVENT = "[Transport] Good ACK";
	public final static String TRANSPORT_GHOST_ACK_EVENT = "[Transport] Ghost ACK";
	public final static String TRANSPORT_DUPLICATE_ACK_EVENT = "[Transport] Duplicate ACK";
	public final static String TRANSPORT_RECIEVED_PACKET_EVENT = "[Transport] Packet Recieved By All";
	public final static String TRANSPORT_RECIEVED_WINDOW_EVENT = "[Transport] Window Recieved By All";
	
	public final static String TRANSPORT_PACKET_RETRANSMIT = "[Transport] Packet Retransmitted";
	public final static String TRANSPORT_PACKET_SKIPPED = "[Transport] Packet Skipped";

	public TCPData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(TRANSPORT_WINDOW, "Messages per RTT", 1));
		options().add(new Option<Integer>(TRANSPORT_BUFFER, "Messages", 1000));
		options().add(new Option<Integer>(TRANSPORT_RETRY_LIMIT, "Attempts", 2));
		options().add(new Option<Integer>(TRANSPORT_RETRY_THRESHOLD, "% nodes silent", 50));
		events().add(new Event(TRANSPORT_GHOST_ACK_EVENT, p));
	}

	@Override
	public void startup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public void beginTick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void endTick() {
		// TODO Auto-generated method stub

	}

	@Override
	public String info() {
		return "The TCP protocol provides a reliable connection between a host and its multicast group, such that a certain percentage of participants are guaranteed receive all the packets sent."; 
	}
	
	public int retryLimit() {
		return getOption(TRANSPORT_RETRY_LIMIT, int.class);
	}
	
	public int retryThreshold() {
		return getOption(TRANSPORT_RETRY_THRESHOLD, int.class);
	}
	
	public int initialWindowSize() {
		return getOption(TRANSPORT_WINDOW, int.class);
	}
	
	public int bufferSize() {
		return getOption(TRANSPORT_BUFFER, int.class);
	}
}

