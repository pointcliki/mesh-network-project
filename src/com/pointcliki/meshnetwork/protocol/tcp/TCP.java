package com.pointcliki.meshnetwork.protocol.tcp;

import java.util.Set;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IMulticastForwardingService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.transport.IClient;
import com.pointcliki.meshnetwork.transport.TransportProtocol;

public class TCP extends TransportProtocol {
	
	public TCP() {}
	
	public TCP(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
	}
	@Override
	public boolean send(String dest, Message m) {
		if (server(dest) == null) return false;
		return server(dest).send(dest, m);
	}

	@Override
	public String name() {
		return "TCP";
	}

	@Override
	protected TCPData createData() {
		return new TCPData(this);
	}
	
	@Override
	public TCPData data() {
		return (TCPData) super.data();
	}

	@Override
	public TCPClient client(String dest) {
		return (TCPClient) super.client(dest);
	}

	@Override
	public TCPServer server(String dest) {
		return (TCPServer) super.server(dest);
	}
	
	@Override
	protected TCPServer createServer(String c) {
		// Create a new TCPServer. In the simple model
		// we assume we are connecting to the group
		// attributed to this node
		String group = "&" + node().name();
		String connection = "!" + node().name();
		return new TCPServer(this, connection, group, data());
	}

	@Override
	public IClient createClient(String c) {
		return new TCPClient(this, c, "&" + c.substring(1), data());
	}

	@Override
	public Set<String> participants(String connection) {
		// In our simple model we only allow a single connection based on the node's group
		return downstream(IMulticastForwardingService.class).routeParticipants("&" + node().name());
	}
}

