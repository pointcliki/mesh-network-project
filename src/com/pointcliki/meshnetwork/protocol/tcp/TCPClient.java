package com.pointcliki.meshnetwork.protocol.tcp;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.transport.IClient;
import com.pointcliki.meshnetwork.transport.TransportAckMessage;
import com.pointcliki.meshnetwork.transport.TransportPayloadMessage;

public class TCPClient implements IClient {
	
	private TCP fParent;
	private String fDest;
	private String fConnection;
	
	public TCPClient(TCP parent, String connection, String dest, TCPData data) {
		fParent = parent;
		fConnection = connection;
		fDest = dest;
	}

	public String source() {
		return fParent.node().name();
	}
	
	@Override
	public String destination() {
		return fDest;
	}
	@Override
	public String connection() {
		return fConnection;
	}
	
	@Override
	public boolean send(String dest, Message m) {
		// TransportSocket is a one way socket
		return false;
	}

	@Override
	public boolean receive(String source, Message m) {
		if (m instanceof TransportPayloadMessage) {
			TransportPayloadMessage tm = (TransportPayloadMessage) m;
			
			boolean okay = fParent.upstream().receive(source, m);
			
			// Send an ACK message back to the origin if we deliver it upstream correctly
			if (okay) fParent.downstream().send(fDest, new TransportAckMessage(fConnection, tm.sequenceNumber()));
			
			return okay;
		}
		
		return fParent.upstream().receive(source, m);
	}
}

