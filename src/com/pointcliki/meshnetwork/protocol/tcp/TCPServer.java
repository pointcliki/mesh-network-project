package com.pointcliki.meshnetwork.protocol.tcp;

import java.util.HashMap;
import java.util.Set;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.transport.TransportAckMessage;
import com.pointcliki.meshnetwork.transport.TransportWindow;

/**
 * A transport server is a class that represents a transport-layer
 * mediator of packet flow out of the node based on the rates at
 * which the clientelle are receiving the packets. 
 * 
 * @author Hugheth
 */

public class TCPServer extends TransportWindow {

	private HashMap<Integer, Set<String>> fAcks;
	
	public TCPServer(TCP parent, String connection, String dest, TCPData data) {
		super(parent, connection, dest, data.initialWindowSize(), data.retryLimit(), data.retryThreshold());
		fAcks = new HashMap<Integer, Set<String>>();
	}

	@Override
	public boolean receive(String source, Message m) {
		// Server only handles ack messages
		if (m instanceof TransportAckMessage) {
			TransportAckMessage ack = (TransportAckMessage) m;
			if (fAcks.get(ack.sequenceNumber()) != null) {
				Set<String> set = fAcks.get(ack.sequenceNumber());
				// The set contains all the acks we're waiting for, so check we are indeed waiting for it and remove it
				if (set.contains(source)) {
					// Remove acks from the set until none are left
					set.remove(source);
					
					fire(TCPData.TRANSPORT_GOOD_ACK_EVENT, 1);
					
					// Check whether the window can slide along
					checkWindow();
					
				} else {
					fire(TCPData.TRANSPORT_DUPLICATE_ACK_EVENT, 1);
				}
			} else {
				fire(TCPData.TRANSPORT_GOOD_ACK_EVENT, 1);
			}
			
		}
		return false;
	}
	
	@Override
	public boolean completed(int sequenceNumber) {
		return (fAcks.get(sequenceNumber) != null && fAcks.get(sequenceNumber).isEmpty());
	}
	
	@Override
	public boolean skippable(int sequenceNumber) {
		
		Set<String> s = fAcks.get(sequenceNumber);
		
		// Use count of current participants for approximation of interested parties
		// as retransmit / skip affects the current participants rather than old ones
		int num = parent().participants(connection()).size();
		
		// Check for met threshold
		// Size of s represents the number of ACKs we're still waiting for.
		// If we are waiting on a proportion less than the threshold, skip a packet.
		return (s.size() < num * (retryThreshold() / 100f));
	}
	
	protected void onTransmit(int sequenceNumber) {
		// When we transmit a packet fill the ACK set with the ACKs we expect
		fAcks.put(nextSequenceNumber(), parent().participants(connection()));
	}
	
	// Fire when a participant leaves the node's group so that we don't wait for any of its ACKs
	public void onLeave(String participant) {
		// Remove from all pending instructions
		for (Set<String> s: fAcks.values()) {
			s.remove(participant);
		}
		// See if this means we can send another packet
		checkWindow();
	}
}
