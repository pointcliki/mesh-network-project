package com.pointcliki.meshnetwork.protocol.search;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IMessageColourerService;
import com.pointcliki.meshnetwork.service.IResourceDiscoveryService;
import com.pointcliki.meshnetwork.service.IResourceRegistrarService;
import com.pointcliki.meshnetwork.simulator.Node;

public class Search extends Protocol implements IResourceDiscoveryService, IResourceRegistrarService {
	
	private HashMap<String, HashSet<String>> fProviders;
	private HashMap<String, SearchRequest> fRequests;
	private HashSet<String> fResources;

	public Search() {}
	
	public Search(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fProviders = new HashMap<String, HashSet<String>>();
		fResources = new HashSet<String>();
	}
	
	@Override
	protected void createMessageColorers() {
		IMessageColourerService s = downstream(IMessageColourerService.class);
		s.addColorer(SearchFloodMessage.class, Color.pink);
		s.addColorer(SearchResponseMessage.class, Color.cyan);
	}

	@Override
	public boolean receive(String source, Message m) {
		if (m instanceof SearchFloodMessage) {
			handleFloodMessage((SearchFloodMessage) m);
			return true;
		}
		
		if (m instanceof SearchResponseMessage) {
			handleResponseMessage(source, (SearchResponseMessage) m);
			return true;
			
		}
		return fUpStream.receive(source, m);
	}

	private void handleResponseMessage(String source, SearchResponseMessage m) {
		
		String r = m.resource();
		
		if (!fProviders.containsKey(r)) fProviders.put(r, new HashSet<String>(1));
		
		HashSet<String> providers = fProviders.get(r);
		
		providers.add(source);
		
		// Check whether we should close the request
		if (fRequests.containsKey(r)) {
			
			SearchRequest sr = fRequests.get(r);
			
			if (sr.maxCount() <= providers.size())
				endRequest(r);
		}
	}

	private void handleFloodMessage(SearchFloodMessage m) {
		if (fResources.contains(m.resource())) {
			fDownStream.send(m.origin(), new SearchResponseMessage(m.resource()));
		}
		
		// Broadcast on as origin may want more resource providers
		fDownStream.send("broadcast", m);
	}

	@Override
	public String name() {
		return "Search";
	}

	@Override
	protected ProtocolData createData() {
		return new SearchData(this);
	}
	
	@Override
	public SearchData data() {
		return (SearchData) super.data();
	}

	@Override
	public Set<String> providers(String resource) {
		if (!fProviders.containsKey(resource))
			fProviders.put(resource, new HashSet<String>(1));
		
		return fProviders.get(resource);
	}

	@Override
	public void findProviders(String resource, int count, int maxRange) {
		// Check whether we're already searching
		if (fRequests.containsKey(resource)) return;
		
		// Else create a new one
		fRequests.put(resource, new SearchRequest(this, resource, count, maxRange));
	}
	
	@Override
	public void tick(long frame) {
		// Tick through requests
		for (SearchRequest r: fRequests.values()) r.tick(frame);
		
		super.tick(frame);
	}

	public void endRequest(String resource) {
		fRequests.remove(resource);
	}

	@Override
	public void advertiseResource(String resource) {
		fResources.add(resource);
	}

	@Override
	public void clearResource(String resource) {
		fResources.remove(resource);
	}
}

