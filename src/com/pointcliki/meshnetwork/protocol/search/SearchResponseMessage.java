package com.pointcliki.meshnetwork.protocol.search;

import com.pointcliki.meshnetwork.message.Message;

public class SearchResponseMessage extends Message {
	
	private String fResource;
	
	public SearchResponseMessage(String resource) {
		fResource = resource;
	}

	public String resource() {
		return fResource;
	}
}
