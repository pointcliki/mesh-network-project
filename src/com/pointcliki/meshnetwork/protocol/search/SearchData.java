package com.pointcliki.meshnetwork.protocol.search;

import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class SearchData extends ProtocolData {

	public static final String SEARCH_EXPAND = "[Search] Ring expand factor";
	public static final String SEARCH_TIMEOUT_FACTOR = "[Search] Timeout factor";

	public SearchData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(SEARCH_EXPAND, "radius", 2));
		options().add(new Option<Float>(SEARCH_TIMEOUT_FACTOR, "ratio to diameter", 2f));
	}

	@Override
	public void startup() {

	}

	@Override
	public void finish() {

	}

	@Override
	public void beginTick() {

	}

	@Override
	public void endTick() {

	}

	@Override
	public String info() {
		return "";
	}
	
	public int expandFactor() {
		return getOption(SEARCH_EXPAND, int.class);
	}
	
	public float timeoutFactor() {
		return getOption(SEARCH_TIMEOUT_FACTOR, int.class);
	}
}
