package com.pointcliki.meshnetwork.protocol.search;

import com.pointcliki.meshnetwork.protocol.ttl.ITimeToLiveService;

public class SearchRequest {
	
	private Search fParent;
	private long fTimeout = 0;
	private int fCurrentRange = 0;
	private int fMaxRange;
	private int fMaxCount;
	private String fResource;
	
	public SearchRequest(Search parent, String resource, int count, int maxRange) {
		fParent = parent;
		fResource = resource;
		fMaxRange = maxRange;
	}
	
	public int maxCount() {
		return fMaxCount;
	}

	public void tick(long frame) {
		
		if (fCurrentRange == fMaxRange) fParent.endRequest(fResource);
		
		if (fTimeout == 0) {
			fCurrentRange = Math.min(fParent.data().expandFactor(), fMaxRange);
			
			// Get the timeout relative to the diameter of the ring
			fTimeout = fParent.simulator().frame() + (long) Math.ceil(fCurrentRange * 2f * fParent.data().timeoutFactor());
			
			// Indicate the message should only travel one stop
			ITimeToLiveService s = fParent.downstream(ITimeToLiveService.class);
			s.setLife(SearchFloodMessage.class, fCurrentRange);
			
			// Broadcast search message
			fParent.downstream().send("broadcast", new SearchFloodMessage(fParent.node().name(), fResource, fCurrentRange));
			
		} else {
			fTimeout--;
		}
	}
}
