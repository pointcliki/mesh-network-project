package com.pointcliki.meshnetwork.protocol.search;

import com.pointcliki.meshnetwork.message.ITaggedMessage;
import com.pointcliki.meshnetwork.message.ITraceableMessage;
import com.pointcliki.meshnetwork.message.Message;

public class SearchFloodMessage extends Message implements ITaggedMessage, ITraceableMessage {

	public String fOrigin;
	public String fResource;
	public String fTag;
	public int fDistance;
	
	public SearchFloodMessage(String origin, String resource, int number) {
		fOrigin = origin;
		fResource = resource;
		fTag = origin + " " + number;
		fDistance = 1;
	}
	
	public SearchFloodMessage(String origin, String resource, String tag, int distance) {
		fOrigin = origin;
		fResource = resource;
		fTag = tag;
		fDistance = distance;
	}
	
	@Override
	public String tag() {
		return fTag;
	}

	public String origin() {
		return fOrigin;
	}
	
	public String resource() {
		return fResource;
	}

	@Override
	public int distance() {
		return fDistance;
	}
}
