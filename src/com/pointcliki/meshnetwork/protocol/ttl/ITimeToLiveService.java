package com.pointcliki.meshnetwork.protocol.ttl;

import com.pointcliki.meshnetwork.message.ITaggedMessage;

public interface ITimeToLiveService {

	public <V extends ITaggedMessage> void setLife(Class<V> cls, int ttl);
	
	public <V extends ITaggedMessage> void clearLife(Class<V> cls);
}
