package com.pointcliki.meshnetwork.protocol.ttl;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class TTLData extends ProtocolData {

	public static final String TTL_PACKET_DIED = "[TTL] Packet died";

	public TTLData(Protocol p) {
		super(p);
		events().add(new Event(TTL_PACKET_DIED, p));
	}

	@Override
	public void startup() {
		
	}

	@Override
	public void finish() {
		
	}

	@Override
	public void beginTick() {
		
	}

	@Override
	public void endTick() {
		
	}

	@Override
	public String info() {
		return "The Time To Live protocol allows a protocol to tag certain packets that it would like to assign a particular lifespan to.";
	}

}
