package com.pointcliki.meshnetwork.protocol.ttl;

import com.pointcliki.meshnetwork.message.ITaggedMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.PayloadMessage;

public class TTLPayloadMessage extends PayloadMessage implements ITaggedMessage {
	
	private int fLife;

	public TTLPayloadMessage(int life, Message m) {
		super(m);
		fLife = life;
	}
	
	public int life() {
		return fLife;
	}
	
	public String tag() {
		return ((ITaggedMessage) payload()).tag();
	}
}
