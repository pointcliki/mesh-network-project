package com.pointcliki.meshnetwork.protocol.ttl;

import java.util.HashMap;

import com.pointcliki.meshnetwork.message.ITaggedMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ttl.ITimeToLiveService;
import com.pointcliki.meshnetwork.protocol.ttl.TTLPayloadMessage;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.simulator.Node;

public class TTL extends Protocol implements ITimeToLiveService {
	
	private HashMap<Class<? extends ITaggedMessage>, Integer> fLives;
	private String fTag;
	private int fLife;

	public TTL() {}
	
	public TTL(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fLives = new HashMap<Class<? extends ITaggedMessage>, Integer>();
	}
	
	@Override
	protected void createEventListeners() {}
	
	@Override
	public boolean send(String destination, Message m) { 
		if (fLives.containsKey(m.getClass())) {
			
			int life;
			// Check whether the tag was recently received
			if (fTag != null) {
				life = fLife - 1;
			} else {
				life = fLives.get(m.getClass());
			}
			
			if (life == 0) {
				fire(TTLData.TTL_PACKET_DIED, 1);
				// Return true as we're performing the desired behaviour by dropping the packet
				return true;
			}
			
			return fDownStream.send(destination, new TTLPayloadMessage(life, m));
		} else {
			return fDownStream.send(destination, m);
		}
	}

	@Override
	public boolean receive(String source, Message m) {
		// Record
		if (m instanceof TTLPayloadMessage) {
			return handlePayload(source, (TTLPayloadMessage) m);
		}
		fTag = null;
		return fUpStream.receive(source, m);
	}

	private boolean handlePayload(String source, TTLPayloadMessage m) {
		// Check whether we're monitoring this message
		if (fLives.containsKey(m.getClass())) {
			// Record life and tag
			fLife = m.life();
			fTag = m.tag();
		} else {
			fTag = null;
		}
		
		return fUpStream.receive(source, m.payload());
	}

	@Override
	public String name() {
		return "TTL";
	}

	@Override
	protected ProtocolData createData() {
		return new TTLData(this);
	}

	@Override
	public <V extends ITaggedMessage> void setLife(Class<V> cls, int ttl) {
		fLives.put(cls,  ttl);
	}

	@Override
	public <V extends ITaggedMessage> void clearLife(Class<V> cls) {
		fLives.remove(cls);
	}

}
