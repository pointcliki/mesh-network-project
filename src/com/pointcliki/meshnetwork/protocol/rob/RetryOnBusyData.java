package com.pointcliki.meshnetwork.protocol.rob;

import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class RetryOnBusyData extends ProtocolData {

	private static final String ROB_TIMEOUT = "[ROB] Retry Timeout";
	private static final String ROB_QUEUE_SIZE = "[ROB] Buffer Size";

	public RetryOnBusyData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(ROB_TIMEOUT, "frames", 20));
		options().add(new Option<Integer>(ROB_QUEUE_SIZE, "messages", 8));
	}

	@Override
	public void startup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public void beginTick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void endTick() {
		// TODO Auto-generated method stub

	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int timeout() {
		return getOption(ROB_TIMEOUT, int.class);
	}
	public int queueSize() {
		return getOption(ROB_QUEUE_SIZE, int.class);
	}
}
