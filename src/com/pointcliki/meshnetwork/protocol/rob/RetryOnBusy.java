package com.pointcliki.meshnetwork.protocol.rob;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.utils.MessageQueue;

public class RetryOnBusy extends Protocol {
	
	private int fTimeout;
	private MessageQueue<RetryOnBusyMessage> fDownBuffer;
	private MessageQueue<RetryOnBusyMessage> fUpBuffer;
	
	private long fExpire = 0;
	private long fDownAttempt = 0;
	private long fUpAttempt = 0;
	
	public RetryOnBusy() {}

	public RetryOnBusy(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fTimeout = data().timeout();
		fDownBuffer = new MessageQueue<RetryOnBusyMessage>(data().queueSize());
		fUpBuffer = new MessageQueue<RetryOnBusyMessage>(data().queueSize());
	}

	@Override
	public boolean send(String dest, Message m) {
		if (!fDownStream.send(dest, m)) {
			if (fDownBuffer.full()) return false;
			fDownBuffer.push(new RetryOnBusyMessage(dest, m));
			
			simulator().log(Simulator.LOG_NODES, "" + node().name() + " queued send for later: " + m);
			
			if (fExpire == 0) {
				fExpire = simulator().frame() + fTimeout;
				fDownAttempt = simulator().frame() + 1;
			}
		}
		return true;
	}

	@Override
	public ProtocolData createData() {
		return new RetryOnBusyData(this);
	}

	@Override
	public void startup() {
		super.startup();
		fDownBuffer.reset();
		fUpBuffer.reset();
	}

	@Override
	public boolean receive(String source, Message m) {
		if (!fUpStream.receive(source, m)) {
			if (fUpBuffer.full()) return false;
			fUpBuffer.push(new RetryOnBusyMessage(source, m));
			
			simulator().log(Simulator.LOG_NODES, "" + node().name() + " queued delivery for later: " + m);
			
			if (fExpire == 0) {
				fExpire = simulator().frame() + fTimeout;
				fUpAttempt = simulator().frame() + 1;
			}
		}
		return true;
	}

	@Override
	public String name() {
		return "RetryOnBusy";
	}
	
	@Override
	public void tick(long frame) {
		super.tick(frame);
		
		if (!fDownBuffer.empty()) {
			if (fExpire == frame) {
				fDownBuffer.poll();
				fExpire += fTimeout;
				fDownAttempt = frame + 1;
				
			} else if (fDownAttempt == frame) {
				RetryOnBusyMessage m = fDownBuffer.peek();
				
				if (fDownStream.send(m.left(), m.message())) {
					fDownBuffer.poll();
					fExpire += fTimeout;
					fDownAttempt++;
					
					simulator().log(Simulator.LOG_NODES, "" + node().name() + " sent a queued message: " + m);
					
				} else {
					fDownAttempt += 1;
				}
			}
			if (fDownBuffer.empty()) fExpire = 0;
		}
		
		if (!fUpBuffer.empty()) {
			if (fExpire == frame) {
				fUpBuffer.poll();
				fExpire += fTimeout;
				fUpAttempt = frame + 1;
				
			} else if (fUpAttempt == frame) {
				RetryOnBusyMessage m = fUpBuffer.peek();
				
				if (fDownStream.receive(m.left(), m.message())) {
					fUpBuffer.poll();
					fExpire += fTimeout;
					fUpAttempt++;
					
					simulator().log(Simulator.LOG_NODES, "" + node().name() + " received a queued message: " + m);
					
				} else {
					fUpAttempt += 1;
				}
			}
			if (fUpBuffer.empty()) fExpire = 0;
		}
	}
	
	@Override
	public RetryOnBusyData data() {
		return (RetryOnBusyData) super.data();
	}

	@Override
	protected void createEventListeners() {
		// TODO Auto-generated method stub
		
	}
}

