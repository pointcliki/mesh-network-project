package com.pointcliki.meshnetwork.protocol.rob;

import com.pointcliki.meshnetwork.message.Message;

public class RetryOnBusyMessage extends Message {

	private String fLeft;
	private Message fMessage;
	
	public RetryOnBusyMessage(String left, Message message) {
		fLeft = left;
		fMessage = message;
	}
	
	public Message message() {
		return fMessage;
	}
	
	public String left() {
		return fLeft;
	}
}
