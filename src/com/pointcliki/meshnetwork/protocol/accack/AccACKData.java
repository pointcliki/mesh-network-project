package com.pointcliki.meshnetwork.protocol.accack;

import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class AccACKData extends ProtocolData {
	
	public static final String ACC_TIMEOUT = "[AccACK] Accumulate Timeout";

	public AccACKData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(ACC_TIMEOUT, "frames", 10));
	}

	@Override
	public String info() {
		return "The Accumulate ACK protocol";
	}

	public int accumulateTimeout() {
		return getOption(ACC_TIMEOUT, int.class);
	}
}
