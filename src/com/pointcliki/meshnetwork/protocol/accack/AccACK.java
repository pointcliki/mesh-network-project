package com.pointcliki.meshnetwork.protocol.accack;

import java.util.List;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.IMulticastAckMessage;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IForwardingService;
import com.pointcliki.meshnetwork.service.IMulticastForwardingService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.transport.TransportAckMessage;
import com.pointcliki.meshnetwork.utils.LeakyMap;

public class AccACK extends Protocol {
	
	public LeakyMap<String, AccACKMessage> fAcks;
	
	public AccACK() {}
	
	public AccACK(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fAcks = new LeakyMap<String, AccACKMessage>();
	}
	
	@Override
	public boolean send(String destination, Message m) {
		if (m instanceof IMulticastAckMessage) {
			
			IMulticastAckMessage ack = (IMulticastAckMessage) m;
			
			// Find the next hop
			IForwardingService s = downstream(IForwardingService.class);
			if (s == null) return false;
			String hop = s.nextHop(destination);
			if (hop == null) return false;
			
			// Embed the ack in our own message which travels hop-by-hop
			fDownStream.send(hop, new AccACKMessage(node().name(), destination, ack));
		}
		return super.send(destination, m);
	}
	
	@Override
	public boolean receive(String source, Message m) {
		// Handle accumulating-ack message
		if (m instanceof AccACKMessage) {
			AccACKMessage acc = (AccACKMessage) m;
			String c = acc.connection();
			String group = groupForConnection(c);
			// The tag unifies AckMessages for the same sequence number and connection
			String tag = acc.tag();
			
			// Check whether the ACK is for us
			if (acc.destination().equals(node().name())) {
				boolean okay = true;
				
				// Unpack the original acks from the accumulative one and send individually 
				for (String s: acc.sources())
					okay = okay & fUpStream.receive(s, new TransportAckMessage(acc.connection(), acc.sequenceNumber()));
				
				return okay;
			}
						
			if (!fAcks.containsKey(tag))
				fAcks.putSoft(tag, acc, data().accumulateTimeout());
		
			// Update the stored acc ACK for this tag
			AccACKMessage out = fAcks.getSoft(tag);
			out.sources().addAll(acc.sources());
			
			// Check whether we've received ACKs for all the route participants of the connection
			IMulticastForwardingService s = downstream(IMulticastForwardingService.class);
			if (s == null) return false;
			
			boolean all = true;
			for (String node: s.routeParticipants(group)) {
				if (!out.sources().contains(node)) {
					all = false;
					break;
				}
			}
			
			// Forward a cumulative ack on
			if (all) {
				fAcks.remove(tag);
				repeat(out);
			}
			
		}
		
		return super.receive(source, m);
	}
	
	private boolean repeat(AccACKMessage m) {
		IForwardingService s = downstream(IForwardingService.class);
		if (s == null) return false;
		
		String hop = s.nextHop(m.destination());
		if (hop == null) return false;
		
		return fDownStream.send(hop, m);
	}
	
	/**
	 * Uses the project-specific assumption that connections map
	 * directly to groups. Could alternatively be implemented with
	 * a group-connection binding service.
	 */
	private String groupForConnection(String c) {
		return "#" + c.substring(1);
	}

	@Override
	public void tick(long frame) {
		List<String> list = fAcks.changes(frame);
		for (String tag: list) {
			// This means we've waited for long enough so forward cumulative ack
			AccACKMessage out = fAcks.getSoft(tag);
			repeat(out);			
		}
		fAcks.update(frame);
		super.tick(frame);
	}
	

	@Override
	protected void createEventListeners() {}

	@Override
	public String name() {
		return "AccACK";
	}	

	@Override
	protected ProtocolData createData() {
		return new AccACKData(this);
	}
	
	@Override
	public AccACKData data() {
		return (AccACKData) super.data();
	}

}
