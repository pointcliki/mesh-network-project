package com.pointcliki.meshnetwork.protocol.accack;

import com.pointcliki.meshnetwork.message.ChannelMessage;
import com.pointcliki.meshnetwork.message.IMulticastAckMessage;
import com.pointcliki.meshnetwork.message.IOrderedMessage;
import com.pointcliki.meshnetwork.message.ITaggedMessage;

import java.util.HashSet;

public class AccACKMessage extends ChannelMessage implements IOrderedMessage, ITaggedMessage {
	
	private HashSet<String> fSources;
	private String fConnection;
	private int fSeq;

	public AccACKMessage(String source, String destination, IMulticastAckMessage m) {
		super(source, destination, null);
		fSources = new HashSet<String>();
		fSources.add(source);
		fConnection = m.connection();
		fSeq = m.sequenceNumber();
	}
	
	@Override
	public String tag() {
		return fConnection + ":" + fSeq;
	}

	public String toString() {
		return "[AccACK message " + sequenceNumber() + " of " + connection() + "]";
	}

	@Override
	public int sequenceNumber() {
		return fSeq;
	}
	
	public String connection() {
		return fConnection;
	}

	public HashSet<String> sources() {
		return fSources;
	}
}
