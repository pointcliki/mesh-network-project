package com.pointcliki.meshnetwork.protocol.aodv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.ITraceableMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IForwardingService;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.service.IMessageColourerService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.utils.Backward;

public class AODV extends Protocol implements IForwardingService {
	
	private HashMap<String, Backward> fTable;
	private ArrayList<AODVRouteRequestMessage> fRequest;
	private int fRequestTimeout;
	private int fTag = 0;
	private String fLastHop;
	
	public AODV() {}

	public AODV(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fRequestTimeout = data().requestTimeout();
	}
	
	@Override
	public void startup() {
		super.startup();
	
		// Initialise empty routing table
		fTable = new HashMap<String, Backward>();
		fRequest = new ArrayList<AODVRouteRequestMessage>();
		// Add local nodes
		Node n = node();
		fTable.put(n.name(), new Backward(n.name(), n.name(), 0));
		for (Node n2: downstream(ILinkLayerService.class).localNodes()) fTable.put(n2.name(), new Backward(n2.name(), n2.name(), 1));
	}

	@Override
	public boolean receive(String source, Message m) {
		fLastHop = source;
		
		if (m instanceof ITraceableMessage) {
			ITraceableMessage tm = (ITraceableMessage) m;
			mergeBackward(source, new Backward(tm.origin(), source, tm.distance()));
		}
		
		if (m instanceof AODVPayloadMessage) {
			return handleMessage(source, (AODVPayloadMessage) m);
			
		} else if (m instanceof AODVRouteRequestMessage) {
			handleRouteRequestMessage(source, (AODVRouteRequestMessage) m);
			return true;
			
		} else if (m instanceof AODVRouteReplyMessage) {
			handleRouteReplyMessage(source, (AODVRouteReplyMessage) m);
			return true;
		}
		return fUpStream.receive(source, m);
	}

	private boolean handleMessage(String hop, AODVPayloadMessage m) {
		// Check for broadcast message
		if (m.destination().equals("broadcast")) {
			fUpStream.receive(m.source(), m.payload());
			return true;
		}
		
		// Receive the message if it's ours
		if (m.destination().equals(node().name())) fUpStream.receive(m.source(), m.payload());
		
		// If we have the destination of the message in our forwarding table, pass the message on
		else if (requestRoute(m.destination()))
			fDownStream.send(fTable.get(m.destination()).nextHop, m);
		
		// Otherwise drop message
		else
			return false;
		
		return true;
	}
	
	private void handleRouteRequestMessage(String hop, AODVRouteRequestMessage m) {

		if (fTable.containsKey(m.destination())) {
			// Only reply if the requesting node is on the shortest path
			if (fTable.containsKey(m.source()) && !hop.equals(fTable.get(m.source()).nextHop)) {
				return;
			}
			
			// Respond
			replyRoute(m.source(), m.destination());
			return;
		}
		
		// Check whether we're already requesting a route for this
		if (requestingRoute(m)) return;
		
		// Request a new route
		fRequest.add(m);
		
		Backward b = new Backward(m.backward().source, hop, m.backward().distance + 1);
		
		fDownStream.send("broadcast", new AODVRouteRequestMessage(m.source(), m.destination(), m.tag(), b, m.timestamp()));
		
	}
	
	private void handleRouteReplyMessage(String hop, AODVRouteReplyMessage m) {
		// Merge backward with current table
		mergeBackward(hop, m.backward());
		
		if (fTable.containsKey(m.destination()) && !node().name().equals(m.destination())) {
			// Pass the reply back towards the requester
			String d = fTable.get(m.destination()).nextHop;
			fDownStream.send(d, new AODVRouteReplyMessage(m.source(), m.destination(), m.tag(), fTable.get(m.source())));
		}		
	}
	
	private boolean mergeBackward(String hop, Backward b) {
		
		if (!fTable.containsKey(b.source) || b.distance + 1 < fTable.get(b.source).distance) {
			
			// Create a backward entry that stores a reverse forwarding entry to the source of the message
			Backward b2 = new Backward(b.source, hop, b.distance + 1);
			
			fTable.put(b.source, b2);
			
			return true;			
		}
		return false;
	}
	
	private boolean requestRoute(String destination) {
		if (fTable.containsKey(destination)) return true;
		
		AODVRouteRequestMessage m = new AODVRouteRequestMessage(node().name(), destination, fTag++, new Backward(node().name(), null, 0), fRequestTimeout + simulator().frame());
		
		// Check whether we're already requesting a route for this
		if (requestingRoute(m)) return false;
		
		// Request a new route
		fRequest.add(m);
		fDownStream.send("broadcast", m);
		
		return false;
	}
	
	private void replyRoute(String requester, String target) {
		// Reply with a route
		fDownStream.send(fTable.get(requester).nextHop, new AODVRouteReplyMessage(target, requester, fTag++, fTable.get(target)));
	}
	
	private boolean requestingRoute(AODVRouteRequestMessage m) {
		Iterator<AODVRouteRequestMessage> it = fRequest.iterator();
		while (it.hasNext()) {
			
			AODVRouteRequestMessage m2 = it.next();
			
			// Check the request hasn't timed out
			if (m2.timestamp() < simulator().frame()) {
				it.remove();
				continue;
			}
			
			// We also require that the request came from the same source as
			// otherwise we would ignore later requests for a route to the
			// same destination from different nodes if we weren't part of the
			// path to the destination of an earlier request				
			if (m2.source().equals(m.source()) && m2.destination().equals(m.destination())) return true;
		}
		return false;
	}

	@Override
	public boolean send(String dest, Message m) {
		
		if (dest == null) return false;
		
		// Check whether a lower layer can handle the request
		IForwardingService s = downstream(IForwardingService.class);
		if (s != null && s.route(dest)) {
			return fDownStream.send(dest, m);
		}
			
		// Otherwise, route through next hop
		if (requestRoute(dest)) {
			String d = fTable.get(dest).nextHop;
			fDownStream.send(d, new AODVPayloadMessage(node().name(), dest, fTag++, m));
			return true;
			
		} else {
			System.err.println("[AODV] Network Busy");
			return false;	
		}		
	}
	/*
	public void render(Graphics g, long time) {
		
		Node n = node();
		for (Backward b: fTable.values()) {
			Node m = simulator().nodes().get(b.source);
			g.setLineWidth(0.2f);
			if (m.name().equals("A1")) {
				g.setLineWidth(0.3f);
				g.setColor(new Color(100, 0, 100));
			} else {
				g.setColor(new Color(100, 0, 100).scaleCopy(1f / b.distance));
			}
			g.drawLine(n.position().x, n.position().y, m.position().x, m.position().y);
		}
		super.render(g, time);
	}
	*/
	
	@Override
	public void tick(long frame) {
		super.tick(frame);
		((AODVData) data()).contributeLinks(fTable.size());
	}
	
	@Override
	public String name() {
		return "AODV";
	}

	@Override
	public String nextHop(String destination) {
		if (requestRoute(destination)) return fTable.get(destination).nextHop;
		return null;
	}

	@Override
	public int distance(String destination) {
		if (fTable.containsKey(destination)) return fTable.get(destination).distance;
		return -1;
	}

	@Override
	public boolean route(String destination) {
		return requestRoute(destination);
	}

	@Override
	protected ProtocolData createData() {
		return new AODVData(this);
	}
	
	@Override
	public AODVData data() {
		return (AODVData) super.data();
	}

	@Override
	protected void createMessageColorers() {
		IMessageColourerService s = downstream(IMessageColourerService.class);
		s.addColorer(AODVRouteReplyMessage.class, Color.green);
		s.addColorer(AODVRouteRequestMessage.class, Color.yellow);
	}

	@Override
	public String receivedHop() {
		return fLastHop;
	}
}

