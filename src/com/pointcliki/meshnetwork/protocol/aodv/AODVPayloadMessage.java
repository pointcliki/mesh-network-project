package com.pointcliki.meshnetwork.protocol.aodv;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.TaggedChannelMessage;

public class AODVPayloadMessage extends TaggedChannelMessage {

	public AODVPayloadMessage(String source, String destination, int number, Message m) {
		super(source, destination, number, m);
	}
}
