package com.pointcliki.meshnetwork.protocol.aodv;

import com.pointcliki.meshnetwork.message.TaggedChannelMessage;
import com.pointcliki.meshnetwork.utils.Backward;

public class AODVRouteReplyMessage extends TaggedChannelMessage {

	public AODVRouteReplyMessage(String source, String destination, int n, Backward back) {
		super(source, destination, n, back);
	}
	
	public AODVRouteReplyMessage(String source, String destination, String tag, Backward back) {
		super(source, destination, tag, back);
	}

	public Backward backward() {
		return (Backward) payload();
	}
}
