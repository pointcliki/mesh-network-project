package com.pointcliki.meshnetwork.protocol.aodv;

import com.pointcliki.meshnetwork.message.ITimestampMessage;
import com.pointcliki.meshnetwork.message.ITraceableMessage;
import com.pointcliki.meshnetwork.message.TaggedChannelMessage;
import com.pointcliki.meshnetwork.utils.Backward;

public class AODVRouteRequestMessage extends TaggedChannelMessage implements ITimestampMessage, ITraceableMessage {
	
	private long fTimeout;

	public AODVRouteRequestMessage(String source, String destination, int n, Backward back, long timeout) {
		super(source, destination, n, back);
		fTimeout= timeout;
	}
	
	public AODVRouteRequestMessage(String source, String destination, String tag, Backward back, long timeout) {
		super(source, destination, tag, back);
		fTimeout= timeout;
	}

	public Backward backward() {
		return (Backward) payload();
	}
	
	public long timestamp() {
		return fTimeout;
	}

	@Override
	public int distance() {
		return backward().distance;
	}

	@Override
	public String origin() {
		return backward().source;
	}
}

