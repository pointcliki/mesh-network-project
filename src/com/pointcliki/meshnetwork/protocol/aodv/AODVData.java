package com.pointcliki.meshnetwork.protocol.aodv;

import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class AODVData extends ProtocolData {

	private static final String AODV_TIMEOUT = "Request Timeout";
	
	private int fLinks;
	private int fChanged;
	private boolean fIteration;
	private boolean fComplete;
	private boolean fTerminated;
	
	public AODVData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(AODV_TIMEOUT, "frames", 20));
	}
	
	@Override
	public void startup() {
		//Grapher g = simulator().createGraph("aodv", "AODV Links");
		//g.addLine(new Color(0, 180, 0));
		//g.addLine(new Color(0, 180, 180));
	}
	
	@Override
	public void finish() {
			
	}

	@Override
	public void beginTick() {
		fLinks = 0;
		fChanged = 0;
		fIteration = false;
	}
	
	@Override
	public void endTick() {
		/*
		simulator().graph("aodv").add(0, fLinks / simulator().nodes().size());
		simulator().graph("aodv").add(1, fChanged);
		
		if (fIteration && fLinks == (int) Math.pow(simulator().nodes().size(), 2) && !fComplete) {
			fComplete = true;
			simulator().log(Simulator.LOG_ROUTES, "[Distance Vector] All nodes linked at frame " + simulator().frame());
		}
		if (fIteration && fChanged == 0 && fComplete && !fTerminated) {
			fTerminated = true;
			simulator().log(Simulator.LOG_ROUTES, "[Distance Vector] Algorithm terminated at frame " + simulator().frame());
		}
		*/
	}
	
	public void iterating() {
		fIteration = true;
	}

	public void contributeLinks(int size) {
		fLinks += size;
	}
	public void incrementDelta() {
		fChanged++;
	}

	@Override
	public String info() {
		return "<html><b>Ad-hoc Distance Vector</b></html>";
	}
	
	public int requestTimeout() {
		return getOption(AODV_TIMEOUT, int.class);
	}
}
