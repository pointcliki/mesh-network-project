package com.pointcliki.meshnetwork.protocol;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultTreeModel;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.Item;
import com.pointcliki.meshnetwork.model.Metric;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.ui.task.DeleteItemTask;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public abstract class ProtocolData extends Item implements Cloneable {

	private SimulationItem fSim;
	private Simulator fSimulator;
	private Protocol fProtocol;
	private ArrayList<Metric> fMetrics;
	private ArrayList<Annotation> fAnnotations;
	private ArrayList<Event> fEvents;
	
	public ProtocolData(Protocol p) {
		super(p.name(), Icons.ProtocolIcon);
		fProtocol = p;
		fMetrics = new ArrayList<Metric>();
		fAnnotations = new ArrayList<Annotation>();
		fEvents = new ArrayList<Event>();
		
		tasks().add(new DeleteItemTask());
	}
	
	public SimulationItem simulation() {
		return fSim;
	}
	
	public void setSimulation(SimulationItem sim) {
		fSim = sim;
		fSimulator = sim.simulator();
	}
	
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode n) {
		for (Annotation a: fAnnotations) {
			n.add(new IconLabelNode(model, a));
		}
		for (Event e: fEvents) {
			n.add(new IconLabelNode(model, e));
		}
		for (Metric m: fMetrics) {
			n.add(new IconLabelNode(model, m));
		}
		return n;
	}
	
	public void startup() {};
	public void finish() {};
	public void beginTick() {};
	public void endTick() {};
	public abstract String info();
	
	public void setProtocol(Protocol p) {
		fProtocol = p;
	}
	
	public Protocol protocol() {
		return fProtocol;
	}
		
	public List<Metric> metrics() {
		return fMetrics;
	}
	
	public List<Annotation> annotations() {
		return fAnnotations;
	}
	
	public List<Event> events() {
		return fEvents;
	}
	
	public Simulator simulator() {
		return fSimulator;
	}
	
	@Override
	public ProtocolData clone() {
		ProtocolData d = (ProtocolData) super.clone();
		d.fAnnotations = cloneList(fAnnotations);
		d.fEvents = cloneList(fEvents);
		d.fMetrics = cloneList(fMetrics);
		return d;
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends Item> ArrayList<T> cloneList(List<T> list) {
	    ArrayList<T> clone = new ArrayList<T>(list.size());
	    for(T item: list) clone.add((T) item.clone());
	    return clone;
	}
		
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("class", fProtocol.getClass().getName());
		return obj;
	}

	public Annotation getAnnotation(String name) {
		for (Annotation a: fAnnotations) if (a.name().equals(name)) return a;
		return null;
	}

	public Metric getMetric(String name) {
		for (Metric m: fMetrics) if (m.name().equals(name)) return m;
		return null;
	}
	
	public Event getEvent(String name) {
		for (Event m: fEvents) if (m.name().equals(name)) return m;
		return null;
	}
}
