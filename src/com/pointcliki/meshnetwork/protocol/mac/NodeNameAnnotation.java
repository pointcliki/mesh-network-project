package com.pointcliki.meshnetwork.protocol.mac;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.protocol.Protocol;

public class NodeNameAnnotation extends Annotation {

	public NodeNameAnnotation(Protocol p) {
		super("[MAC] Node Names", p);
	}

	
}
