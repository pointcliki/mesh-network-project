package com.pointcliki.meshnetwork.protocol.mac;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.ChannelMessage;

/**
 * A MAC message represents data being transferred across the network
 * with data-link layer packet header information.
 * @author Hugheth
 */
public class MACMessage extends ChannelMessage {
	
	private static int sUID = 0;
	private int fID;
	
	public MACMessage(String source, String destination, Message payload) {
		super(source, destination, payload);
		fID = sUID++;
	}
	/*
	public Color color() {
		if (fPayload != null) return fPayload.color();
		return Color.white;
	}
	*/
	
	@Override
	public String toString() {
		return "[LinkLayer message " + fID + " from " + source() + " to " + destination() + " of size " + size() + "] " + (payload() != null ? " " + payload().toString() : ""); 
	}
}

