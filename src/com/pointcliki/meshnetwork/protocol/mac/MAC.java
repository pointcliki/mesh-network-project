package com.pointcliki.meshnetwork.protocol.mac;

import java.util.ArrayList;
import java.util.HashMap;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.IPayloadMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.model.EventInstance;
import com.pointcliki.meshnetwork.model.IEventListener;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IForwardingService;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.service.IMessageColourerService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.utils.MessageColorer;
import com.pointcliki.meshnetwork.utils.MessageQueue;

public class MAC extends Protocol implements ILinkLayerService, IMessageColourerService, IForwardingService {
		
	public static final int IDLE = 0;
	public static final int RECEIVING = 1;
	public static final int TRYING_TO_SEND = 2;
	public static final int TRANSMITTING = 3;
	public static final int OFFLINE = 4;
	
	private int fState;
	private int fLastState;
	
	private ArrayList<Node> fLocalNodes;
	private int fNumLocalReceivers = 0;	// The number of nodes receiving nearby
	private int fNumLocalTransmitters = 0;	// The number of nodes transmitting nearby
	private int fNumLocalTryingToSend = 0;	// The number of nodes trying to send nearby
	
	private int fTransferPacketSize;	// The size of the transferring packet, in frames
	private int fTransferPacketAmount;	// The number of frames elapsed so far
	private int fNumTries;	// The number of frames we've been waiting to start transmitting
	
	private int fRetryLimit;
	private MessageQueue<MACMessage> fMessages;
	private Color fMessageColor;
	private HashMap<Class<? extends Message>, MessageColorer> fColorers;
	private String fLastHop;
	
	public static final Color backColor = new Color(206, 206, 206);
	
	public MAC() {
		super();
	}
	
	public MAC(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fState = IDLE;
		fRetryLimit = data().retryLimit();
		fMessages = new MessageQueue<MACMessage>(data().queueSize());
	}
	
	public void timeout() {
		if (fState == TRYING_TO_SEND) {
			fMessages.poll();
			simulator().log(Simulator.LOG_LINKLAYER, "[" + node().name() + ": Was forced to timeout]");
			for (Node n2: fLocalNodes) n2.upstream(MAC.class).fNumLocalTryingToSend--;
			fState = IDLE;
		}
	}
	
	@Override
	public void startup() {
		super.startup();
	}
		
	public void flush() {
		timeout();
		fMessages.reset();
	}
	
	public ArrayList<Node> localNodes() {
		return fLocalNodes;
	}
	
	@Override
	public Node local(String s) {
		for (Node n: localNodes()) if (n.name().equals(s)) return n;
		return null;
	}
	
	public String name() {
		return "MAC";
	}
	
	@Override
	public void tick(long frame) {
		super.tick(frame);
		data().contributeBuffer(fMessages.size());
	}
	
	public boolean tryToSend() {
		assert(fState == TRYING_TO_SEND);
		
		simulator().log(Simulator.LOG_MAC, "[" + node().name() + ": Trying to send]");
		
		// Check that there are no local nodes already receiving that we would interfere
		// with if we were to start transmitting now.
		if (fNumLocalReceivers > 0) {
			data().incrementCongestion();
			return false;
		}
		
		// Determine whether the node successfully transmits
		boolean transmits = true;
		// TODO: ADD MATHS TO DETERMINE WHETHER IT TRANSMITS BASED ON NUMBER OF LOCAL TRYING TO SENDERS
		if (fNumLocalTryingToSend > 100) transmits = true;	
		
		if (!transmits) {
			data().incrementCongestion();
			return false;
		}
		
		transmits = false;
		// Check that there is at least one node who wants to receive the packet
		for (Node n2: fLocalNodes) {
			MAC mac = n2.upstream(MAC.class);
			
			if ((mac.fState == IDLE || mac.fState == TRYING_TO_SEND) && mac.fNumLocalTransmitters == 0 && mac.wantsMessage(fMessages.peek())) {
				transmits = true;
				break;
			}
		}
		
		if (!transmits) {
			fNumTries++;
			if (fNumTries == fRetryLimit) {
				MACMessage m = fMessages.poll();
				simulator().log(Simulator.LOG_MAC, "[" + node().name() + ": Dropped a message] " + m);
				data().incrementLostPackets();
				for (Node n2: fLocalNodes) n2.upstream(MAC.class).fNumLocalTryingToSend--;
				fState = IDLE;
			}
			return false;
		}
		// Start transmitting
		fState = TRANSMITTING;
		fTransferPacketAmount = 0;
		fTransferPacketSize = fMessages.peek().size();
		fMessageColor = color(fMessages.peek());
		
		// Iterate through local nodes to notify of change
		for (Node n2: fLocalNodes) {
			MAC mac = n2.upstream(MAC.class);
			
			// If the node is ready to receive, change its state
			if ((mac.fState == IDLE || mac.fState == TRYING_TO_SEND) && mac.fNumLocalTransmitters == 0 && mac.wantsMessage(fMessages.peek()))
				mac.startReceiving(this, fMessages.peek());
			
			// Update node's network information
			mac.fNumLocalTransmitters++;
			mac.fNumLocalTryingToSend--;
		}
		return true;
	}
		
	public Color color(Message m) {
		if (fColorers.containsKey(m.getClass())) {
			return fColorers.get(m.getClass()).color(m);
			
		} else if (m instanceof IPayloadMessage) {
			return color(((IPayloadMessage) m).payload());
		}
		return Color.white;
	}

	public void startReceiving(MAC upstream, MACMessage m) {
		assert(fState == IDLE);
		
		simulator().log(Simulator.LOG_MAC, "[" + node().name() + ": Started receving]");
		
		// Start receiving
		fState = RECEIVING;
		for (Node n2: fLocalNodes) n2.upstream(MAC.class).fNumLocalReceivers++;
	}
	
	public void startTryingToSend() {
		assert(fState == IDLE);
				
		simulator().log(Simulator.LOG_MAC, "[" + node().name() + ": Started trying to send]");
		
		// Start trying to send
		fState = TRYING_TO_SEND;
		fNumTries = 0;
		for (Node n2: fLocalNodes) n2.upstream(MAC.class).fNumLocalTryingToSend++;
	}
	
	public void sendPacket() {
		assert(fState == TRANSMITTING);
		fTransferPacketAmount++;
		simulator().log(Simulator.LOG_MAC, "[" + node().name() + ": Sent a packet (" + fTransferPacketAmount + "/" + fTransferPacketSize + ")]");
		// Check for end
		if (fTransferPacketAmount == fTransferPacketSize) {
			
			// Post message
			MACMessage m = fMessages.poll();
	
			for (Node n2: fLocalNodes) {
				MAC mac = n2.upstream(MAC.class);
				if (mac.fState == RECEIVING) mac.receiveMessage(m);
				mac.fNumLocalTransmitters--;
			}
			fState = IDLE;
		}
		data().incrementUtility();
	}
	
	public void receiveMessage(MACMessage m) {
		assert(fState == RECEIVING);
		
		fLastHop = m.source();
		
		simulator().log(Simulator.LOG_MAC, "[" + node().name() + " Received a message]");
		simulator().log(Simulator.LOG_MAC, m.toString());
		
		fUpStream.receive(m.source(), m.payload());
		for (Node n2: fLocalNodes) n2.upstream(MAC.class).fNumLocalReceivers--;
		
		fState = IDLE;
	}
	
	public void cancelMessage() {
		assert(fState == RECEIVING);
		
		for (Node n2: fLocalNodes) n2.upstream(MAC.class).fNumLocalReceivers--;
		fState = IDLE;
	}
	
	public boolean wantsMessage(MACMessage m) {
		return m.equals("broadcast") || m.equals(this.node().name());
	}

	public boolean send(String dest, Message m) {
		boolean okay = fMessages.push(new MACMessage(node().name(), dest, m));
		if (!okay) {
			simulator().log(Simulator.LOG_MAC, "[" + node().name() + ": Buffer full]");
			data().incrementLostPackets();
		}
		return okay;
	}
	
	public int state() {
		return fState;
	}
	
	public void saveState() {
		fLastState = fState;
	}
	
	public int lastState() {
		return fLastState;
	}
	
	public int queueSize() {
		return fMessages.size();
	}
	public int queueCapacity() {
		return fMessages.capacity();
	}

	public void calculateLocalNodes() {
		fLocalNodes = new ArrayList<Node>();
		for (Node n2: simulator().nodes().values()) {
			if (node().name().equals(n2.name())) continue;
			if (node().position().distance(n2.position()) < simulator().nodeRange()) fLocalNodes.add(n2);
		}
	}

	@Override
	public int linkCost(String dest) {
		if (dest.equals(name())) return 0;
		if (local(dest) != null) return 1;
		return -1;
	}
	
	public Color messageColor() {
		return fMessageColor;
	}
	
	public void finish() {
		fState = IDLE;
		fLastState = IDLE;
	}

	public MACData data() {
		return (MACData) super.data();
	}

	@Override
	/**
	 * We don't expect any protocols below this so the behaviour
	 * of the MAC protocol when recieving a message is undefined
	 */
	public boolean receive(String destination, Message message) {
		return false;
	}
	
	public ProtocolData createData() {
		return new MACData(this);
	}

	@Override
	protected void createEventListeners() {
		setEventHandler(MACData.MAC_NODE_ONLINE, new IEventListener<MAC>() {
			@Override
			public void handleEvent(MAC p, EventInstance ev) {
				fState = IDLE;
				fLastState = IDLE;
			}
		});
		setEventHandler(MACData.MAC_NODE_OFFLINE, new IEventListener<MAC>() {
			@Override
			public void handleEvent(MAC p, EventInstance ev) {
				
				for (Node n2: fLocalNodes) {
					MAC mac = n2.upstream(MAC.class); 
							
					if (fState == TRYING_TO_SEND) {
						mac.fNumLocalTryingToSend--;
						 
					} else if (fState == TRANSMITTING) {
						mac.fNumLocalTransmitters--;
						
						if (mac.state() == RECEIVING) mac.cancelMessage();
						
						simulator().log(Simulator.LOG_NODES, "[" + node().name() + " Dropped a message] " + fMessages.peek());
						
					} else if (fState == RECEIVING) {
						mac.fNumLocalReceivers--;
					}
				}
				
				fMessages.reset();
				fState = OFFLINE;
				fLastState = OFFLINE;
			}
		});
	}

	@Override
	public void addColorer(Class<? extends Message> cls, MessageColorer colorer) {
		fColorers.put(cls, colorer);
	}
	
	@Override
	public void addColorer(Class<? extends Message> cls, final Color color) {
		addColorer(cls, new MessageColorer() {
			@Override
			public Color color(Message m) {
				return color;
			}
		});
	}

	@Override
	public void clearColorer(Class<? extends Message> cls) {
		fColorers.remove(cls);
	}

	@Override
	public String nextHop(String destination) {
		if (route(destination)) return destination;
		return null;
	}

	@Override
	public boolean route(String destination) {
		for (Node n: fLocalNodes) if (n.name().equals(destination)) return true;
		return false;
	}

	@Override
	public int distance(String destination) {
		if (destination.equals(node().name())) return 0;
		if (route(destination)) return 1;
		return -1;
	}

	@Override
	public String receivedHop() {
		return fLastHop;
	}
}

