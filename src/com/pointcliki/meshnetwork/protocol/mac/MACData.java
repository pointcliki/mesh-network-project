package com.pointcliki.meshnetwork.protocol.mac;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.model.Metric;
import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class MACData extends ProtocolData {
	
	private int fUtility;
	private int fCongestion;
	private int fLostPackets;
	private int fBuffer;
	
	public final static String MAC_PACKETS = "[MAC] Transmitted Packets";
	public final static String MAC_LOST = "[MAC] Lost Packets";
	public static final String MAC_CONGESTED = "[MAC] Congested Nodes";
	public static final String MAC_BUFFER_SIZE = "[MAC] Buffered Packets";
	public static final String MAC_NODE_ONLINE = "[MAC] Node Online";
	public static final String MAC_NODE_OFFLINE = "[MAC] Node Offline";
	
	public MACData(Protocol p) {
		super(p);
		
		annotations().add(new NodeNameAnnotation(p));
		annotations().add(new PacketAnnotation(p));
		metrics().add(new Metric(MAC_PACKETS, p));
		metrics().add(new Metric(MAC_LOST, p));
		metrics().add(new Metric(MAC_CONGESTED, p));
		metrics().add(new Metric(MAC_BUFFER_SIZE, p));
		events().add(new Event(MAC_NODE_ONLINE, p));
		events().add(new Event(MAC_NODE_OFFLINE, p));
		events().add(new Event("[MAC] Node Moved", p));
		
		options().add(new Option<Integer>("Queue Size", "packets", 8));
		options().add(new Option<Integer>("Retry Limit", "congested frames", 8));
	}

	@Override
	public void startup() {
		// Put graphs
		//Grapher macGraph = simulator().createGraph("mac", "Link Layer");
		//macGraph.addLine(Color.blue);
		//macGraph.addLine(new Color(250, 100, 0));
		//macGraph.addLine(new Color(50, 0, 0));		
	}
	
	public void finish() {
		
	}

	@Override
	public void beginTick() {
		fUtility = 0;
		fCongestion = 0;
		fLostPackets = 0;
		fBuffer = 0;
	}
	
	public void incrementUtility() {
		fUtility++;
	}
	public void incrementCongestion() {
		fCongestion++;
	}
	public void incrementLostPackets() {
		fLostPackets++;
	}
	public void contributeBuffer(int i) {
		fBuffer += i;
	}

	@Override
	public void endTick() {
		simulation().contributeToMetric(MAC_PACKETS, fUtility);
		simulation().contributeToMetric(MAC_LOST, fLostPackets);
		simulation().contributeToMetric(MAC_CONGESTED, fCongestion);
		simulation().contributeToMetric(MAC_BUFFER_SIZE, fBuffer);
	}

	@Override
	public String info() {
		return "<html><b>MAC Protocol</b><br/><small>Interesting</small></html>";
	}

	public int retryLimit() {
		return getOption("Retry Limit", int.class);
	}
	public int queueSize() {
		return getOption("Queue Size", int.class);
	}
}

