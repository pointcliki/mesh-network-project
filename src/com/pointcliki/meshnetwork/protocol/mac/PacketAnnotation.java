package com.pointcliki.meshnetwork.protocol.mac;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class PacketAnnotation extends Annotation {

	public PacketAnnotation(Protocol p) {
		super("[MAC] Packets", p);
	}

	@Override
	public void render(Simulator sim, Graphics g, long time) {
		
		float interval = Math.min((time - sim.lastFrameTime() + 0f) / sim.frameLength(), 0.95f);
		g.setLineWidth(3f);
		g.setAntiAlias(true);
		
		for (Node n: sim.nodes().values()) {
			ILinkLayerService mac = n.upstream(ILinkLayerService.class);
			g.setColor(mac.messageColor());
			if (mac.lastState() == MAC.TRANSMITTING) {								
				for (Node n2: mac.localNodes()) if (n2.upstream(ILinkLayerService.class).lastState() == MAC.RECEIVING) {
					Vector2f v = n2.position().copy().sub(n.position());
					Vector2f off = v.copy().scale(interval);
					Vector2f reach = v.copy().normalise().scale(4f);
					g.drawLine(n.position().x + off.x, n.position().y + off.y, n.position().x + off.x + reach.x, n.position().y + off.y + reach.y);
					//Vector2f v = n2.position().copy().sub(position());
					//g.drawLine(position().x, position().y, n2.position().x, n2.position().y);
				}
			}
		}
	}

}
