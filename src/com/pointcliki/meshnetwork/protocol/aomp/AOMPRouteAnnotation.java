package com.pointcliki.meshnetwork.protocol.aomp;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class AOMPRouteAnnotation extends Annotation {

	public final static String NAME = "[AOMP] Routes";
	
	public AOMPRouteAnnotation(Protocol p) {
		super(NAME, p);
	}
	
	@Override
	public void render(Simulator sim, Graphics g, long time) {
		g.setColor(Color.white);
		g.setLineWidth(1f);
		
		for (Node n: sim.nodes().values()) {
			AOMP p = n.upstream(AOMP.class);
			List<String> list = p.nextHops("&A1");
			if (list != null) {
				for (String s: list) {
					Node m = sim.nodes().get(s);
					g.setColor(new Color(200, 0, 100));
					g.drawLine(n.position().x, n.position().y, m.position().x, m.position().y);
				}
			}
		}
	}
}
