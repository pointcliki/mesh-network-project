package com.pointcliki.meshnetwork.protocol.aomp;

import com.pointcliki.meshnetwork.model.Event;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IForwardingService;

public class AOMPData extends ProtocolData {
	
	public final static String AOMP_PING_TIMEOUT = "[Transport] Ping Timeout";

	public AOMPData(Protocol p) {
		super(p);
		annotations().add(new AOMPRouteAnnotation(p));
		events().add(new Event(IForwardingService.EVENT_JOIN, p));
		events().add(new Event(IForwardingService.EVENT_LEAVE, p));
	}

	@Override
	public void startup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beginTick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endTick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return null;
	}

}
