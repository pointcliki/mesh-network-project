package com.pointcliki.meshnetwork.protocol.aomp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IForwardingService;
import com.pointcliki.meshnetwork.service.IMessageColourerService;
import com.pointcliki.meshnetwork.service.IMulticastForwardingService;
import com.pointcliki.meshnetwork.service.IMulticastGroupLeaderService;
import com.pointcliki.meshnetwork.service.IMulticastGroupRegistrarService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.utils.Forward;

/**
 * AOMP is a custom-designed protocol designed to harness the
 * ad-hoc nature of the path discovery in lower layers. Similar in
 * style to RSVP but much simpler in construction, AOMP deals with
 * one-to-many multicast groups where a group has only a single
 * transmitter, identified by the name of the group (i.e. group &A01
 * is the group which node A01 transmits to).
 * 
 * This restriction eliminates the need for multicast provider discovery by
 * requiring explicit connection to a multicasting node through the use
 * of a join message.
 * 
 * @author Hugheth
 */

public class AOMP extends Protocol implements IMulticastForwardingService, IMulticastGroupRegistrarService, IMulticastGroupLeaderService {
	
	private HashSet<String> fRegisteredGroups;
	/**
	 * We use an array list here because we might have multiple 
	 */
	protected HashMap<String, ArrayList<Forward>> fNextHops;
	private int fTag = 0;
	
	public AOMP() {}
	
	public AOMP(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
	}
	
	public void startup() {
		fRegisteredGroups = new HashSet<String>();
		fNextHops = new HashMap<String, ArrayList<Forward>>();
	}

	@Override
	public boolean join(String group) {
		// Add to group
		fRegisteredGroups.add(group);
		
		IForwardingService s = downstream(IForwardingService.class);
		if (s == null) return false;
		
		// Get the group leader
		String leader = groupLeader(group);
		// Get the next hop
		String hop = s.nextHop(leader);
		
		if (hop == null) {
			s.route(leader);
			return false;
		}
		
		// Send a message to the next hop
		return fDownStream.send(hop, new AOMPJoinMessage(node().name(), group, 1));
	}

	@Override
	public boolean leave(String group) {
		return false;
	}

	@Override
	public boolean send(String dest, Message m) {
		
		if (dest == null) return false;
		
		// Multicast protocol only sends to groups
		if (dest.startsWith("&"))
			return sendPayload(new AOMPPayloadMessage(dest, fTag++, m));
		
		else
			return fDownStream.send(dest, m);
	}
	
	private boolean sendPayload(AOMPPayloadMessage m) {
		// Check whether we have next hop for this group
		ArrayList<Forward> keys = fNextHops.get(m.group());
		if (keys != null) {
			// Log
			simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " pushed multicast transmission to " + m.group());
			
			// If we only have one recipient we can send directly to this
			if (keys.size() == 1) return fDownStream.send(keys.get(0).nextHop, m); 
			else return fDownStream.send("broadcast", m);
			
		} else {
			simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " didn't push message for " + m.group());
			return false;
		}
	}

	@Override
	public boolean receive(String source, Message m) {
		if (m instanceof AOMPJoinMessage) {
			return handleJoinMessage(source, (AOMPJoinMessage) m);
			
		} else if (m instanceof AOMPPayloadMessage) {
			handleMessage(source, (AOMPPayloadMessage) m);
		}
		return true;
	}
	
	private void handleMessage(String source, AOMPPayloadMessage m) {
				
		// Reject if it comes from a direction we send to
		if (fNextHops.containsKey(m.group()) && fNextHops.get(m.group()).contains(source)) return;
		
		// Receive downstream if we are part of the group
		if (fRegisteredGroups.contains(m.group())) fUpStream.receive(source, m.payload());
		
		// Send on
		sendPayload(new AOMPPayloadMessage(m.group(), m.tag(), m.payload()));
	}

	private boolean handleJoinMessage(String source, AOMPJoinMessage m) {
		
		String group = m.group();
		
		IForwardingService s = downstream(IForwardingService.class);
		if (s == null) return false;
		
		boolean okay = true;
		
		// Add source to list of destinations for the group
		if (!fNextHops.containsKey(group)) {
			fNextHops.put(group, new ArrayList<Forward>());
			
			// Check whether we control the group
			if (ownGroup(m.group())) {
				
				// Get the group leader
				String leader = groupLeader(group);
				// Get the next hop
				String hop = s.nextHop(leader);
				
				if (hop == null) {
					s.route(leader);
					return false;
				}
				
				// Send on the packet towards the group's leader
				okay = fDownStream.send(hop, new AOMPJoinMessage(m.origin(), group, m.distance() + 1));
			}
		}
		
		fNextHops.get(group).add(new Forward(m.origin(), s.receivedHop(), m.distance()));
		
		return okay;
	}

	@Override
	public String name() {
		return "AOMP";
	}

	@Override
	public List<String> nextHops(String group) {
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<Forward> fs = fNextHops.get(group);
		for (Forward f: fs) list.add(f.nextHop);
		return list;
	}
	
	@Override
	public Set<String> routeParticipants(String group) {
		HashSet<String> list = new HashSet<String>();
		ArrayList<Forward> fs = fNextHops.get(group);
		for (Forward f: fs) list.add(f.destination);
		return list;
	}

	@Override
	public ProtocolData createData() {
		return new AOMPData(this);
	}

	@Override
	protected void createMessageColorers() {
		IMessageColourerService s = downstream(IMessageColourerService.class);
		s.addColorer(AOMPJoinMessage.class, Color.pink);
	}

	@Override
	public Set<String> uniqueNextHops(String group) {
		HashSet<String> list = new HashSet<String>();
		ArrayList<Forward> fs = fNextHops.get(group);
		for (Forward f: fs) list.add(f.nextHop);
		return list;
	}

	@Override
	public boolean joined(String group) {
		return fRegisteredGroups.contains(group);
	}

	@Override
	public String groupLeader(String group) {
		IMulticastGroupLeaderService s = downstream(IMulticastGroupLeaderService.class);
		if (s != null) return s.groupLeader(group);
		// By default just assume groups of &__ form and remove &
		return group.substring(1);
	}
	@Override
	public boolean ownGroup(String group) {
		return group.equals(groupLeader(group));
	}
}
