package com.pointcliki.meshnetwork.protocol.aomp;

import com.pointcliki.meshnetwork.message.ITraceableMessage;
import com.pointcliki.meshnetwork.message.MulticastControlMessage;

/**
 * The AOMPJoinMessage is a message passed between
 * hops of a path from a node who wishes to join a
 * group to the group's leader.
 * 
 * The destination method is somewhat exotic as the message
 * stops at each hop of the path to the leader so that a
 * multicast route can be set up leading back to the listener
 * from the group's leader.
 * 
 * We query a forwarding service to find the nextHop to the
 * leader and use this as the destination.
 * 
 * @author Hugheth
 */
public class AOMPJoinMessage extends MulticastControlMessage implements ITraceableMessage {
	
	private int fDistance;

	public AOMPJoinMessage(String origin, String group, int distance) {
		super(origin, group);
		fDistance = distance;
	}
	
	public String toString() {
		return "[AOMPJoinMessage for group " + group() + " ]";
	}
	
	public int distance() {
		return fDistance;
	}

	@Override
	public int size() {
		return 1;
	}
}
