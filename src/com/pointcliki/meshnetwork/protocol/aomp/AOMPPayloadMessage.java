package com.pointcliki.meshnetwork.protocol.aomp;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.TaggedMulticastMessage;

public class AOMPPayloadMessage extends TaggedMulticastMessage {

	public AOMPPayloadMessage(String group, String tag, Message payload) {
		super(group, tag, payload);
	}
	
	public AOMPPayloadMessage(String group, int number, Message payload) {
		super(group, number, payload);
	}
		
	public String toString() {
		return "[AOMP Payload message] " + payload().toString();
	}
}
