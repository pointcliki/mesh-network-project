package com.pointcliki.meshnetwork.protocol.dvmrp;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.TaggedMulticastMessage;

public class DVMRPPayloadMessage extends TaggedMulticastMessage {

	public DVMRPPayloadMessage(String group, String tag, Message payload) {
		super(group, tag, payload);
	}
	
	public DVMRPPayloadMessage(String group, int number, Message payload) {
		super(group, number, payload);
	}
		
	public String toString() {
		return "[DVMRP Payload message] " + payload().toString();
	}
}
