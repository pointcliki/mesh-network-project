package com.pointcliki.meshnetwork.protocol.dvmrp;

import com.pointcliki.meshnetwork.message.MulticastControlMessage;

public class DVMRPPruneMessage extends MulticastControlMessage {

	/**
	 * Create a new prune request
	 * @param origin Who is sending messages that we don't want
	 * @param group Who are they sending them to
	 */
	public DVMRPPruneMessage(String origin, String group) {
		super(origin, group);
	}
	
	@Override
	public String toString() {
		return "[Prune " + origin() + " from " + group() + "]";
	}
}
