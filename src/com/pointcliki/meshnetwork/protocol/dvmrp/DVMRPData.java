package com.pointcliki.meshnetwork.protocol.dvmrp;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class DVMRPData extends ProtocolData {

	public DVMRPData(Protocol p) {
		super(p);
		annotations().add(new DVMRPRouteAnnotation(p));
	}

	@Override
	public void startup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public void beginTick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void endTick() {
		// TODO Auto-generated method stub

	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return null;
	}

}
