package com.pointcliki.meshnetwork.protocol.dvmrp;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class DVMRPRouteAnnotation extends Annotation {

	public final static String NAME = "[DVMRP] Routes";
	
	public DVMRPRouteAnnotation(Protocol p) {
		super(NAME, p);
	}

	@Override
	public void render(Simulator sim, Graphics g, long time) {
		g.setColor(Color.white);
		g.setLineWidth(1);
		
		for (Node n: sim.nodes().values()) {
			DVMRP p = n.upstream(DVMRP.class);
			if (!p.fPrunes.containsKey("&tiger") || !p.fPrunes.get("&tiger").selfPruned()) {
				g.drawOval(-8 + n.position().x, -8 + n.position().y, 16, 16);
			}
		}
	}
}
