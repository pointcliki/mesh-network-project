package com.pointcliki.meshnetwork.protocol.dvmrp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IForwardingService;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.service.IMessageColourerService;
import com.pointcliki.meshnetwork.service.IMulticastForwardingService;
import com.pointcliki.meshnetwork.service.IMulticastGroupRegistrarService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class DVMRP extends Protocol implements IMulticastForwardingService, IMulticastGroupRegistrarService {
	
	protected HashMap<String, MulticastPrune> fPrunes;
	protected HashSet<String> fRegisteredGroups;
	private int fTag = 0;
	
	public DVMRP() {}
	
	public DVMRP(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fPrunes = new HashMap<String, MulticastPrune>();
		fRegisteredGroups = new HashSet<String>();
	}

	@Override
	public boolean send(String dest, Message m) {
		
		if (dest == null) return false;
		
		// The multicast service only handle messages that are destined for groups
		if (dest.startsWith("&")) {
			simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " pushed multicast transmission from " + node().name() + " to " + dest);
			return sendPayload(new DVMRPPayloadMessage(dest, fTag++, m));
		} else {
			return fDownStream.send(dest, m);
		}
	}
	
	private boolean sendPayload(DVMRPPayloadMessage m) {
		MulticastPrune p = fPrunes.get(m.group());
		
		// Check that at least one of our neighbours may want this message
		List<Node> locals = downstream(ILinkLayerService.class).localNodes();
		int count = 0;
		String mono = null;
		
		for (Node n: locals) {
			if (!p.contains(n.name())) {
				count++;
				mono = n.name();
				break;
			}
		}
		
		// Broadcast to parties who are interested, or send direct if there's just one
		if (count == 1) return fDownStream.send(mono, m);
		else if (count > 1) return fDownStream.send("broadcast", m);
		
		return false;
	}

	@Override
	public void startup() {
		super.startup();
	}

	@Override
	public boolean receive(String source, Message m) {
		if (m instanceof DVMRPJoinMessage) {
			handleJoinMessage(source, (DVMRPJoinMessage) m);
			return true;
			
		} else if (m instanceof DVMRPPruneMessage) {
			handlePruneMessage(source, (DVMRPPruneMessage) m);
			return true;
			
		} else if (m instanceof DVMRPPayloadMessage) {
			handleMessage(source, (DVMRPPayloadMessage) m);
			return true;
		}
		return fUpStream.receive(source, m);
	}

	private void handleMessage(String source, DVMRPPayloadMessage m) {
		
		if (!fPrunes.containsKey(m.group())) fPrunes.put(m.group(), new MulticastPrune());
		MulticastPrune p = fPrunes.get(m.group());
		
		// Find out the last hop that the message took to get here
		String usedHop = downstream(IForwardingService.class).receivedHop();
		
		// Next hop along the shortest path to the source
		String trueHop = downstream(IForwardingService.class).nextHop(source);
		
		// Add source to prune list as an implicit prune
		if (trueHop != null) p.addPrune(trueHop);
		
		// Only accept the message if it has come along the shortest path to the source
		if (trueHop != null && !usedHop.equals(trueHop)) {
			simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " ignored redundant message for " + m.group());
			// Ensure that the neighbour who sent the message knows that we're not interested in future			
			if (!p.notified(usedHop)) {
				// Notify the neighbour that they don't need to send us messages
				send(usedHop, new DVMRPPruneMessage(source, m.group()));
				p.addNotified(usedHop);
			}
			return;
		}
		
		// Receive downstream if we are part of the group
		if (fRegisteredGroups.contains(m.group())) fUpStream.receive(source, m.payload());
				
		// Only forward if we haven't been pruned
		if (p.selfPruned()) {
			// Check whether we've notified the previous hop recently - TODO: use a forgetful set
			if (!p.notified(usedHop)) {
				send(usedHop, new DVMRPPruneMessage(source, m.group()));
				p.addNotified(usedHop);
			}
			return;
		}
		
		boolean okay = sendPayload(new DVMRPPayloadMessage(m.group(), m.tag(), m.payload()));
		
		// If no-one wants the message and we aren't interested ourselves we should self prune
		if (!okay && !fRegisteredGroups.contains(m.group())) {
			
			simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " SELF PRUNE");
			
			p.setSelfPruned(true);
			
			// Ensure upstream knows that we don't have any interested parties
			if (!p.notified(source)) {
				send(trueHop, new DVMRPPruneMessage(source, m.group()));
				p.addNotified(source);
			}
		}
	}

	private void handlePruneMessage(String source, DVMRPPruneMessage m) {
		if (!fPrunes.containsKey(m.group())) fPrunes.put(m.group(), new MulticastPrune());
		
		MulticastPrune p = fPrunes.get(m.group());
		// Add to prune list
		p.addPrune(source);
		
		simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " recieved a prune request from " + source + " for " + m.group());
		
		// Don't need to consider self pruning if we want the message
		if (fRegisteredGroups.contains(m.group())) return;
		
		// Check that at least one of our neighbours may want this message
		List<Node> locals = downstream(ILinkLayerService.class).localNodes();
		boolean interested = false;
		for (Node n: locals) {
			if (!p.contains(n.name())) {
				interested = true;
				break;
			}
		}
				
		// Check whether we've changed to be self pruned
		if (!interested && !p.selfPruned()) {
			
			simulator().log(Simulator.LOG_MULTIROUTING, node().name() + " CASCADE PRUNE " + source);
			
			p.setSelfPruned(true);
			
			// Ensure upstream knows that we don't have any interested parties
			String hop = downstream(IForwardingService.class).nextHop(m.origin());
			
			if (hop != null) {
				send(hop, new DVMRPPruneMessage(m.origin(), m.group()));
				p.addNotified(hop);
			}
		}
	}

	private void handleJoinMessage(String source, DVMRPJoinMessage payload) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String name() {
		return "DVMRP";
	}
	
	@Override
	public void tick(long frame) {
		super.tick(frame);
		
	}

	@Override
	public boolean join(String group) {
		fRegisteredGroups.add(group);
		return true;
	}

	@Override
	public boolean leave(String group) {
		fRegisteredGroups.remove(group);
		return true;
	}

	@Override
	protected ProtocolData createData() {
		return new DVMRPData(this);
	}

	@Override
	protected void createMessageColorers() {
		IMessageColourerService s = downstream(IMessageColourerService.class);
		s.addColorer(DVMRPPruneMessage.class, Color.blue);
		s.addColorer(DVMRPJoinMessage.class, Color.green);
	}

	@Override
	public List<String> nextHops(String group) {
		ILinkLayerService ll = downstream(ILinkLayerService.class);
		ArrayList<String> nodes = new ArrayList<String>();
		for (Node n: ll.localNodes()) nodes.add(n.name());
		if (fPrunes.containsKey(group)) {
			for (String s: fPrunes.get(group).prunes()) nodes.remove(s);
		}
		return nodes;
	}

	@Override
	public Set<String> uniqueNextHops(String group) {
		return new HashSet<String>(nextHops(group));
	}

	@Override
	public Set<String> routeParticipants(String group) {
		return null;
	}
	

	@Override
	public boolean joined(String group) {
		return fRegisteredGroups.contains(group);
	}
}
