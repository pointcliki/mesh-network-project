package com.pointcliki.meshnetwork.protocol.dvmrp;

import com.pointcliki.meshnetwork.message.MulticastControlMessage;

public class DVMRPJoinMessage extends MulticastControlMessage {

	public DVMRPJoinMessage(String origin, String group) {
		super(origin, group);
	}

	public String toString() {
		return "[Join " + origin() + " from " + group() + "]";
	}
}

