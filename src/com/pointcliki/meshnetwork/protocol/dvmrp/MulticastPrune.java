package com.pointcliki.meshnetwork.protocol.dvmrp;

import java.util.HashSet;
import java.util.Set;

public class MulticastPrune {

	private boolean fSelfPruned = false;
	private HashSet<String> fPruned;
	private HashSet<String> fNotified;
	
	public MulticastPrune() {
		fPruned = new HashSet<String>();
		fNotified = new HashSet<String>();
	}
	
	public void addPrune(String name) {
		fPruned.add(name);
	}
	
	public void removePrune(String name) {
		fPruned.remove(name);
	}
	
	public boolean selfPruned() {
		return fSelfPruned;
	}
	
	public void setSelfPruned(boolean pruned) {
		fSelfPruned = pruned;
	}
	
	public boolean notified(String neighbour) {
		return fNotified.contains(neighbour);
	}
	
	public void addNotified(String neighbour) {
		fNotified.add(neighbour);
	}
	
	public boolean contains(String name) {
		return fPruned.contains(name);
	}
	
	public Set<String> prunes() {
		return fPruned;
	}
	
	public void flushCache() {
		fNotified.clear();
	}
	
	public String toString() {
		return "<MulticastPrune " + fSelfPruned + ": " + fPruned + ">";
	}
}
