package com.pointcliki.meshnetwork.protocol.nocy;

import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class NoCyData extends ProtocolData {
	
	public static final String NOCY_EXPIRE_TIME = "[NoCy] Expire Time";

	public NoCyData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(NOCY_EXPIRE_TIME, "frames", 20));
	}

	@Override
	public void startup() {
	}

	@Override
	public void finish() {
	}

	@Override
	public void beginTick() {
	}

	@Override
	public void endTick() {
	}

	@Override
	public String info() {
		return "NoCy is a protocol which prevents tagged messages from cycling around network paths and can act as an alternative to a TimeToLive service";
	}

	public int expireTime() {
		return getOption(NOCY_EXPIRE_TIME, int.class);
	}
}
