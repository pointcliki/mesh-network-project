package com.pointcliki.meshnetwork.protocol.nocy;

import com.pointcliki.meshnetwork.message.ITaggedMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.utils.LeakySet;

public class NoCy extends Protocol {
	
	private LeakySet<String> fSeen;
	
	public NoCy() {}
	
	public NoCy(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		fSeen = new LeakySet<String>();
	}
	
	@Override
	public boolean send(String destination, Message m) {
		boolean okay = super.send(destination, m);
		
		// See our own messages
		if (okay && m instanceof ITaggedMessage) {
			ITaggedMessage tm = (ITaggedMessage) m;
			fSeen.addSoft(tm.tag(), data().expireTime());
		}
		
		return okay;
	}
	
	@Override
	public boolean receive(String source, Message m) {
		// Ignore messages if we've seen them recently already
		if (m instanceof ITaggedMessage) {
			ITaggedMessage tm = (ITaggedMessage) m;
			if (fSeen.contains(tm.tag())) return false;
		}
		
		return super.receive(source, m);
	}
	
	@Override
	public void tick(long frame) {
		fSeen.update(frame);
		super.tick(frame);
	}
	

	@Override
	protected void createEventListeners() {}

	@Override
	public String name() {
		return "NoCy";
	}

	@Override
	protected ProtocolData createData() {
		return new NoCyData(this);
	}
	
	@Override
	public NoCyData data() {
		return (NoCyData) super.data();
	}

}
