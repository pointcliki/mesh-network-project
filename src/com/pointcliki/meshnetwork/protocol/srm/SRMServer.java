package com.pointcliki.meshnetwork.protocol.srm;

import java.util.HashSet;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.transport.TransportWindow;

public class SRMServer extends TransportWindow {
	
	private HashSet<Integer> fNacks;

	public SRMServer(SRM srm, String connection, String dest, SRMData data) {
		super(srm, connection, dest, data.initialWindowSize(), data.retryLimit(), data.retryThreshold());
		fNacks = new HashSet<Integer>();
	}

	@Override
	public boolean receive(String source, Message m) {
		// Server only handles NACK messages
		if (m instanceof SRMNackMessage) {
			SRMNackMessage nack = (SRMNackMessage) m;
			fNacks.add(nack.sequenceNumber());
			checkWindow();
			fire(SRMData.SRM_NACK_EVENT, 1);
			return true;
		}
		return false;
	}

	@Override
	public boolean completed(int sequenceNumber) {
		return !fNacks.contains(sequenceNumber);
	}

	@Override
	public boolean skippable(int sequenceNumber) {
		return true;
	}

	@Override
	protected void onTransmit(int sequenceNumber) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLeave(String participant) {
		// Don't need to do anything
	}
}

