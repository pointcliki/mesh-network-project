package com.pointcliki.meshnetwork.protocol.srm;

import java.util.Set;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IMulticastForwardingService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.transport.TransportProtocol;

public class SRM extends TransportProtocol {

public SRM() {}
	
	public SRM(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
	}
	
	@Override
	public String name() {
		return "SRM";
	}

	@Override
	protected ProtocolData createData() {
		return new SRMData(this);
	}
	@Override
	public SRMData data() {
		return (SRMData) super.data();
	}

	@Override
	protected SRMServer createServer(String c) {
		// Create a new SRMServer. In the simple model
		// we assume we are connecting to the group
		// attributed to this node
		String group = "&" + node().name();
		String connection = "!" + node().name();
		return new SRMServer(this, connection, group, data());
	}

	@Override
	public SRMClient createClient(String c) {
		return new SRMClient(this, c, "&" + c.substring(1), data());
	}

	@Override
	public Set<String> participants(String connection) {
		// In our simple model we only allow a single connection based on the node's group
		return downstream(IMulticastForwardingService.class).routeParticipants("&" + node().name());
	}

}

