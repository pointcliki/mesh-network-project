package com.pointcliki.meshnetwork.protocol.srm;

import com.pointcliki.meshnetwork.transport.TransportAckMessage;

public class SRMNackMessage extends TransportAckMessage {

	public SRMNackMessage(String connection, int seq) {
		super(connection, seq);
	}
}

