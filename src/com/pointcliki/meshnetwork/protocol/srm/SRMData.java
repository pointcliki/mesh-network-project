package com.pointcliki.meshnetwork.protocol.srm;

import com.pointcliki.meshnetwork.model.Option;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.protocol.tcp.TCPData;

public class SRMData extends ProtocolData {

	public static final String SRM_NACK_EVENT = "[SRM] Nack Event";

	public SRMData(Protocol p) {
		super(p);
		options().add(new Option<Integer>(TCPData.TRANSPORT_WINDOW, "Messages per RTT", 1));
		options().add(new Option<Integer>(TCPData.TRANSPORT_BUFFER, "Messages", 1000));
		options().add(new Option<Integer>(TCPData.TRANSPORT_RETRY_LIMIT, "Attempts", 2));
		options().add(new Option<Integer>(TCPData.TRANSPORT_RETRY_THRESHOLD, "% nodes silent", 50));
	}

	@Override
	public String info() {
		return "Static Reliable Multicast creates a connection for a multicast group that allows local retransmit to occur in the place of failure.";
	}

	public int retryLimit() {
		return getOption(TCPData.TRANSPORT_RETRY_LIMIT, int.class);
	}
	
	public int retryThreshold() {
		return getOption(TCPData.TRANSPORT_RETRY_THRESHOLD, int.class);
	}
	
	public int initialWindowSize() {
		return getOption(TCPData.TRANSPORT_WINDOW, int.class);
	}
	
	public int bufferSize() {
		return getOption(TCPData.TRANSPORT_BUFFER, int.class);
	}
}

