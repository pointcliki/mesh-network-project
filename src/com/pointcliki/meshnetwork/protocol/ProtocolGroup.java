package com.pointcliki.meshnetwork.protocol;

import java.util.ArrayList;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.model.SimulationSubItem;

public class ProtocolGroup extends SimulationSubItem<ProtocolData> {
	
	private boolean fLibrary;
	
	public ProtocolGroup(String name, ArrayList<Protocol> protocols) {
		this(null, name, protocols, true);
	}
	
	public ProtocolGroup(SimulationItem sim, String name, ArrayList<Protocol> protocols, boolean library) {
		super(sim, name, library ? Icons.BooksIcon : Icons.StackIcon);
		fLibrary = library;
		for (Protocol p: protocols) fItems.add(p.data());
	}

	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) {
		
		for (ProtocolData item: fItems) {
			if (item != null) model.insertNodeInto(new IconLabelNode(model, item, fLibrary), node, model.getChildCount(node));
		}
		return node;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Protocol> T getProtocol(String name, Class<T> cls) {
		for (ProtocolData item: fItems) {
			if (item.name().equals(name)) return (T) item.protocol();
		}
		return null;
	}
	
	protected <T extends Protocol> IconLabelNode childOfType(MutableTreeNode parent, String name) {
		for (int i = 0; i < parent.getChildCount(); i++) {
			if (((ProtocolData) ((IconLabelNode) parent.getChildAt(i)).item()).protocol().name().equals(name)) return (IconLabelNode) parent.getChildAt(i);
		}
		return null;
	}
	
	public void add(DefaultTreeModel model, MutableTreeNode parent, ProtocolData data, int index) {
		// Check whether we've already got this protocol type
		IconLabelNode n = childOfType(parent, data.protocol().name());
		if (n != null) {
			if (n.getParent().getIndex(n) < index) index--;
			model.removeNodeFromParent(n);
			fItems.remove(n.item());
		}
		
		fItems.add(data);
		model.insertNodeInto(new IconLabelNode(model, data, false), parent, index);
		SimulationsPanel.getInstance().save();
	}
}
