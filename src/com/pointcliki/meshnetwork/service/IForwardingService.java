package com.pointcliki.meshnetwork.service;

import com.pointcliki.meshnetwork.simulator.IReceiver;

public interface IForwardingService extends IReceiver {
	
	public static final String EVENT_JOIN = "[Multicast] Join";
	public static final String EVENT_LEAVE = "[Multicast] Leave";

	/**
	 * Returns the next hop of a route to the destination
	 * @param destination The node to select the route to
	 * @return The name of the first node along the selected route
	 */
	public String nextHop(String destination);
	
	/**
	 * Forces a route to be found to the destination
	 * @param destination The destination of the path
	 * @return Whether a route currently exists
	 */
	public boolean route(String destination);
	
	/**
	 * Returns the hop count of the route to the destination node
	 * @param destination The node to measure the distance to
	 * @return Either the hop count or -1 if there is no route
	 */
	public int distance(String destination);

	/**
	 * Returns the last hop of the most recent packet to be received by this service
	 * @return The name of the node that sent the message
	 */
	public String receivedHop();
}
