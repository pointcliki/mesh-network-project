package com.pointcliki.meshnetwork.service;

import java.util.List;
import java.util.Set;

public interface IMulticastForwardingService {
	
	public List<String> nextHops(String group);
	public Set<String> uniqueNextHops(String group);
	
	public Set<String> routeParticipants(String group);
}
