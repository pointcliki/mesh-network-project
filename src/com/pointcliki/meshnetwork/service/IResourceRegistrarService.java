package com.pointcliki.meshnetwork.service;

public interface IResourceRegistrarService {

	public void advertiseResource(String resource);
	
	public void clearResource(String resource);
}
