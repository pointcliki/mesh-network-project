package com.pointcliki.meshnetwork.service;

import java.util.Set;

public interface IResourceDiscoveryService {
	public Set<String> providers(String resource);
	
	public void findProviders(String resource, int count, int maxRange);
}
