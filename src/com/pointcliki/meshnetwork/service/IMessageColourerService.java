package com.pointcliki.meshnetwork.service;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.utils.MessageColorer;

public interface IMessageColourerService {

	public Color color(Message m);
	
	public void addColorer(Class<? extends Message> cls, MessageColorer colorer);
	
	public void clearColorer(Class<? extends Message> cls);

	public void addColorer(Class<? extends Message> cls, Color color);
}
