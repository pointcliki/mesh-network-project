package com.pointcliki.meshnetwork.service;

public interface IMulticastGroupLeaderService {

	public String groupLeader(String group);
	
	public boolean ownGroup(String group);
}
