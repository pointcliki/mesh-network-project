package com.pointcliki.meshnetwork.service;

public interface IMulticastGroupRegistrarService {

	public boolean join(String group);
	
	public boolean leave(String group);
	
	public boolean joined(String group);
}
