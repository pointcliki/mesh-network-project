package com.pointcliki.meshnetwork.service;

import java.util.Set;

import com.pointcliki.meshnetwork.transport.IClient;
import com.pointcliki.meshnetwork.transport.IServer;

/**
 * A connection service keeps a list of connections to entities
 * in the network. These can be either single nodes or multicast
 * groups.
 * 
 * @author Hugheth
 */
public interface ISocketService {

	public IClient client(String connection);
	
	public IClient connect(String destination);
	
	public IServer server(String connection);
	
	public Set<String> participants(String connection);
}
