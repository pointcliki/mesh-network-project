package com.pointcliki.meshnetwork.service;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.simulator.IReceiver;
import com.pointcliki.meshnetwork.simulator.ISender;

public interface IApplicationService extends ISender, IReceiver {
	
	public Protocol downstream();
	
	public <V> V downstream(Class<V> type);
}
