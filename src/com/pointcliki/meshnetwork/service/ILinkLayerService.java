package com.pointcliki.meshnetwork.service;

import java.util.List;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.simulator.Node;

public interface ILinkLayerService {

	public List<Node> localNodes();
	
	public Node local(String s);
	
	public void calculateLocalNodes();
	
	public int state();
	
	public void saveState();
	
	public int lastState();
	
	public int queueSize();
	
	public int queueCapacity();
	
	public int linkCost(String dest);

	public void finish();

	public boolean tryToSend();

	public void sendPacket();

	public void startTryingToSend();
	
	public Color messageColor();
}
