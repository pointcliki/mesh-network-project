package com.pointcliki.meshnetwork.model;

import javax.swing.tree.DefaultTreeModel;

import com.pointcliki.meshnetwork.ui.panel.ResultsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class Batch extends Item {

	private IconLabelNode fNode;

	public Batch(String name) {
		super(name, Icons.PlayIcon);
		
	}
	
	public void add(SimulationItem sim) {
		DefaultTreeModel model = (DefaultTreeModel) ResultsPanel.getInstance().tree().getModel();
		IconLabelNode node = new IconLabelNode(model, sim);
		model.insertNodeInto(node, fNode, fNode.getChildCount());
	}

	public void setNode(IconLabelNode b) {
		fNode = b;
	}

}
