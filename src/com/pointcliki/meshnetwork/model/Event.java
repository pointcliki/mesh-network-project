package com.pointcliki.meshnetwork.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.tree.DefaultTreeModel;

import org.jfree.data.xy.YIntervalSeries;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class Event extends ProtocolSubItem {
	
	private String fInfo = "";
	
	private ArrayList<EventInstance> fEvents;
	private DefaultListModel fList;
	private YIntervalSeries fSeries;
	
	public Event(String name, Protocol p) {
		super(name, Icons.EventIcon, p);
		
		fEvents = new ArrayList<EventInstance>();
	}

	@Override
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) {
		return node;
	}
	
	public String info() {
		return fInfo;
	}
	
	public List<EventInstance> events() {
		return fEvents;
	}
	public void add(EventInstance ev) {
		fEvents.add(ev);
		if (fSeries != null) {
			fSeries.add(ev.time(), 0, 0, 0);
			fSeries.add(ev.time(), ev.value(), ev.value(), ev.value());
			fSeries.add(ev.time(), 0, 0, 0);
		}
	}
	
	public YIntervalSeries series() {
		if (fSeries == null) fSeries = new YIntervalSeries(name());
		for (EventInstance ev: fEvents) {
			fSeries.add(ev.time(), 0, 0, 0);
			fSeries.add(ev.time(), ev.value(), ev.value(), ev.value());
			fSeries.add(ev.time(), 0, 0, 0);
		}
		return fSeries;
	}

	public ListModel listModel() {
		if (fList == null) {
			fList = new DefaultListModel();
			for (EventInstance ev: fEvents) {
				fList.addElement(ev.name() + " @ " + ev.time() + " : " + ev.value());
			}
		}
		return fList;
	}
}
