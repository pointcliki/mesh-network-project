package com.pointcliki.meshnetwork.model;

public enum ScriptType {
	Mobility, Flow, Layout
}
