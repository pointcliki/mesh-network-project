package com.pointcliki.meshnetwork.model;

import org.newdawn.slick.Graphics;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class Annotation extends ProtocolSubItem {

	public Annotation(String name, Protocol p) {
		super(name, Icons.DrawIcon, p);
	}

	public void render(Simulator simulator, Graphics g, long time) {}
}
