package com.pointcliki.meshnetwork.model;

import java.util.ArrayList;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class ScriptGroup extends SimulationSubItem<Script> {

	private boolean fLibrary;
	
	public ScriptGroup(String name, ArrayList<Script> scripts) {
		this(null, name, scripts, true);
	}
	
	public ScriptGroup(SimulationItem sim, String name, ArrayList<Script> scripts, boolean library) {
		super(sim, name, library ? Icons.BooksIcon : Icons.ScriptIcon);
		fLibrary = library;
		fItems = scripts;
	}
	@Override
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) {
		
		for (Script s: fItems) {
			model.insertNodeInto(new IconLabelNode(model, s, fLibrary), node, model.getChildCount(node));
		}
		return node;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Script> T get(String name) {
		for (Script s: fItems) {
			if (s.name().equals(name)) return (T) s;
		}
		return null;
	}
	
	protected <T extends Script> IconLabelNode childOfType(MutableTreeNode parent, String name) {
		for (int i = 0; i < parent.getChildCount(); i++) {
			if (((Script) ((IconLabelNode) parent.getChildAt(i)).item()).name().equals(name)) return (IconLabelNode) parent.getChildAt(i);
		}
		return null;
	}
	
	@Override
	public void add(DefaultTreeModel model, MutableTreeNode parent, Script script, int index) {
		// Check whether we've already got this Script type
		IconLabelNode n = childOfType(parent, script.name());
		if (n != null) {
			if (n.getParent().getIndex(n) < index) index--;
			model.removeNodeFromParent(n);
			fItems.remove(n.item());
		}
		
		fItems.add(script);
		model.insertNodeInto(new IconLabelNode(model, script, false), parent, index);
		SimulationsPanel.getInstance().save();
	}
}
