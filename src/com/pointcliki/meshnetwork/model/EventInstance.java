package com.pointcliki.meshnetwork.model;

public class EventInstance {

	private float fValue;
	private long fTime;
	private String fName;
	private String fInfo;

	public EventInstance(String name, long time, float value) {
		this(name, time, value, "");
	}
	
	public EventInstance(String name, long time, float value, String info) {
		fName = name;
		fTime = time;
		fValue = value;
		fInfo = info;
	}

	public String name() {
		return fName;
	}
	
	public long time() {
		return fTime;
	}
	
	public float value() {
		return fValue;
	}
	
	public String info() {
		return fInfo;
	}
}
