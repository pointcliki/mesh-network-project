package com.pointcliki.meshnetwork.model;

import com.l2fprod.common.propertysheet.DefaultProperty;

public class Option<T extends Object> {
	
	private String fName;
	private String fUnits;
	private T fValue;
	
	public Option(String name, String units, T value) {
		fName = name;
		fUnits = units;
		fValue = value;
	}
	
	public String name() {
		return fName;
	}
	
	public String units() {
		return fUnits;
	}
	
	public T value() {
		return fValue;
	}

	public DefaultProperty property() {
		DefaultProperty prop = new DefaultProperty();
		prop.setName(fName);
		prop.setDisplayName("<html><b>" + fName + "</b> <small>" + fUnits + "</small></html>");
		prop.setEditable(true);
		prop.setValue(fValue);
		prop.setType(fValue.getClass());
		return prop;

	}

	@SuppressWarnings("unchecked")
	public void setValue(Object object) {
		fValue = (T) object;
	}
	
	public void importFromJSON(String s) {
		if (s.equals("")) return;
		if (fValue instanceof Boolean) setValue(Boolean.parseBoolean(s));
		else if (fValue instanceof Integer) setValue(Integer.parseInt(s));
		else if (fValue instanceof Float) setValue(Float.parseFloat(s));
		else setValue(s);
	}
}
