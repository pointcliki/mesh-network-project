package com.pointcliki.meshnetwork.model;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class SimulationSubItem<T extends Item> extends Item {
	
	protected ArrayList<T> fItems;
	private SimulationItem fSimulation;
	
	public SimulationSubItem(SimulationItem sim, String name, Icon icon) {
		this(sim, name, icon, new ArrayList<T>());
	}

	public SimulationSubItem(SimulationItem sim, String name, Icon icon, ArrayList<T> items) {
		super(name, icon);
		fSimulation = sim;
		fItems = items;
	}
	
	public ArrayList<T> items() {
		return fItems;
	}
	
	@Override
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) {
		int i = 0;
		for (T item: fItems) {
			model.insertNodeInto(new IconLabelNode(model, item, false), node, i);
			i++;
		}
		return node;
	}
	
	public void add(DefaultTreeModel model, MutableTreeNode parent, T item, int index) {
		fItems.add(index, item);
		model.insertNodeInto(new IconLabelNode(model, item, false), parent, index);
		SimulationsPanel.getInstance().save();
	}

	public void remove(DefaultTreeModel model, IconLabelNode n, T item) {
		fItems.remove(item);
		model.removeNodeFromParent(n);
		SimulationsPanel.getInstance().save();
	}
	
	@SuppressWarnings("unchecked")
	public void importFromJSON(JSONObject obj, SimulationItem item, Class<? extends Item> cls) throws JSONException {
		fItems.clear();
		
		super.importFromJSON(obj);
		
		JSONArray arr = obj.getJSONArray("items");
		for (int i = 0; i < arr.length(); i++) {
			JSONObject obit = arr.getJSONObject(i);
			try {
				if (cls.equals(ProtocolData.class)) {
					
					ArrayList<URL> urls = new ArrayList<URL>();
					urls.add(new URL("file://./bin"));
					
					File dir = new File("protocols");
					if (dir.isDirectory()) {
						for (String s: dir.list()) {
							if (s.endsWith(".jar")) {
								urls.add(new URL("file://./protocols/" + s));
							}
						}
					}
					
					URLClassLoader loader = new URLClassLoader(urls.toArray(new URL[urls.size()]));
					ProtocolData data = ((Protocol) Class.forName(obit.getString("class"), true, loader).newInstance()).data();
					data.importFromJSON(obit);
					fItems.add((T) data);
				
				} else if (cls.equals(Script.class)) {
					Script s = new Script(obit.getString("name"), ScriptType.valueOf(obit.getString("type")));
					fItems.add((T) s);
				} else {
					Class<? extends Item> scls = null;
					T inst;
					Protocol p = fSimulation.stack().getProtocol(obit.getString("protocol"), null);

					if (p != null && p.data() != null) {
						if (cls.equals(Annotation.class)) {
							scls = p.data().getAnnotation(obit.getString("name")).getClass();
						}
						if (cls.equals(Event.class)) scls = p.data().getEvent(obit.getString("name")).getClass();
						if (cls.equals(Metric.class)) scls = p.data().getMetric(obit.getString("name")).getClass();
						
						if (scls != cls) {
							inst = (T) scls.getConstructor(Protocol.class).newInstance(p);
						} else {
							inst = (T) cls.getConstructor(String.class, Protocol.class).newInstance(obit.getString("name"), p);
						}
						fItems.add(inst);
					}
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		
		JSONArray items = new JSONArray();
		for (Item i: fItems) {
			items.put(i.exportToJSON());
		}
		
		obj.put("items", items);
		return obj;
	}
}
