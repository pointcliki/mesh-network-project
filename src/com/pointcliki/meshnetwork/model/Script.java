package com.pointcliki.meshnetwork.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.ui.task.DeleteItemTask;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class Script extends Item {

	private ScriptType fType;
	
	public Script(String name, ScriptType type) {
		super(name, Icons.ScriptIcon);
		fType = type;
		
		tasks().add(new DeleteItemTask());
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("type", fType.toString());
		return obj;
	}
	
	public ScriptType type() {
		return fType;
	}
}
