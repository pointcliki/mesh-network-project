package com.pointcliki.meshnetwork.model;

import javax.swing.AbstractAction;

public abstract class Task extends AbstractAction {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -6860028230382454793L;
	
	private Item fActive;
	
	public Task(String name) {
		super(name);
	}
	
	public void setActiveItem(Item item) {
		fActive = item;
	}
	
	public Item activeItem() {
		return fActive;
	}
}
