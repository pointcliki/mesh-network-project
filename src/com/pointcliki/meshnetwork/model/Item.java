package com.pointcliki.meshnetwork.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.tree.DefaultTreeModel;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;

public class Item implements Cloneable {

	private String fName;
	private Icon fIcon;
	private ArrayList<Task> fTasks;
	private ArrayList<Option<? extends Object>> fOptions;
	
	public Item(String name, Icon icon) {
		fName = name;
		fIcon = icon;
		fTasks = new ArrayList<Task>();
		fOptions = new ArrayList<Option<? extends Object>>();
	}
	
	public List<Task> tasks() {
		return fTasks;
	}
	
	public List<Option<? extends Object>> options() {
		return fOptions;
	}
	
	public String name() {
		return fName;
	}
	
	public Icon icon() {
		return fIcon;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getOption(String name, Class<T> cls) {
		for (Option<? extends Object> o: options()) {
			if (name.equals(o.name())) return (T) o.value();
		}
		return null;
	}
	
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) { return null; };
	/*
	public Item copyToInstance(SimulationItem sim) {
		
	}
	*/
	
	public Item clone() {
		try {
			return (Item) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	public void importFromJSON(JSONObject obj) throws JSONException {
		for (Option<? extends Object> o: fOptions) {
			o.importFromJSON(obj.getJSONObject("options").optString(o.name()));
		}
	}
	
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		JSONObject opts = new JSONObject();
		for (Option<? extends Object> o: options()) {
			opts.putOpt(o.name(), o.value().toString());
		}
		obj.put("name", name());
		obj.put("options", opts);
		
		return obj;
	}
	
	public boolean equals(Object o) {
		return (o instanceof Item && name().equals(((Item) o).name()));
	}
}

