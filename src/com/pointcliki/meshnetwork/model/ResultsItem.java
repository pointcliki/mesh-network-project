package com.pointcliki.meshnetwork.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.ui.panel.ResultsPanel;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class ResultsItem extends Item {

	private IconLabelNode fNode;
	private SimulationItem fParentSim;
	private SimulationItem[] fChildSims;
	private Batch fBatch;

	public ResultsItem(String name) {
		super(name, Icons.MetricIcon);
		
	}
	
	public void setNode(IconLabelNode n) {
		fNode = n;
	}

	public void runSimulation(SimulationItem sim) throws JSONException {
		fParentSim = sim;
		
		DefaultTreeModel model = (DefaultTreeModel) ResultsPanel.getInstance().tree().getModel();
		
		fBatch = new Batch("Batch 1");
		IconLabelNode b = new IconLabelNode(model, fBatch);
		fBatch.setNode(b);
		
		model.insertNodeInto(new IconLabelNode(model, sim.stack()), fNode, model.getChildCount(fNode));
		model.insertNodeInto(new IconLabelNode(model, sim.scripts()), fNode, model.getChildCount(fNode));
		model.insertNodeInto(new IconLabelNode(model, sim.annotations()), fNode, model.getChildCount(fNode));
		model.insertNodeInto(new IconLabelNode(model, sim.events()), fNode, model.getChildCount(fNode));
		model.insertNodeInto(new IconLabelNode(model, sim.metrics()), fNode, model.getChildCount(fNode));
		model.insertNodeInto(b, fNode, model.getChildCount(fNode));
				
		JSONObject obj = sim.exportToJSON();
		
		final ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
		
		int its = sim.iterations();
		
		fChildSims = new SimulationItem[its];
		
		for (int i = 0; i < its; i++) {
			SimulationItem it = new SimulationItem(sim.name() + " #" + i, this);
			it.importFromJSON(obj);
			fChildSims[i] = it;
		}
		// Create a thread which calls back to the UI thread when the pool has completed execution
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					for (int i = 0; i < fChildSims.length; i++) {
						final SimulationItem item = fChildSims[i];
						executor.schedule(new Simulator(item, i), 0, TimeUnit.SECONDS);
					}
					// Shutdown now so that the executor will terminate when all the queued simulations have completed 
					executor.shutdown();
					// Await termination
					executor.awaitTermination(30, TimeUnit.SECONDS);
					done();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}, "Simulation Scheduler").start();		 
	}
	
	protected void done() {
		for (Metric m: fParentSim.metrics().items()) {
			// Get the equivalent metrics in the children
			ArrayList<Metric> ms = new ArrayList<Metric>();
			for (SimulationItem it: fChildSims) {
				for (Metric m2: it.metrics().items()) {
					if (m2.equals(m)) {
						ms.add(m2);
						break;
					}
				}
			}
			
			for (int i = 0; i < fParentSim.duration(); i++) {
					float avg = 0;
					int n = 0;
					for (Metric m2: ms) {
						if (m2.results().size() > i) {
							avg += m2.results().get(i);
							n++;
						}
					}
					avg = avg / n;
					float sd = 0;
					for (Metric m2: ms) {
						if (m2.results().size() > i) {
							sd += Math.pow(m2.results().get(i) - avg, 2);
						}
					}
					sd = sd / (n - 1);
					sd = (float) Math.sqrt(sd);
					
					m.contribute(avg, sd);
			}
		}
		for (Event ev: fParentSim.events().items()) {
			HashMap<Long, Float> map = new HashMap<Long, Float>();
			for (SimulationItem it: fChildSims) {
				for (Event ev2: it.events().items()) {
					if (ev2.equals(ev)) {
						for (EventInstance inst: ev2.events()) {
							if (!map.containsKey(inst.time())) map.put(inst.time(), 0f);
							map.put(inst.time(), map.get(inst.time()) + inst.value());
						}
						break;
					}
				}
			}
			for (Entry<Long, Float> ent: map.entrySet()) {
				ev.add(new EventInstance(ev.name(), ent.getKey(), ent.getValue() / fChildSims.length));
			}
		}
	}

	public void started(final SimulationItem sim) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fBatch.add(sim);
			}
		});
	}
}
