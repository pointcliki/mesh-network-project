package com.pointcliki.meshnetwork.model;

import java.util.ArrayList;

import javax.swing.tree.DefaultTreeModel;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.protocol.ProtocolGroup;
import com.pointcliki.meshnetwork.protocol.mac.MAC;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.ui.task.DeleteSimulationTask;
import com.pointcliki.meshnetwork.ui.task.DuplicateSimulationTask;
import com.pointcliki.meshnetwork.ui.task.RunSimulationTask;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class SimulationItem extends Item {
	
	private ProtocolGroup fGroup;
	private ScriptGroup fScriptGroup;
	private SimulationSubItem<Annotation> fAnnotations;
	private SimulationSubItem<Event> fEvents;
	private SimulationSubItem<Metric> fMetrics;
	private ResultsItem fResults;
	private Simulator fSimulator;
	
	public SimulationItem(String name) {
		this(name, null);
	}
	
	public SimulationItem(String name, ResultsItem results) {
		super(name, Icons.PlayIcon);
		
		fResults = results;
		
		ArrayList<Protocol> arr = new ArrayList<Protocol>();
		arr.add(new MAC());
		
		fGroup = new ProtocolGroup(this, "Protocol Stack", arr, false);
		fScriptGroup = new ScriptGroup(this, "Scripts", new ArrayList<Script>(), false);
		fAnnotations = new SimulationSubItem<Annotation>(this, "Annotations", Icons.DrawIcon);
		fEvents = new SimulationSubItem<Event>(this, "Events", Icons.EventIcon);
		fMetrics = new SimulationSubItem<Metric>(this, "Metrics", Icons.MetricIcon);
		
		options().add(new Option<Boolean>("Graphical", "", false));
		options().add(new Option<Integer>("Iterations", "", 1));
		options().add(new Option<Integer>("Step Duration", "ms per frame", 0));
		options().add(new Option<Integer>("Duration", "frames", 100));
		options().add(new Option<Integer>("Node Range", "pixels", 100));
		options().add(new Option<Integer>("Log Verbosity", "", 0));
		
		tasks().add(new RunSimulationTask());
		tasks().add(new DeleteSimulationTask());
		tasks().add(new DuplicateSimulationTask());
	}
	
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) {
		if (fResults == null) model.insertNodeInto(new IconLabelNode(model, fGroup), node, model.getChildCount(node));
		if (fResults == null) model.insertNodeInto(new IconLabelNode(model, fScriptGroup), node, model.getChildCount(node));
		if (fResults == null) model.insertNodeInto(new IconLabelNode(model, fAnnotations), node, model.getChildCount(node));
		model.insertNodeInto(new IconLabelNode(model, fEvents), node, model.getChildCount(node));
		model.insertNodeInto(new IconLabelNode(model, fMetrics), node, model.getChildCount(node));
		return node;
	}
	
	public ProtocolGroup stack() {
		return fGroup;
	}	
	
	public ScriptGroup scripts() {
		return fScriptGroup;
	}
	public SimulationSubItem<Annotation> annotations() {
		return fAnnotations;
	}
	
	@Override
	public void importFromJSON(JSONObject obj) throws JSONException {
		super.importFromJSON(obj);
		fGroup.importFromJSON(obj.getJSONObject("stack"), this, ProtocolData.class);
		fScriptGroup.importFromJSON(obj.getJSONObject("scripts"), this, Script.class);
		fEvents.importFromJSON(obj.getJSONObject("events"), this, Event.class);
		fMetrics.importFromJSON(obj.getJSONObject("metrics"), this, Metric.class);
		fAnnotations.importFromJSON(obj.getJSONObject("annotations"), this, Annotation.class);
	}
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("stack", fGroup.exportToJSON());
		obj.put("scripts", fScriptGroup.exportToJSON());
		obj.put("annotations", fAnnotations.exportToJSON());
		obj.put("metrics", fMetrics.exportToJSON());
		obj.put("events", fEvents.exportToJSON());
		return obj;
	}
	
	public int iterations() {
		return getOption("Iterations", int.class);
	}
	public boolean graphical() {
		return getOption("Graphical", boolean.class);
	}
	public int frameDuration() {
		return getOption("Step Duration", int.class);
	}
	public int nodeRange() {
		return getOption("Node Range", int.class);
	}
	public int duration() {
		return getOption("Duration", int.class);
	}
	
	public int verbosity() {
		return getOption("Log Verbosity", int.class);
	}

	public void setSimulator(Simulator simulator) {
		fSimulator = simulator;
		for (ProtocolData d: stack().items()) {
			d.setSimulation(this);
		}
	}
	
	public ResultsItem results() {
		return fResults;
	}
	
	public Simulator simulator() {
		return fSimulator;
	}

	public void contributeToMetric(String name, float f) {
		for (Metric m: fMetrics.items()) {
			if (m.name().equals(name)) {
				m.contribute(f);
			}
		}
	}

	public void started() {
		results().started(this);
	}

	public void contributeToEvent(EventInstance ev) {
		for (Event e: fEvents.items()) {
			if (e.name().equals(ev.name())) {
				e.add(ev);
			}
		}
	}

	public SimulationSubItem<Event> events() {
		return fEvents;
	}
	public SimulationSubItem<Metric> metrics() {
		return fMetrics;
	}
}
