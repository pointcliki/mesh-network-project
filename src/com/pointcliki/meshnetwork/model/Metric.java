package com.pointcliki.meshnetwork.model;

import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.tree.DefaultTreeModel;

import org.jfree.data.xy.YIntervalSeries;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.ui.utils.IconLabelNode;
import com.pointcliki.meshnetwork.ui.utils.Icons;

public class Metric extends ProtocolSubItem {
	
	private ArrayList<Float> fList;
	private ArrayList<Float> fDiffs;
	private DefaultListModel fListModel;
	private YIntervalSeries fSeries;

	public Metric(String name, Protocol p) {
		super(name, Icons.MetricIcon, p);
		
		fList = new ArrayList<Float>();
		fDiffs = new ArrayList<Float>();
	}
	
	public void contribute(float f) {
		contribute(f, 0);
	}
	
	public void contribute(float f, float d) {
		fList.add(f);
		fDiffs.add(d);
		if (fListModel != null) fListModel.addElement(f);
		if (fSeries != null) {
			fSeries.add(fList.size() - 1, f, f - d, f + d);
		}
	}
	
	public ArrayList<Float> results() {
		return fList;
	}
	
	public DefaultListModel listModel() {
		if (fListModel == null) {
			fListModel = new DefaultListModel();
			for (float f: fList) fListModel.addElement(f);
		}
		return fListModel;
	}
	
	public YIntervalSeries series() {
		if (fSeries == null) {
			int i = 0;
			fSeries = new YIntervalSeries(name());
			for (float f: fList) {
				float d = fDiffs.get(i);
				fSeries.add(i, f, f - d, f + d);
				i++;
			}
		}
		return fSeries;
	}

	@Override
	public IconLabelNode populate(DefaultTreeModel model, IconLabelNode node) {
		return node;
	}
}
