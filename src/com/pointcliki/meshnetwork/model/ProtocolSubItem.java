package com.pointcliki.meshnetwork.model;

import javax.swing.Icon;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.ui.task.DeleteItemTask;

public class ProtocolSubItem extends Item {

	private Protocol fProtocol;

	public ProtocolSubItem(String name, Icon icon, Protocol protocol) {
		super(name, icon);
		fProtocol = protocol;
		tasks().add(new DeleteItemTask());
	}

	public Protocol protocol() {
		return fProtocol;
	}
	
	@Override
	public void importFromJSON(JSONObject obj) throws JSONException {
		super.importFromJSON(obj);
		
		obj.put("name", name());
		obj.put("protocol", fProtocol.name());
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("protocol", fProtocol.name());
		
		return obj;
	}
}
