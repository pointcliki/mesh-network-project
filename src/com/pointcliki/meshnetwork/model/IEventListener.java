package com.pointcliki.meshnetwork.model;

import com.pointcliki.meshnetwork.protocol.Protocol;

public interface IEventListener<P extends Protocol> {

	public void handleEvent(P p, EventInstance ev);
}
