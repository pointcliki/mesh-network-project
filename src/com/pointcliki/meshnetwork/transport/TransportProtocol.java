package com.pointcliki.meshnetwork.transport;

import java.util.HashMap;

import com.pointcliki.meshnetwork.message.IMulticastAckMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.model.EventInstance;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IMulticastGroupRegistrarService;
import com.pointcliki.meshnetwork.service.ISocketService;
import com.pointcliki.meshnetwork.simulator.Node;

public abstract class TransportProtocol extends Protocol implements ISocketService {

	private HashMap<String, IClient> fClients;
	private HashMap<String, IServer> fServers;
	
	public TransportProtocol() {}
	
	public TransportProtocol(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
		
		fClients = new HashMap<String, IClient>();
		fServers = new HashMap<String, IServer>();
	}
	
	@Override
	public boolean receive(String source, Message m) {
		if (m instanceof TransportSynMessage) {
			return handleSyn(source, (TransportSynMessage) m);
			
		} else if (m instanceof IMulticastAckMessage) {
			return handleAck(source, (IMulticastAckMessage) m);
			
		} else if (m instanceof TransportPayloadMessage) {
			return handlePayload(source, (TransportPayloadMessage) m);
			
		} else {
			// TODO: Remove debug
			System.err.println("Couldn't handle message " + m);
			return false;
		}
	}
	
	protected boolean handlePayload(String source, TransportPayloadMessage payload) {
		// Send up
		if (fClients.containsKey(payload.connection())) {
			fClients.get(payload.connection()).receive(source, payload);
		}
		return false;
	}

	protected boolean handleAck(String source, IMulticastAckMessage m) {
		return false;
	}

	protected boolean handleSyn(String source, TransportSynMessage m) {
		IServer serv = createServer(m.connection());
		if (serv != null) {
			fServers.put(m.connection(), serv);
			return true;
		}
		return false;
	}
	
	public IClient client(String c) {
		return fClients.get(c);
	}
	
	public IServer server(String c) {
		return fServers.get(c);
	}
	
	@Override
	public IClient connect(String dest) {
		String c = "!" + dest;
		String group = "&" + dest;
		
		// Check we haven't already connected
		if (client(c) != null) return client(c);
		
		// Send a join message if we don't belong to the group
		IMulticastGroupRegistrarService registrar = downstream(IMulticastGroupRegistrarService.class);
		if (!registrar.joined(group)) registrar.join(group);
		
		// Send a SYN message
		boolean okay = fDownStream.send(dest, new TransportSynMessage(node().name()));
		
		if (!okay) return null;
		
		IClient client = createClient(c);

		fClients.put(dest, client);
		
		return client;
	}
		
	protected abstract IClient createClient(String c);
	
	protected abstract IServer createServer(String c);

	@Override
	protected void createEventListeners() {
		// TODO: Increase window size on packet received
		
		// TODO: Half window size on loss
	}

	public void recieveServerEvent(IServer server, EventInstance e) {
		recieveEvent(e);
	}
}
