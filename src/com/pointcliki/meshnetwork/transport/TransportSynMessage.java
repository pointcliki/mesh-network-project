package com.pointcliki.meshnetwork.transport;

import com.pointcliki.meshnetwork.message.Message;

public class TransportSynMessage extends Message {

	private String fConnection;

	public TransportSynMessage(String connection) {
		fConnection = connection;
	}
	
	public String connection() {
		return fConnection;
	}
}

