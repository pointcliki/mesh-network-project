package com.pointcliki.meshnetwork.transport;

import java.util.Set;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.model.EventInstance;
import com.pointcliki.meshnetwork.protocol.tcp.TCPData;
import com.pointcliki.meshnetwork.utils.MessageQueue;
import com.pointcliki.meshnetwork.service.IForwardingService;

public abstract class TransportWindow implements IServer {
	
	private int fWindowSize;
	private int fTimeout;
	private int fTimeoutTicker = 0;
	private int fRetryLimit;
	private int fRetryTicker;
	private int fRetryThreshold;
	private int fWindowTicker = 0;
	private int fNextSequenceNumber = 0;
	private MessageQueue<TransportPayloadMessage> fQueuedMessages;
	private MessageQueue<TransportPayloadMessage> fSentMessages;
	private TransportProtocol fParent;
	private String fConnection;
	private String fDest;
	
	private int fValRTT;
	private int fValRTTVar;
	// TODO: Work out how to calculate this
	private int fValRate;
	
	public TransportWindow(TransportProtocol parent, String connection, String dest,
			int initialWindowSize, int retryLimit, int retryThreshold) {
		fParent = parent;
		fConnection = connection;
		fDest = dest;
		fWindowSize = initialWindowSize;
		fRetryLimit = retryLimit;
		fRetryThreshold = retryThreshold;
		fQueuedMessages = new MessageQueue<TransportPayloadMessage>(initialWindowSize);
		fSentMessages = new MessageQueue<TransportPayloadMessage>(initialWindowSize);
		
		calculateInitialTimeout();
	}
	
	public void calculateInitialTimeout() {
		fValRTT = 0;
		Set<String> set = fParent.participants(fConnection);
		
		IForwardingService s = fParent.downstream(IForwardingService.class);
		
		for (String node: set) {
			fValRTT = Math.max(fValRTT, s.distance(node) * 2);
		}
		fValRTTVar = 2;
		
		fTimeout = fValRTT + fValRTTVar;
	}
	
	public void updateTimeout(int rtt) {
		
	}

	public String source() {
		return fParent.node().name();
	}
	
	public String connection() {
		return fConnection;
	}
	
	public String destination() {
		return fDest;
	}
	
	public TransportProtocol parent() {
		return fParent;
	}
	
	@Override
	public boolean send(String dest, Message m) {
		// Can only send to this connection
		if (!dest.equals(fConnection)) return false;
		
		boolean okay = fQueuedMessages.push(new TransportPayloadMessage(connection(), fNextSequenceNumber++, m));
		if (!okay) return false;
		// Try sending the message
		return transmitMessage();
	}

	protected boolean transmitMessage() {
		boolean okay = true;
		
		if (!fSentMessages.full() && !fQueuedMessages.empty()) {
			TransportPayloadMessage m = fQueuedMessages.poll();
			fSentMessages.push(m);
			okay = fParent.downstream().send(fDest, m);			
			if (okay) {
				onTransmit(m.sequenceNumber());

				fWindowTicker++;
				
				fire(TCPData.TRANSPORT_RECIEVED_PACKET_EVENT, 1);
				
				// Fire event whenever window size has sent
				if (fWindowTicker == fWindowSize) {
					fWindowTicker = 0;
					fire(TCPData.TRANSPORT_RECIEVED_WINDOW_EVENT, 1);
				}
				fTimeoutTicker = 0;
			}
		}
		return okay;
	}
	
	@Override
	public abstract boolean receive(String source, Message message);
	
	/**
	 * Classes extending TransportWindow should return true if
	 * the message with the sequence number supplied has been
	 * received by all the participants successfully.
	 */
	public abstract boolean completed(int sequenceNumber);
	
	/**
	 * Classes extending TransportWindow should return true if
	 * the message with the sequence number supplied has been
	 * received by enough of the participants that it can be
	 * skipped rather than retransmitted
	 */
	public abstract boolean skippable(int sequenceNumber);
	
	protected abstract void onTransmit(int sequenceNumber);
	
	/**
	 * This method is called by the TransportProtocol when a
	 * participant leaves the connection, so that the window
	 * can handle this in a suitable manner
	 * @param participant
	 */
	public abstract void onLeave(String participant);
	
	/**
	 * Classes extending TransportWindow should call checkWindow
	 * whenever they believe the message at the head of the window
	 * has been received successfully by the connection participants
	 */
	protected void checkWindow() {
		// While the head of the message queue has been transmitted to all clients slide the window along
		while (completed(waitingOn())) {
				
			// No longer need the send message
			fSentMessages.poll();
			
			// Transmit another message now a space is free
			transmitMessage();
		}
	}
	
	private int waitingOn() {
		return fSentMessages.peek().sequenceNumber();
	}

	public void tick(long frame) {
		fTimeoutTicker++;
		// Check for timeout of ACKs
		if (fTimeoutTicker == fTimeout) retry();
	}

	private void retry() {
		fTimeoutTicker = 0;
		if (fRetryTicker == fRetryLimit || skippable(waitingOn())) {
			skip(); 
		} else {
			fRetryTicker++;
			
			// Don't touch the ACKs set because we only need people who
			// haven't already ACK'd the packet to ACK it before continuing
			
			boolean okay = fParent.downstream().send(fDest, fSentMessages.peek());
			if (!okay) skip();
			
			fire(TCPData.TRANSPORT_PACKET_RETRANSMIT, 1);
		}
	}

	private void skip() {
		fRetryTicker = 0;
		fSentMessages.poll();
		fTimeout = 0;
	}
	
	public int nextSequenceNumber() {
		return fNextSequenceNumber;
	}
	
	public int retryThreshold() {
		return fRetryThreshold;
	}

	public int windowSize() {
		return fWindowSize;
	}
	
	public void setWindowSize(int i) {
		fWindowSize = i;
		
		// Transfer buffered messages to a new queue
		MessageQueue<TransportPayloadMessage> n = new MessageQueue<TransportPayloadMessage>(i);
		while (!fQueuedMessages.empty()) n.push(fQueuedMessages.poll());
		fQueuedMessages = n;
		
		// Transfer the sent but ack-pending messages to a new queue of the appropriate size
		n = new MessageQueue<TransportPayloadMessage>(i);
		while (!fSentMessages.empty()) n.push(fSentMessages.poll());
		fSentMessages = n;
	}
	
	public int timeout() {
		return fTimeout;
	}
	
	public void fire(String name, float value) {
		fParent.recieveServerEvent(this, new EventInstance(name, fParent.simulator().frame(), value));
	}
}

