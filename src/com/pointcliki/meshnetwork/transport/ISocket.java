package com.pointcliki.meshnetwork.transport;

import com.pointcliki.meshnetwork.simulator.IReceiver;
import com.pointcliki.meshnetwork.simulator.ISender;

public interface ISocket extends ISender, IReceiver {	
	public String connection();
	public String source();	
	public String destination();
}
