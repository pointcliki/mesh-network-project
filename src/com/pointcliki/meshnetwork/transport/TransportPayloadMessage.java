package com.pointcliki.meshnetwork.transport;

import com.pointcliki.meshnetwork.message.IOrderedMessage;
import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.PayloadMessage;

public class TransportPayloadMessage extends PayloadMessage implements IOrderedMessage {

	private String fConnection;
	private int fSeq;

	public TransportPayloadMessage(String connection, int sequenceNumber, Message m) {
		super(m);
		fConnection = connection;
		fSeq = sequenceNumber;
	}
		
	public String connection() {
		return fConnection;
	}
	
	public int sequenceNumber() {
		return fSeq;
	}

}
