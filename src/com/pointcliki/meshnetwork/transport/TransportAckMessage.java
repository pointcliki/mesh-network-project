package com.pointcliki.meshnetwork.transport;

import com.pointcliki.meshnetwork.message.IMulticastAckMessage;
import com.pointcliki.meshnetwork.message.IOrderedMessage;
import com.pointcliki.meshnetwork.message.ITaggedMessage;
import com.pointcliki.meshnetwork.message.Message;

public class TransportAckMessage extends Message implements IOrderedMessage, IMulticastAckMessage, ITaggedMessage {

	private String fConnection;
	private int fSeq;
	
	public TransportAckMessage(String connection, int seq) {
		fSeq = seq;
		fConnection = connection;
	}
	/*
	@Override
	public Color color() {
		return Color.gray;
	}
	*/
	
	public String connection() {
		return fConnection;
	}

	public int sequenceNumber() {
		return fSeq;
	}

	@Override
	public String tag() {
		return fConnection + ":" + fSeq;
	}
}
