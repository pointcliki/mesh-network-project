package com.pointcliki.meshnetwork;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.pointcliki.meshnetwork.ui.panel.DataPanel;
import com.pointcliki.meshnetwork.ui.panel.GraphPanel;
import com.pointcliki.meshnetwork.ui.panel.LibraryPanel;
import com.pointcliki.meshnetwork.ui.panel.ResultsPanel;
import com.pointcliki.meshnetwork.ui.panel.ConfigurationPanel;
import com.pointcliki.meshnetwork.ui.panel.SimulationsPanel;
import com.pointcliki.meshnetwork.ui.panel.TaskPanel;
import com.pointcliki.meshnetwork.ui.utils.Icons;
import com.pointcliki.meshnetwork.ui.utils.SimpleDockable;

import bibliothek.gui.dock.common.CContentArea;
import bibliothek.gui.dock.common.CControl;
import bibliothek.gui.dock.common.CGrid;
import bibliothek.gui.dock.common.DefaultSingleCDockable;
import bibliothek.gui.dock.common.SingleCDockable;
import bibliothek.gui.dock.common.mode.ExtendedMode;
import bibliothek.gui.dock.common.theme.ThemeMap;

public class MeshNetworkApplication extends JFrame {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 2856961647692445206L;

	public MeshNetworkApplication() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			UIManager.put("Tree.paintLines", Boolean.FALSE);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		setTitle("Mesh Network Simulator");
		setSize(1024, 768);
		setLocationRelativeTo(null);
		setExtendedState(MAXIMIZED_BOTH);
		
		createDesktop();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private void createDesktop() {
		
		CControl control = new CControl(this);
		control.getThemes().select(ThemeMap.KEY_FLAT_THEME);
		add(control.getContentArea());
		
		ResultsPanel r = new ResultsPanel();
		
		final DefaultSingleCDockable sim = new SimpleDockable("Simulations", new SimulationsPanel(), Icons.PlayIcon);
		final SingleCDockable lib = new SimpleDockable("Library", new LibraryPanel(), Icons.BooksIcon);		
		final SingleCDockable res = new SimpleDockable("Results", r, Icons.PlayIcon);
		final SingleCDockable config = new SimpleDockable("Configuration", new ConfigurationPanel(), Icons.ConfigurationIcon);
		final SingleCDockable task = new SimpleDockable("Tasks", new TaskPanel(), Icons.EventIcon);
		final SingleCDockable data = new SimpleDockable("Data", new DataPanel(), Icons.ScriptIcon);
		final SingleCDockable graph = new SimpleDockable("Graph", new GraphPanel(), Icons.MetricIcon);
		
		CGrid grid = new CGrid(control);
		grid.add(0, 0, 1, 2, lib);
		grid.add(1, 2, 1, 2, res);
		grid.add(1, 0, 1, 2, sim);
		grid.add(0, 1, 1, 2, task);
		grid.add(2, 3, 2, 3, data);
		grid.add(2, 3, 2, 3, graph);
		grid.add(2, 0, 2, 1, config);
		
		CContentArea area = control.getContentArea();
		area.deploy(grid);
		
		r.setFocusWorker(new Runnable() {

			@Override
			public void run() {
				res.setExtendedMode(ExtendedMode.NORMALIZED);
				data.setExtendedMode(ExtendedMode.NORMALIZED);
			}
			
		});
	}

	public static void main(String[] args) {
		new MeshNetworkApplication();
	}
}
