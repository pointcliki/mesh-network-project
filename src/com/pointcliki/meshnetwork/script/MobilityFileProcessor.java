package com.pointcliki.meshnetwork.script;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.simulator.Simulator.SimulationEvent;

public class MobilityFileProcessor extends FileProcessor {
	
	private int fDelay = 0;

	public MobilityFileProcessor(Simulator s, String file) {
		super(s, file);
	}

	@Override
	protected boolean processLine(Simulator s, String[] line) throws Exception {
		String type = line[0];
		
		if (type.equals("wait")) {
			fDelay += Integer.parseInt(line[1]);
			
		} else if (type.equals("move")) {
			
		} else if (type.equals("disconnect")) {
			
			final String source = line[1];
			
			s.dispatcher().schedule(new Minion<SimulationEvent>() {
				@Override
				public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
					e.simulator().nodes().get(source).disconnect();
					
					return Minion.CONTINUE;
				}
			}, fDelay);	
			
			
		} else if (type.equals("connect")) {
			
			final String source = line[1];
			
			s.dispatcher().schedule(new Minion<SimulationEvent>() {
				@Override
				public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
					e.simulator().nodes().get(source).connect();
					
					return Minion.CONTINUE;
				}
			}, fDelay);	
		} else if (type.equals("reset")) {
			
			final String source = line[1];
			int offline = Integer.parseInt(line[2]);
			
			
			s.dispatcher().schedule(new Minion<SimulationEvent>() {
				@Override
				public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
					e.simulator().nodes().get(source).disconnect();
					
					return Minion.CONTINUE;
				}
			}, fDelay);
			
			s.dispatcher().schedule(new Minion<SimulationEvent>() {
				@Override
				public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
					e.simulator().nodes().get(source).connect();
					
					return Minion.CONTINUE;
				}
			}, fDelay + offline);
		} else {
			return false;
		}
		return true;
	};
	
}
