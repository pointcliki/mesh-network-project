package com.pointcliki.meshnetwork.script;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.meshnetwork.application.ApplicationMessage;
import com.pointcliki.meshnetwork.protocol.mac.MAC;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.service.IMulticastGroupRegistrarService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;
import com.pointcliki.meshnetwork.simulator.Simulator.SimulationEvent;

public class FlowFileProcessor extends FileProcessor {
	
	private int fDelay = 0;

	public FlowFileProcessor(Simulator s, String file) {
		super(s, file);
	}

	@Override
	protected boolean processLine(Simulator s, String[] line) throws Exception {
		String type = line[0];
		if (type.equals("wait")) {
				fDelay += Integer.parseInt(line[1]);
				
		} else if (type.equals("once")) {
			final String source = line[1];
			final String sink = line[2];
			final int size = Integer.parseInt(line[3]);
			
			s.dispatcher().schedule(new Minion<SimulationEvent>() {
				@Override
				public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
					Node n = e.simulator().nodes().get(source);
					// Check that the node exists and can send messages
					if (n != null && n.upstream(ILinkLayerService.class).state() != MAC.OFFLINE) {
						e.simulator().nodes().get(source).application().send(source, null);
					}
					
					return Minion.CONTINUE;
				}
			}, fDelay);
		} else if (type.equals("join")) {
			final String source = line[1];
			final String group = line[2];
			
			s.dispatcher().schedule(new Minion<SimulationEvent>() {
				@Override
				public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
					Node n = e.simulator().nodes().get(source);
					// Check that the node exists and can send messages
					if (n != null && n.upstream(ILinkLayerService.class).state() != MAC.OFFLINE) {
						n.application().downstream(IMulticastGroupRegistrarService.class).join(group);
					}
					
					return Minion.CONTINUE;
				}
			}, fDelay);	
			
		} else {
			return false;					
		}
		return true;
	}

}
