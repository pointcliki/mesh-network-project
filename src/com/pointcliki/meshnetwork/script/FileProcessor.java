package com.pointcliki.meshnetwork.script;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.pointcliki.meshnetwork.simulator.Simulator;

public abstract class FileProcessor {
	
	private int fLine = 0;
 
	public FileProcessor(Simulator s, String file) {
		BufferedReader r = null;
		try {
			r = new BufferedReader(new FileReader(file));
			while (r.ready()) {
				String[] line = r.readLine().split(" ");
				try {
					if (!processLine(s, line)) {
						System.out.println("Skipped line " + fLine + " of layout file");
					}
				} catch (Exception e) {
					System.out.println("Skipped line " + fLine + " of layout file");
					e.printStackTrace();
					
				} finally {
					fLine++;
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (r != null) r.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public int line() {
		return fLine;
	}
	
	protected abstract boolean processLine(Simulator s, String[] line) throws Exception;
}
