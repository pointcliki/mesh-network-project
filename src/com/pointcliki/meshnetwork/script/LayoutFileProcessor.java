package com.pointcliki.meshnetwork.script;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

public class LayoutFileProcessor extends FileProcessor {

	public LayoutFileProcessor(Simulator s, String string) {
		super(s, string);
	}

	@Override
	protected boolean processLine(Simulator sim, String[] line) throws Exception {
		String type = line[0];
		if (type.equals("node")) {
			Node n = new Node(line[1], sim);	
			n.position(new Vector2f(Integer.valueOf(line[2]), Integer.valueOf(line[3])));
			if (sim.graphical()) sim.game().simulationScene().canvas().addChild(n.entity());
			sim.nodes().put(line[1], n);
			
		} else if (type.equals("grid")) {
			int i = 0;
			String prefix = line[1];
			int offx = Integer.valueOf(line[2]);
			int offy = Integer.valueOf(line[3]);
			int dx = Integer.valueOf(line[4]);
			int dy = Integer.valueOf(line[5]);
			int nx = Integer.valueOf(line[6]);
			int ny = Integer.valueOf(line[7]);
			for (int y = 0; y < ny; y++)
				for (int x = 0; x < nx; x++) {
					Node n = new Node(line[1] + i, sim);
					n.position(new Vector2f(offx + dx * x, offy + dy * y));
					sim.nodes().put(prefix + i, n);
					if (sim.graphical()) sim.game().simulationScene().canvas().addChild(n.entity());
					i++;
				}
		} else {
			return false;
		}
		return true;
	}

}
