package com.pointcliki.meshnetwork.application;

import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;

public class ApplicationData extends ProtocolData {
	
	private int fSentPackets;
	private int fReceivedPackets;
	private int fLostPackets;

	public ApplicationData(Protocol p) {
		super(p);
	}

	@Override
	public void startup() {
		fSentPackets = 0;
		fReceivedPackets = 0;
		fLostPackets = 0;
	}

	@Override
	public void finish() {

	}

	@Override
	public void beginTick() {
		fSentPackets = 0;
		fReceivedPackets = 0;
		fLostPackets = 0;
	}
	
	public void incrementSent(int s) {
		fSentPackets += s;
	}
	
	public void incrementReceived(int s) {
		fReceivedPackets += s;
	}
	
	public void incrementLost(int s) {
		fLostPackets += s;
	}

	@Override
	public void endTick() {
		//simulator().graph("app").add(0, fLostPackets);
		//simulator().graph("app").add(1, fSentPackets);
		//simulator().graph("app").add(2, fReceivedPackets);
	}

	@Override
	public String info() {
		return "<html><b>Basic Application</b><br/></html>";
	}

}
