package com.pointcliki.meshnetwork.application;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.TaggedChannelMessage;

public class ApplicationMessage extends TaggedChannelMessage {

	public ApplicationMessage(String source, String destination, int tagNumber, Message m) {
		super(source, destination, tagNumber, m);
	}

	public String toString() {
		return "[Application message from " + source() + " to " + destination() + " of size " + size() + "] " + (payload() != null ? "\n" + payload() : ""); 
	}

}