package com.pointcliki.meshnetwork.application;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.service.IApplicationService;
import com.pointcliki.meshnetwork.simulator.Node;
import com.pointcliki.meshnetwork.simulator.Simulator;

/**
 * An application is a top layer entity that is sends and receives
 * application layer packets across the network. When defining a
 * protocol stack, the highest layer protocol sets a 
 * 
 * @author hugheth
 */
public class Application extends Protocol implements IApplicationService {
	
	public Application() {}
	
	public Application(Node n, ProtocolData data, Protocol downstream) {
		super(n, data, downstream);
	}
	
	@Override
	public boolean receive(String source, Message m) {
		node().simulator().data("app", ApplicationData.class).incrementReceived(m.size());
		node().simulator().log(Simulator.LOG_APP, "Application at " + node().name() + " received: " + m);
		return true;
	}

	@Override
	public void tick(long frame) {}

	@Override
	public boolean send(String destination, Message m) {
		boolean okay = fDownStream.send(destination, m);
		if (okay) node().simulator().data("app", ApplicationData.class).incrementSent(m.size());
		else node().simulator().data("app", ApplicationData.class).incrementLost(m.size());
		return okay;
	}

	@Override
	public void startup() {
		
	}

	@Override
	public String name() {
		return "Application";
	}
	
	@Override
	public ProtocolData createData() {
		return new ApplicationData(this);
	}
	
	@Override
	public ApplicationData data() {
		return (ApplicationData) super.data();
	}

	@Override
	protected void createEventListeners() {
		
	}
}
