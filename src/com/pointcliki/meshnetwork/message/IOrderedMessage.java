package com.pointcliki.meshnetwork.message;

public interface IOrderedMessage {
	public int sequenceNumber();
}
