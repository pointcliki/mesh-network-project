package com.pointcliki.meshnetwork.message;

public class MulticastControlMessage extends Message implements IMulticastMessage {

	private String fGroup;
	private String fOrigin;
	
	public MulticastControlMessage(String origin, String group) {
		fGroup = group;
		fOrigin = origin;
	}
	public String origin() {
		return fOrigin;
	}

	@Override
	public String group() {
		return fGroup;
	}

}
