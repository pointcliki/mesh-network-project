package com.pointcliki.meshnetwork.message;

/**
 * The tagged message interface allows us to determine whether a
 * message of the same class or in the same level of the protocol
 * stack hierarchy is equivalent to another. 
 *  
 * @author Hugheth
 */
public interface ITaggedMessage {

	public String tag();
}
