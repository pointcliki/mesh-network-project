package com.pointcliki.meshnetwork.message;

public class PayloadMessage extends Message implements IPayloadMessage {

	private Message fPayload;
	
	public PayloadMessage(Message payload) {
		fPayload = payload;
	}
	
	@Override
	public Message payload() {
		return fPayload;
	}
	
	@Override
	public int size() {
		if (fPayload == null) return 1;
		return fPayload.size();
	}
}
