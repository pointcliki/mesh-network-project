package com.pointcliki.meshnetwork.message;

/**
 * A traceable message contains the origin of the message
 * and distance from the origin the message has travelled.
 * This is useful for distance vector protocols as it may
 * allow them to ascertain valid paths back to the origin
 * from a node that receives a traceable message.
 * 
 * @author Hugheth
 */
public interface ITraceableMessage {

	public int distance();
	public String origin();
}

