package com.pointcliki.meshnetwork.message;

public interface IMulticastMessage {
	public String group();
}
