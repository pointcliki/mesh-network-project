package com.pointcliki.meshnetwork.message;

public interface ITimestampMessage {

	public long timestamp();
}
