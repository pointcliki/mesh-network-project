package com.pointcliki.meshnetwork.message;

public class TextMessage extends Message{

	private String fText;
	
	public TextMessage(String text) {
		fText = text;
	}
	
	public String text() {
		return fText;
	}
	
	public int size() {
		return fText.length() / 100;
	}
}
