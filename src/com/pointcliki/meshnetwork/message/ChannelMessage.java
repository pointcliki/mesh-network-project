package com.pointcliki.meshnetwork.message;

import com.pointcliki.meshnetwork.message.Message;
import com.pointcliki.meshnetwork.message.IChannelMessage;

public abstract class ChannelMessage extends PayloadMessage implements IChannelMessage {

	private String fSource;
	private String fDest;

	public ChannelMessage(String source, String destination, Message m) {
		super(m);
		fSource = source;
		fDest = destination;
	}
	
	public String destination() {
		return fDest;
	}

	@Override
	public String source() {
		return fSource;
	}
}
