package com.pointcliki.meshnetwork.message;

public class TaggedMulticastMessage extends Message implements IMulticastMessage, ITaggedMessage {

	private Message fPayload;
	private String fGroup;
	private String fTag;
	
	public TaggedMulticastMessage(String group, String tag, Message payload) {
		fGroup = group;
		fPayload = payload;
		fTag = tag; 
	}
	
	public TaggedMulticastMessage(String group, int number, Message payload) {
		fGroup = group;
		fPayload = payload;
		fTag = group + ":" + number;
	}
	
	@Override
	public int size() {
		if (fPayload == null) return 1;
		return fPayload.size();
	}
	
	public String group() {
		return fGroup;
	}

	@Override
	public String tag() {
		return fTag;
	}

	public Message payload() {
		return fPayload;
	}
}
