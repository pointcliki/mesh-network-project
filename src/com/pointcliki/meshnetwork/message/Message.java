package com.pointcliki.meshnetwork.message;

/**
 * The Message class is the root class for information that
 * can pass through the mesh network. All messages have a size.
 * 
 * @author Hugheth
 */
public class Message {
	public int size() {
		return 1;
	}
}
