package com.pointcliki.meshnetwork.message;

public interface IChannelMessage extends IPayloadMessage {
	public String source();
	public String destination();
}
