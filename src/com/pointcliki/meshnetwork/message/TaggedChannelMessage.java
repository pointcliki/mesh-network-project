package com.pointcliki.meshnetwork.message;

public abstract class TaggedChannelMessage extends ChannelMessage implements ITaggedMessage {

	private String fTag;
	
	public TaggedChannelMessage(String source, String destination, int tagNumber, Message m) {
		super(source, destination, m);
		fTag = source + ":" + tagNumber;
	}
	
	public TaggedChannelMessage(String source, String destination, String tag, Message m) {
		super(source, destination, m);
		fTag = tag;
	}

	@Override
	public String tag() {
		return fTag;
	}

}
