package com.pointcliki.meshnetwork.message;

public interface IMulticastAckMessage extends ITaggedMessage {
	public int sequenceNumber();
	public String connection();
}
