package com.pointcliki.meshnetwork.simulator;

import java.awt.Font;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import com.pointcliki.core.Scene;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;

public class SimulatorScene extends Scene {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -3434676313550166866L;

	protected static UnicodeFont sFont;
	private Simulator fSim;
	private SimulatorEntity fCanvas;
	
	public SimulatorScene(SimulatorGame app, Simulator sim) {
		super(app);
		registerUIMinion();
		fSim = sim;
		fCanvas = new SimulatorEntity(fSim);
	}

	@Override
	public void init() {
		super.init();
		
		addChild(fCanvas);
		
		try {
			initFont();
		} catch (SlickException e) {
			System.out.println(e.getMessage());
		}
		fFrameManager.queueNext(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type,	FrameEvent event) {
				boolean cont = fSim.graphicalTick(timeManager().currentTime());
				if (cont) return Minion.CONTINUE;
				return Minion.FINISH;
			}
		});
		fTimeManager.start();
		
		fSim.setup();
	}

	@SuppressWarnings("unchecked")
	private void initFont() throws SlickException {
		// Initialise the font
		sFont = new UnicodeFont(new Font("Arial", 1, 10));
		sFont.addAsciiGlyphs();
		sFont.getEffects().add(new ColorEffect(java.awt.Color.white));
		sFont.loadGlyphs();
	}
	
	public static UnicodeFont font() {
		return sFont;
	}

	public SimulatorEntity canvas() {
		return fCanvas;
	}
}

