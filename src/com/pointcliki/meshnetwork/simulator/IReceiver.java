package com.pointcliki.meshnetwork.simulator;

import com.pointcliki.meshnetwork.message.Message;

public interface IReceiver {
	/**
	 * Alert the receiver of a new message to process
	 * @param m The new message
	 * @return Whether the receiver processed the message
	 */
	public boolean receive(String source, Message message);
}
