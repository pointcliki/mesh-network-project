package com.pointcliki.meshnetwork.simulator;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.core.Viewport;

/**
 * The simulator game class is the root class for the OpenGL canvas
 * on which graphical simulations are displayed. This parents the
 * SimulatorScene class which handles the rendering of the simulation. 
 * 
 * @author Hugheth
 */
public class SimulatorGame extends PointClikiGame {
	
	private SimulatorScene fSimScene;
	private Simulator fSim;
	
	public SimulatorGame(Simulator s) {
		super("Mesh Network Simulator");
		fSim = s;
	}

	@Override
	protected void configureScenes() {
		fSimScene = new SimulatorScene(this, fSim);
		fScenes.add(fSimScene);
	}

	@Override
	protected void configureViewports() {
		fViewports.add(new Viewport(fScenes.get(0), 0, new Rectangle(0, 0, 1280, 720)));
	}
	
	@Override
	public void init(GameContainer c) throws SlickException {
		super.init(c);
		application().setClearEachFrame(false);
	}
	
	public SimulatorScene simulationScene() {
		return fSimScene;
	}
	
	public static void play(PointClikiGame game) {
		sGame = game;
		try {
			sApp = new AppGameContainer(sGame);
			sApp.setForceExit(false);
			sApp.setDisplayMode(960, 720, false);
			sApp.setVerbose(false);
			sApp.setShowFPS(false);
			sApp.setAlwaysRender(true);
			sApp.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
