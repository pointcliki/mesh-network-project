package com.pointcliki.meshnetwork.simulator;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.meshnetwork.model.EventInstance;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.mac.MACData;
import com.pointcliki.meshnetwork.service.IApplicationService;

public class Node {

	private String fName;
	private Protocol fProtocol;
	private Simulator fSimulator;
	private NodeEntity fEntity;
	private Vector2f fPosition;
	
	public Node(String name, Simulator s) {
		fProtocol = s.createProtocolStack(this);
		fName = name;
		fSimulator = s;
		if (s.graphical()) fEntity = new NodeEntity(this);
	}
	
	public NodeEntity entity() {
		return fEntity;
	}
	
	public IApplicationService application() {
		return upstream(IApplicationService.class);
	}
	
	public Simulator simulator() {
		return fSimulator;
	}
	
	public Protocol protocol() {
		return fProtocol;
	}
	
	public String name() {
		return fName;
	}
	
	@Override
	public String toString() {
		return "<Node " + fName + ">";
	}
	
	public void tick(long frame) {
		fProtocol.tick(frame);
	}

	public void startup() {
		fProtocol.startup();
	}
	public void finish() {
		fProtocol.finish();
	}
	
	public void connect() {
		simulator().log(Simulator.LOG_NODES, "[" + fName + " Reconnected]");
		fProtocol.recieveEvent(new EventInstance(MACData.MAC_NODE_ONLINE, simulator().frame(), 1));
		startup();
	}

	public void disconnect() {
		simulator().log(Simulator.LOG_NODES, "[" + fName + " Disconnected]");
		fProtocol.recieveEvent(new EventInstance(MACData.MAC_NODE_OFFLINE, simulator().frame(), 1));
	}
	

	@SuppressWarnings("unchecked")
	public <V> V upstream(Class<V> type) {
		if (type.isInstance(fProtocol)) return (V) fProtocol;
		return fProtocol.upstream(type);
	}

	public void position(Vector2f pos) {
		fPosition = pos;
		if (fEntity != null) fEntity.position(pos);
	}
	public Vector2f position() {
		return fPosition;
	}
}
