package com.pointcliki.meshnetwork.simulator;

import com.pointcliki.meshnetwork.message.Message;


public interface ISender {
	/**
	 * Send a message with this sender
	 * @param destination The destination address of the message
	 * @param message The message to send
	 * @return Whether the sender could accept and send this message
	 */
	public boolean send(String destination, Message message);
}
