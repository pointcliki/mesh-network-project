package com.pointcliki.meshnetwork.simulator;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.pointcliki.core.Viewport;
import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.ui.UIEntity;

public class SimulatorEntity extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3027196245305310089L;
	
	private Simulator fSim;

	public SimulatorEntity(Simulator sim) {
		fSim = sim;
	}
	
	@Override
	public void render(Graphics graphics, Viewport view, long currentTime) {
		graphics.setColor(new Color(0, 0, 0, 20));
		graphics.fillRect(0, 0, 1280, 720);
		
		for (Annotation a: fSim.annotations()) {
			a.render(fSim, graphics, currentTime);
		}
		super.render(graphics, view, currentTime);
	}
}

