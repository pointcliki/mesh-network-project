package com.pointcliki.meshnetwork.simulator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultListModel;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.meshnetwork.model.Annotation;
import com.pointcliki.meshnetwork.model.Script;
import com.pointcliki.meshnetwork.model.ScriptType;
import com.pointcliki.meshnetwork.model.SimulationItem;
import com.pointcliki.meshnetwork.protocol.Protocol;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.script.FlowFileProcessor;
import com.pointcliki.meshnetwork.script.LayoutFileProcessor;
import com.pointcliki.meshnetwork.script.MobilityFileProcessor;
import com.pointcliki.meshnetwork.service.ILinkLayerService;

public class Simulator implements Runnable {
	
	public int fVerbosity;
	
	public static final int LOG_MAC = 1;
	public static final int LOG_LINKLAYER = 2;
	public static final int LOG_ROUTING = 4;
	public static final int LOG_ROUTES = 8;
	public static final int LOG_MULTIROUTING = 16;
	public static final int LOG_NODES = 32;
	public static final int LOG_APP = 64;
	public static final int LOG_SIMULATOR = 128;

	private SimulationItem fSimulation;
	private SimulatorGame fGame;
	private boolean fGraphical;
	private DefaultListModel fLog;
	
	private long fLastFrameTime;
	private long fFrame = 0;
	private HashMap<String, Node> fNodes;
	
	private SimulationDispatcher fDispatcher;
	
	public Simulator(SimulationItem sim, int i) {
		fSimulation = sim;
		fGraphical = sim.graphical() && i == 0;
		
		fNodes = new HashMap<String, Node>();
		
		fDispatcher = new SimulationDispatcher();
		fDispatcher.scheduleAlways(new SimulatorSteps.PreTickStep());
		fDispatcher.scheduleAlways(new SimulatorSteps.TickStep());
		fDispatcher.scheduleAlways(new SimulatorSteps.PostTickStep());
		
		fLog = new DefaultListModel();
		fVerbosity = sim.verbosity();
		
		sim.setSimulator(this);
	}
	
	public SimulationItem simulation() {
		return fSimulation;
	}
	
	@Override
	public void run() {
		try {
			
			log(LOG_SIMULATOR, "=== Simulation Started ===");
			
			// Display the first simulation
			if (fGraphical) {
				fGame = new SimulatorGame(this);
				SimulatorGame.play(fGame);
			} else {
				setup();
				// Loop through steps of the simulation
				for (int i = 0; i < simulation().duration() + 1; i++) {
					tick();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setup() {
		
		// Read in scripts
		for (Script s: fSimulation.scripts().items()) {
			if (s.type() == ScriptType.Layout) new LayoutFileProcessor(this, "layouts/" + s.name());
			else if (s.type() == ScriptType.Mobility) new MobilityFileProcessor(this, "mobility/" + s.name());
			else if (s.type() == ScriptType.Flow) new FlowFileProcessor(this, "flows/" + s.name());
		}
		
		calculateLocalNodes();
		for (ProtocolData d: fSimulation.stack().items()) d.startup();
		for (Node n: fNodes.values()) n.startup();
		
		if (fGraphical) fLastFrameTime = fGame.simulationScene().timeManager().currentTime();
		
		simulation().started();
	}
	

	protected Protocol createProtocolStack(Node n) {
		Protocol bottom = null;
		Protocol p = null;
		for (ProtocolData d: fSimulation.stack().items()) {
			try {
				p = d.protocol().getClass().getConstructor(Node.class, ProtocolData.class, Protocol.class).newInstance(n, d, p);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			if (bottom == null) bottom = p;
		}
		return bottom;
	}
	
	/**
	 * If we are running a graphical simulation this method
	 * checks whether one or more steps of the simulation
	 * can be run this game frame
	 */
	public boolean graphicalTick(long time) {
		int dur = simulation().frameDuration();
		while (time > fLastFrameTime + dur) {
			if (!tick()) return false;
			fLastFrameTime += dur;
		}
		return true;
	}
	
	public boolean tick() {
		if (fFrame == simulation().duration()) {
			for (Node n: fNodes.values()) n.finish();
			log(LOG_SIMULATOR, "=== Simulation Finished ===");
			return false;
		}
		
		log(LOG_MAC, "=== Frame " + fFrame + " ===");
		
		fDispatcher.tick(fFrame);
		
		fFrame++;
		return true;
	}

	public void dispatchSimulationEvent() {
		fDispatcher.dispatchSimulationEvent(fFrame);
	}

	public SimulatorGame game() {
		return fGame;
	}
	
	public long lastFrameTime() {
		return fLastFrameTime;
	}
	public long frameLength() {
		return fSimulation.frameDuration();
	}
	
	/**
	 * Calculate which nodes are near to each other, storing
	 * the values in the node instances themselves.
	 */
	public void calculateLocalNodes() {
		for (Node n: fNodes.values()) n.upstream(ILinkLayerService.class).calculateLocalNodes();
	}
	
	public long frame() {
		return fFrame;
	}
	
	public HashMap<String, Node> nodes() {
		return fNodes;
	}
	public int nodeRange() {
		return simulation().nodeRange();
	}
	
	public List<ProtocolData> data() {
		return fSimulation.stack().items();
	}
	
	public <T extends ProtocolData> T data(String s, Class<T> cls) {
		for (ProtocolData d: fSimulation.stack().items()) {
			if (d.name().equals(s)) return cls.cast(d);
		}
		return null;
	}
	
	public boolean graphical() {
		return fGraphical;
	}

	public SimulationDispatcher dispatcher() {
		return fDispatcher;
	}
	
	public void log(int flag, String string) {
		if ((fVerbosity & flag) > 0) fLog.addElement(string);
	}
	public DefaultListModel log() {
		return fLog;
	}
	
	public class SimulationEvent implements IEvent {

		private long fFrame;
		
		public SimulationEvent(long frame) {
			fFrame = frame;
		}
		
		public long frame() {
			return fFrame;
		}
		
		public Simulator simulator() {
			return Simulator.this;
		}
	}
	
	
	public class SimulationDispatcher extends Dispatcher<SimulationEvent> {
		/**
		 * Serial key
		 */
		private static final long serialVersionUID = -8473346733443874831L;

		public void tick(long frame) {
			dispatchEvent("frame", new SimulationEvent(frame));
		}

		public void dispatchSimulationEvent(long frame) {
			dispatchEvent("frame" + frame, new SimulationEvent(frame));
		}

		public void scheduleAlways(Minion<SimulationEvent> m) {
			addMinion("frame", m);
		}
		public void schedule(Minion<SimulationEvent> m, long delay) {
			addMinion("frame" + delay, m);
		}
	}

	public List<Annotation> annotations() {
		return simulation().annotations().items();
	}
}

