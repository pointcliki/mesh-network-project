package com.pointcliki.meshnetwork.simulator;

import java.util.ArrayList;
import java.util.Collections;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.meshnetwork.protocol.ProtocolData;
import com.pointcliki.meshnetwork.protocol.mac.MAC;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.meshnetwork.simulator.Simulator.SimulationEvent;

public class SimulatorSteps {
	
	public static class PreTickStep extends Minion<SimulationEvent> {
		
		public PreTickStep() {
			super(-100);
		}
		
		/**
		 * Computes one frame of the simulation.
		 */
		@Override
		public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
			for (ProtocolData d: e.simulator().data()) d.beginTick();
			return Minion.CONTINUE;
		}
	}
	
	public static class TickStep extends Minion<SimulationEvent> {
		/**
		 * Computes one frame of the simulation.
		 */
		@Override
		public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
			
			e.simulator().dispatchSimulationEvent();
								
			// Nudge protocols
			for (Node n: e.simulator().nodes().values()) n.protocol().tick(e.frame());
			
			// Collect nodes that are trying to send
			ArrayList<Node> trying = new ArrayList<Node>();
			
			// Iterate through nodes to determine who wants to send
			for (Node n: e.simulator().nodes().values()) {
				ILinkLayerService mac = n.upstream(ILinkLayerService.class);
				
				if (mac.state() == MAC.IDLE && mac.queueSize() > 0) mac.startTryingToSend();
				if (mac.state() == MAC.TRYING_TO_SEND) trying.add(n);
			}
			
			// Shuffle them into a random order
			Collections.shuffle(trying);
			
			for (Node n: trying) if (n.upstream(ILinkLayerService.class).state() == MAC.TRYING_TO_SEND) {
				n.upstream(ILinkLayerService.class).tryToSend();
			}
			
			for (Node n: e.simulator().nodes().values()) n.upstream(ILinkLayerService.class).saveState();
			
			// Now iterate through the nodes to send packets
			for (Node n: e.simulator().nodes().values()) if (n.upstream(ILinkLayerService.class).state() == MAC.TRANSMITTING) {
				n.upstream(ILinkLayerService.class).sendPacket();
			}
			
			// TODO: DO THINGS WITH CONGESTION
			
			return Minion.CONTINUE;
		}
	}
	
	public static class PostTickStep extends Minion<SimulationEvent> {
		
		public PostTickStep() {
			super(100);
		}
		
		@Override
		public long run(Dispatcher<SimulationEvent> dispatcher, String type, SimulationEvent e) {
			for (ProtocolData d: e.simulator().data()) d.endTick();
			return Minion.CONTINUE;
		}
	}
}
