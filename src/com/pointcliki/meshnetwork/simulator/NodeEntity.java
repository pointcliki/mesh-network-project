package com.pointcliki.meshnetwork.simulator;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.pointcliki.core.Viewport;
import com.pointcliki.meshnetwork.protocol.mac.MAC;
import com.pointcliki.meshnetwork.service.ILinkLayerService;
import com.pointcliki.ui.UIEntity;

public class NodeEntity extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -6465904206023334022L;

	private Node fNode;
	
	public NodeEntity(Node n) {
		fNode = n;
	}

	@Override
	public void render(Graphics graphics, Viewport view, long currentTime) {
		graphics.setColor(Color.black);
		graphics.setLineWidth(1);
		graphics.fillOval(-5, -5, 10, 10);
		
		graphics.setColor(new Color(200 * (fNode.upstream(ILinkLayerService.class).queueSize()) / fNode.upstream(ILinkLayerService.class).queueCapacity() + 30, 30, 30));
		
		if (fNode.upstream(ILinkLayerService.class).state() == MAC.OFFLINE) {
			graphics.drawOval(-5, -5, 10, 10);
		} else {
			graphics.fillOval(-5, -5, 10, 10);
		}
		
		
		//SimulatorScene.font().drawString(-10, 12, fName, Color.white);
		// Draw buffer
		//SimulatorScene.font().drawString(-4, -6, fMessages.size() + "", Color.black);
	}
}
