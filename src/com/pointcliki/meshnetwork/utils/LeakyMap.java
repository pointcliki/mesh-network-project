package com.pointcliki.meshnetwork.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LeakyMap<A, B> extends HashMap<A, LeakyRecord<B>> implements ILeakyStructure<A> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7637504866658975569L;

	private long fCurrentTime;
	
	public List<A> changes(long currentTime) {
		 ArrayList<A> list = new ArrayList<A>();
		 for (A key: keySet()) {
			 if (get(key).fExpireTime <= currentTime) list.add(key);
		 }
		 return list;
	}
	
	public void update(long currentTime) {
		fCurrentTime = currentTime;
		for (A key: changes(currentTime)) remove(key);
	}
	
	public B putSoft(A key, B value, int lifespan) {
		if (lifespan < 1) return null;
		
		long expires = lifespan + fCurrentTime;
		put(key, new LeakyRecord<B>(value, expires));
				
		return value;
	}
	
	public void updateSoft(A key, B value) {
		get(key).fElem = value;
	}

	public B getSoft(A key) {
		return get(key).fElem;
	}
}