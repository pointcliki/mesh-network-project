package com.pointcliki.meshnetwork.utils;

import java.util.List;

public interface ILeakyStructure<A> {

	public List<A> changes(long currentTime);
	
	public void update(long currentTime);
}
