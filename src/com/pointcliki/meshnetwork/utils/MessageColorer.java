package com.pointcliki.meshnetwork.utils;

import org.newdawn.slick.Color;

import com.pointcliki.meshnetwork.message.Message;

public abstract class MessageColorer {

	public abstract Color color(Message m);

}
