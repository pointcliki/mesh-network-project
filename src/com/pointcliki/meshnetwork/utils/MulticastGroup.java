package com.pointcliki.meshnetwork.utils;

import java.util.ArrayList;
import java.util.Iterator;

public class MulticastGroup implements Iterable<String> {

	private String fLeader;
	private ArrayList<String> fParticipants;
	private boolean fSimplex;
	
	public MulticastGroup(String leader, boolean simplex) {
		fLeader = leader;
		fSimplex = simplex;
	}

	@Override
	public Iterator<String> iterator() {
		return fParticipants.iterator();
	}
	
	public void addParticipant(String name) {
		fParticipants.add(name);
	}
	
	public void removeParticipant(String name) {
		fParticipants.remove(name);
	}
	
	public boolean simplex() {
		return fSimplex;
	}
	
	public String leader() {
		return fLeader;
	}
}
