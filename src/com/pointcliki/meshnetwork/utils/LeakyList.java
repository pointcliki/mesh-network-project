package com.pointcliki.meshnetwork.utils;

import java.util.ArrayList;
import java.util.List;

public class LeakyList<A> extends ArrayList<LeakyRecord<A>> implements ILeakyStructure<A> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1258643874245935794L;

	private long fCurrentTime;
	
	public void addSoft(A elem, int lifespan) {
		add(new LeakyRecord<A>(elem, lifespan + fCurrentTime));
	}

	@Override
	public List<A> changes(long currentTime) {
		ArrayList<A> list = new ArrayList<A>();
		for (LeakyRecord<A> record: this) {
			if (record.fExpireTime <= currentTime) {
				list.add(record.fElem);
			}
		}
		return list;
	}

	@Override
	public void update(long currentTime) {
		fCurrentTime = currentTime;
		for (A elem: changes(currentTime)) remove(elem);
	}

}