package com.pointcliki.meshnetwork.utils;

public class LeakyRecord<B> {
	public B fElem;
	public long fExpireTime;
	public LeakyRecord(B elem, long expireTime) {
		fElem = elem;
		fExpireTime = expireTime;
	}
}