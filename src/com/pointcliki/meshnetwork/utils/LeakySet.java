package com.pointcliki.meshnetwork.utils;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

public class LeakySet<A> extends HashSet<LeakyRecord<A>> implements ILeakyStructure<A> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 11120987172L;
	
	private long fCurrentTime;
	
	public void addSoft(A elem, int lifespan) {
		add(new LeakyRecord<A>(elem, lifespan + fCurrentTime));
	}
	
	@Override
	public List<A> changes(long currentTime) {
		ArrayList<A> list = new ArrayList<A>();
		for (LeakyRecord<A> record: this) {
			if (record.fExpireTime <= currentTime) {
				list.add(record.fElem);
			}
		}
		return list;
	}
	
	public void update(long currentTime) {
		fCurrentTime = currentTime;
		for (A elem: changes(currentTime)) remove(elem);
	}
}
