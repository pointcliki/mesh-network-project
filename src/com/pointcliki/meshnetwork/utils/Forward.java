package com.pointcliki.meshnetwork.utils;

import com.pointcliki.meshnetwork.message.Message;

public class Forward extends Message {
	public String destination;
	public String nextHop;
	public int distance;
	public Forward(String destination, String nextHop, int distance) {
		this.destination = destination;
		this.nextHop = nextHop;
		this.distance = distance;
	}
	
	public String toString() {
		return "[" + destination + " in " + distance + " via " + nextHop + "]";
	}

	@Override
	public int size() {
		return 1;
	}
}