package com.pointcliki.meshnetwork.utils;

import com.pointcliki.meshnetwork.message.Message;

public class Backward extends Message {
	public String source;
	public String nextHop;
	public int distance;
	public Backward(String source, String nextHop, int distance) {
		this.source = source;
		this.nextHop = nextHop;
		this.distance = distance;
	}
	
	public String toString() {
		return "[" + source + " in " + distance + " via " + nextHop + "]";
	}

	@Override
	public int size() {
		return 1;
	}
}