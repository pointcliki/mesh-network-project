package com.pointcliki.meshnetwork.utils;

import java.util.ArrayList;

import com.pointcliki.meshnetwork.message.Message;

public class MessageQueue<T extends Message> {
	
	private ArrayList<T> fArray;
	private int fStart = 0;
	private int fEnd = 0;
	private int fSize = 0;

	public MessageQueue(int capacity) {
		fArray = new ArrayList<T>(capacity + 1);
		for (int i = 0; i < capacity; i++) fArray.add(null);
	}
	
	public boolean push(T message) {
		if (full()) return false;
		fArray.set(fEnd, message); 
		fEnd = (fEnd + 1) % fArray.size();
		fSize++;
		return true;
	}
	
	public T poll() {
		T m = peek();
		if (m != null) {
			fStart = (fStart + 1) % fArray.size();
			fSize--;
		}
		return m;
	}

	public T peek() {
		if (fSize == 0) return null;
		return fArray.get(fStart);
	}

	public int size() {
		return fSize;
	}
	
	public boolean empty() {
		return fSize == 0;
	}
	
	public boolean full() {
		return fSize == capacity();
	}

	public void reset() {
		fStart = 0;
		fEnd = 0;
	}
	
	public int capacity() {
		return fArray.size() - 2;
	}
}
