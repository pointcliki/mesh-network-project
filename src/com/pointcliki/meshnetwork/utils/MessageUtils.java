package com.pointcliki.meshnetwork.utils;

import com.pointcliki.meshnetwork.message.IChannelMessage;
import com.pointcliki.meshnetwork.message.Message;

public class MessageUtils {
	
	@SuppressWarnings("unchecked")
	public static <V extends Message> V unwrap(Message m, Class<V> cls) {
		if (cls.isInstance(m)) return (V) m;
		else if (m instanceof IChannelMessage) return MessageUtils.unwrap(((IChannelMessage) m).payload(), cls);
		return null;
	}	
}

